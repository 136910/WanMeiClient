﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiBussinessService
{
    /// <summary>
    /// 基础服务接口，释放资源
    /// </summary>
    public interface IWanMeiService : IDisposable
    {
        void Dispose();
    }
}
