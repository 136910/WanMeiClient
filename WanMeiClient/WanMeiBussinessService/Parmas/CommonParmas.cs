﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiBussinessService
{
    class CommonParmas
    {
    }
    public class GetRaidFightHandleResultParams
    {
        public int HeroId { get; set; }
        public string Un { get; set; }
        public int FightStatus { get; set; }
        public long PotentialValues { get; set; }
        public long Money { get; set; }
        public int RaidId { get; set; }
        /// <summary>
        /// 获取副本经验值后当前角色拥有的经验值
        /// </summary>
        public long RoleCurrentExValues { get; set; }
        public int RoleCurrentLevel { get; set; }
        public int RoleCurrentHealth { get; set; }
        public int RoleCurrentMagic { get; set; }
        public int RoleCurrentArmor { get; set; }
        public int RoleCurrentCritical { get; set; }
        public int RoleCurrentAp { get; set; }

    }
}
