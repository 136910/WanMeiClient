﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WanMeiModel;

namespace WanMeiBussinessService
{
    public interface IRaidService:IWanMeiService
    {
        /// <summary>
        /// 获取所有副本列表信息
        /// </summary>
        /// <returns></returns>
        List<RaidItem> GetRaidList();
        /// <summary>
        /// 获取副本的怪
        /// </summary>
        /// <param name="radiId"></param>
        /// <returns></returns>
        List<MonsterModel> GetMonsterList(int radiId);
        /// <summary>
        /// 获取战斗处理结果
        /// </summary>
        /// <param name="monsters"></param>
        /// <param name="fightStatus"></param>
        /// <param name="un"></param>
        /// <returns></returns>
        RaidFightResultModel GetRaidFightHandleResult(GetRaidFightHandleResultParams parms);
    }
}
