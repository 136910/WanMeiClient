﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WanMeiCommon;
using WanMeiModel;
using WanMeiSqlTools;

namespace WanMeiBussinessService
{
    public class UserService : IUserService
    {

        #region IUserService 成员
        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Register(UserRegisterModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms = 
            {
                new SqlParameter("@username",model.UserName),
                new SqlParameter("@password",MD5Manager.MD5Encrypt(model.PassWord)),
                new SqlParameter("@istrue",1),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "wanmei_pro_register", parms);
            if (rtn_err.Value != null)
            {
                LogManger.Instance.WriteLog("用户注册存储过程wanmei_pro_register返回:" + rtn_err.Value);
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Login(UserLoginModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms = 
            {
                new SqlParameter("@username",model.UserName),
                new SqlParameter("@password",MD5Manager.MD5Encrypt(model.PassWord)),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "wanmei_pro_login", parms);
            if (rtn_err.Value != null)
            {
                LogManger.Instance.WriteLog("用户登录存储过程wanmei_pro_login返回:" + rtn_err.Value);
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }
        /// <summary>
        /// 通过用户名检测用户是否存在
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool IsExistUser(string userName)
        {
            bool result = false;
            SqlParameter[] parms = 
            { 
                new SqlParameter("@username",userName)
            };
            string sql = "select Id from C_User where UserName=@username";
            object obj = new SqlManager().ExecuteScalar(sql, parms);
            if (obj != null && Convert.ToInt32(obj) > 0)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// 设置昵称的时候，同时添加这个用户的当前角色
        /// </summary>
        /// <param name="nick"></param>
        /// <param name="un"></param>
        /// <param name="heroId"></param>
        /// <returns></returns>
        public int SetRole(string nick, string un, int heroId)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms = 
            {
                new SqlParameter("@nick",nick),
                new SqlParameter("@un",un),
                new SqlParameter("@heroId",heroId),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "wanmei_pro_setrole", parms);
            if (rtn_err.Value != null)
            {
                LogManger.Instance.WriteLog("用户登录存储过程wanmei_pro_login返回:" + rtn_err.Value);
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }
        #endregion

        #region IWanMeiService 成员

        public void Dispose()
        {

        }

        #endregion

       

       

       
    }
}
