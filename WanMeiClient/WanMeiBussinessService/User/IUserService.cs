﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WanMeiModel;

namespace WanMeiBussinessService
{
    public interface IUserService:IWanMeiService
    {
        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Register(UserRegisterModel model);
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Login(UserLoginModel model);
        /// <summary>
        /// 通过用户名检测用户是否存在
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        bool IsExistUser(string userName);
        /// <summary>
        /// 设置昵称的时候，同时添加这个用户的当前角色
        /// </summary>
        /// <param name="nick"></param>
        /// <param name="un"></param>
        /// <param name="heroId"></param>
        /// <returns></returns>
        int SetRole(string nick, string un, int heroId);

    }
}
