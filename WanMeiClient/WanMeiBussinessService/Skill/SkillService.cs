﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WanMeiCommon;
using WanMeiModel;
using WanMeiSqlTools;

namespace WanMeiBussinessService
{
    public class SkillService : ISkillService
    {


        #region ISkillService 成员
        /// <summary>
        /// 获取角色状态技能列表(当前角色可以学习的所有技能)
        /// </summary>
        /// <param name="heroId"></param>
        /// <param name="un"></param>
        /// <returns></returns>
        public List<StatusSkillItem> GetStatusSkillList(int heroId, string un)
        {
            var result = new List<StatusSkillItem>();
            SqlParameter[] parms = 
                 {
                    new SqlParameter("@un",SecretClass.DecryptQueryString(un)),
                    new SqlParameter("@heroId",heroId),
                 };
            StringBuilder sb = new StringBuilder();
            sb.Append("select cd.Id, SkillName,ISNULL(cr.Id,0) as IsStudy,cd.SkillType,SkillIcon,SkillIntroduce,SkillValue,IntervalTime,NeedMagic,GroupUpValue,CdTime,SkillTime,NeedLevel,ISNULL(cr.SkillLevel,0) AS SkillLevel,SkillOption,PassivePlace,SkillImg,SpecialEffectType from C_Skill cd left join C_RoleSkill cr on cd.Id = cr.SkillId and cr.HeroId=@heroId and cr.UserName=@un where cd.PublicStatus=1 and cd.HeroId=@heroId order by cd.NeedLevel");
            var dt = new SqlManager().ExecuteDataset(CommandType.Text, sb.ToString(), parms);
            if (dt != null && dt.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                {
                    result.Add(new StatusSkillItem()
                    {
                        Id = Convert.ToInt32(dt.Tables[0].Rows[i]["Id"]),
                        HeroId = heroId,
                        Un= un,
                        SkillName = dt.Tables[0].Rows[i]["SkillName"].ToString(),
                        SpecialEffectType = Convert.ToInt32(dt.Tables[0].Rows[i]["SpecialEffectType"]),
                        IsStudy = Convert.ToInt32(dt.Tables[0].Rows[i]["IsStudy"])>0?1:0,
                        SkillType = Convert.ToInt32(dt.Tables[0].Rows[i]["SkillType"]),
                        SkillIcon = string.Format("{0}{1}/{2}", ConfigSettings.Instance.GameProImgHost, SkillUpload.Instance.FolderName, dt.Tables[0].Rows[i]["SkillIcon"].ToString()),
                        SkillIntroduce = dt.Tables[0].Rows[i]["SkillIntroduce"].ToString(),
                        IntervalTime = Convert.ToInt32(dt.Tables[0].Rows[i]["IntervalTime"]),
                        NeedMagic = Convert.ToInt32(dt.Tables[0].Rows[i]["NeedMagic"]),
                        GroupUpValue = Convert.ToInt32(dt.Tables[0].Rows[i]["GroupUpValue"]),
                        CdTime = Convert.ToInt32(dt.Tables[0].Rows[i]["CdTime"]),
                        SkillTime = Convert.ToInt32(dt.Tables[0].Rows[i]["SkillTime"]),
                        NeedLevel = Convert.ToInt32(dt.Tables[0].Rows[i]["NeedLevel"]),
                        SkillLevel = Convert.ToInt32(dt.Tables[0].Rows[i]["SkillLevel"]),
                        SkillValue = Convert.ToInt32(dt.Tables[0].Rows[i]["SkillValue"]),
                        SkillOption = Convert.ToInt32(dt.Tables[0].Rows[i]["SkillOption"]),
                        PassivePlace = Convert.ToInt32(dt.Tables[0].Rows[i]["PassivePlace"]),
                        SkillImg = string.Format("{0}{1}/{2}", ConfigSettings.Instance.GameProImgHost, SkillUpload.Instance.FolderName, dt.Tables[0].Rows[i]["SkillImg"].ToString()),
                    });
                }

            }
            return result;
        }

        /// <summary>
        /// 学习或者升级技能
        /// </summary>
        /// <param name="skillId"></param>
        /// <param name="heroId"></param>
        /// <param name="un"></param>
        /// <returns></returns>
        public int Study(int skillId, int heroId, string un, long needPotential)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms = 
            {
                new SqlParameter("@skillId",skillId),
                new SqlParameter("@heroId",heroId),
                new SqlParameter("@un",un),
                new SqlParameter("@needPotential",needPotential),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "wanmei_proc_studyskill", parms);
            if (rtn_err.Value != null)
            {
                LogManger.Instance.WriteLog("学习或者升级技能存储过程返回:" + rtn_err.Value);
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        #endregion

        #region IWanMeiService 成员

        public void Dispose()
        {

        }

        #endregion
    }
}
