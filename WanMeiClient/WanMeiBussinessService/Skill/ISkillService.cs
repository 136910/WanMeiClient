﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WanMeiModel;

namespace WanMeiBussinessService
{
    public interface ISkillService:IWanMeiService
    {
        /// <summary>
        /// 获取角色状态技能列表(当前角色可以学习的所有技能)
        /// </summary>
        /// <param name="heroId"></param>
        /// <param name="un"></param>
        /// <returns></returns>
        List<StatusSkillItem> GetStatusSkillList(int heroId, string un);
        /// <summary>
        /// 学习或者升级技能
        /// </summary>
        /// <param name="skillId"></param>
        /// <param name="heroId"></param>
        /// <param name="un"></param>
        /// <returns></returns>
        int Study(int skillId, int heroId, string un, long needPotential);
    }
}
