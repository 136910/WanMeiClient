﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WanMeiModel;

namespace WanMeiBussinessService
{
    public interface IHeroService:IWanMeiService
    {
        /// <summary>
        /// 获取角色选择界面，所有英雄对应的当前玩家的相关信息
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        List<ChoseHeroItem> GetHerosByUserName(string userName);
        /// <summary>
        /// 获取当前角色装备上的装备信息列表
        /// </summary>
        /// <param name="un"></param>
        /// <param name="heroid"></param>
        /// <returns></returns>
        List<HeroEquipmentItem> GetCurrentEquipments(string un, int heroid);
        /// <summary>
        /// 获取当前角色背包道具信息
        /// </summary>
        /// <param name="un"></param>
        /// <param name="heroid"></param>
        /// <returns></returns>
        HeroBackPackPropManager GetCurrentBackPackInfos(string un, int heroid);
        /// <summary>
        /// 将背包装备脱下
        /// </summary>
        /// <param name="un"></param>
        /// <param name="backId"></param>
        /// <returns></returns>
        bool TakeOff(string un, int backId);
        /// <summary>
        /// 背包的装备，装备上
        /// </summary>
        /// <param name="un"></param>
        /// <param name="backId"></param>
        /// <param name="heroId"></param>
        /// <returns></returns>
        int TakeOn(string un, int backId, int heroId);

        /// <summary>
        /// 获取当前账户的当前选择的角色信息+装备列表
        /// </summary>
        /// <param name="un"></param>
        /// <param name="heroId"></param>
        /// <returns></returns>
        UserInfoModelResult GetUserInfoModel(string un, int heroId);
    }
}
