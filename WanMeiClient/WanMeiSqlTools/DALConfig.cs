﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiSqlTools
{
    public class DALConfig
    {
        public static string GetConnStr(string connKey)
        {
            return ConfigurationManager.AppSettings[connKey];
        }

        public static string ConnectionString
        {
            get { return ConfigurationManager.AppSettings["ConnectionStrings"]; }//form System.Configuration.dll
        }

        public static string ErpConnectionString
        {
            get { return ConfigurationManager.AppSettings["ErpConnectonStrings"]; }
        }
    }
}
