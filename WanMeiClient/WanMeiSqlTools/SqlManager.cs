﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiSqlTools
{
    public class SqlManager : SqlHelper
    {
        public SqlManager()
        {
            _conn = DALConfig.ConnectionString;
        }
        public SqlManager(string connKey)
        {
            _conn = DALConfig.GetConnStr(connKey);
        }
        private string _conn = string.Empty;

        public SqlParameter GetRtnParameter()
        {
            SqlParameter rtn_err = new SqlParameter("rtn_err", SqlDbType.Int);
            rtn_err.Direction = ParameterDirection.Output;
            return rtn_err;
        }
        /// <summary>
        /// 查询单个数据值
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <returns></returns>
        public object ExecuteScalar(string sqlStr, params SqlParameter[] commandParameters)
        {
            return base.ExecuteScalar(_conn, CommandType.Text, sqlStr, null);
        }

        /// <summary>
        /// 非查询操作，commandType可能是存储过程或SQL语句
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(CommandType type, string sqlOrStoredProcedure, params SqlParameter[] commandParameters)
        {
            return base.ExecuteNonQuery(_conn, type, sqlOrStoredProcedure, commandParameters);
        }
        /// <summary>
        /// 查询结果集
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public DataSet ExecuteDataset(CommandType type, string sqlOrStoredProcedure, params SqlParameter[] commandParameters)
        {
            return base.ExecuteDataset(_conn, type, sqlOrStoredProcedure, commandParameters);
        }
        /// <summary>
        /// 查询结果集
        /// </summary>
        /// <param name="cmdType"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public SqlDataReader ExecuteReader(CommandType cmdType, string sqlOrStoredProcedure, params SqlParameter[] commandParameters)
        {
            return base.ExecuteReader(_conn, cmdType, sqlOrStoredProcedure, commandParameters);
        }
        /// <summary>
        /// 事务执行
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paramers"></param>
        /// <returns></returns>
        public bool ExecTransactionAsUpdate(string[] sql, List<SqlParameter[]> paramers = null)
        {
            return base.ExecTransactionAsUpdate(sql, _conn, paramers);
        }
    }
}
