﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiDataAccess
{
    public class EfRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// 数据上下文
        /// </summary>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        private readonly DbContext _context;

        /// <summary>
        /// 数据操作实体对象
        /// </summary>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        private readonly DbSet<TEntity> _dbSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="EfRepository{T}" /> class.
        /// </summary>
        /// <param name="dbContext">The dbContext</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public EfRepository(DbContext dbContext)
        {
            _context = dbContext;
            _dbSet = _context.Set<TEntity>();
        }

        /// <summary>
        /// 增加单个实体
        /// </summary>
        /// <param name="entity">The entity</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public TEntity Insert(TEntity entity)
        {

            _dbSet.Add(entity);
            // _context.SaveChanges();
            return entity;
        }



        /// <summary>
        /// 根据主键id 删除实体
        /// </summary>
        /// <param name="id">The id</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public void Delete(object id)
        {
            var entity = _dbSet.Find(id);
            Delete(entity);
        }

        /// <summary>
        /// 根据实体 删除实体
        /// </summary>
        /// <param name="entity">The entity</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public void Delete(TEntity entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
        }



        /// <summary>
        /// 根据实体 修改实体
        /// </summary>
        /// <param name="entity">The entity</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            // _context.SaveChanges();
        }

        /// <summary>
        /// 执行sql
        /// </summary>
        /// <param name="sql">The sql</param>
        /// <param name="parameters">The parameters</param>
        /// <returns>
        /// IEnumerable{`0}
        /// </returns>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public IEnumerable<TEntity> ExecuteSqlQuery(string sql, params object[] parameters)
        {
            return _dbSet.SqlQuery(sql, parameters);
        }

        /// <summary>
        /// 根据id 获取实体
        /// </summary>
        /// <param name="id">The id</param>
        /// <returns>
        /// `0
        /// </returns>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public TEntity GetById(object id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// 根据查询条件获取单个实体
        /// </summary>
        /// <param name="expression">The expression</param>
        /// <returns>
        /// `0
        /// </returns>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public TEntity FindSingleByExpression(Expression<Func<TEntity, bool>> expression)
        {
            return _dbSet.FirstOrDefault(expression);
        }

        /// <summary>
        /// 根据查询条件，排序，是否包含子查询，返回集合实体
        /// </summary>
        /// <param name="filter">The filter</param>
        /// <param name="orderBy">The orderBy</param>
        /// <param name="includeProperties">The includeProperties</param>
        /// <returns>
        /// The IQueryable{`0}
        /// </returns>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return orderBy != null ? orderBy(query).ToList() : query.ToList();
        }


        /// <summary>
        /// 返回 iqueryable 查询 语句式查询
        /// </summary>
        /// <value>
        /// The table.
        /// </value>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public IQueryable<TEntity> Table
        {
            get { return _dbSet; }
        }

        /// <summary>
        /// 保存修改
        /// </summary>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// 字段disposed
        /// </summary>
        /// 创建者：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        private bool _disposed;

        /// <summary>
        /// The Void
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        /// 创建者：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }

        /// <summary>
        /// 执行与释放或重置非托管资源相关的应用程序定义的任务。
        /// </summary>
        /// 创建者：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
