//------------------------------------------------------------------------------
// <auto-generated>
//    此代码是根据模板生成的。
//
//    手动更改此文件可能会导致应用程序中发生异常行为。
//    如果重新生成代码，则将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace WanMeiDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class C_Hero
    {
        public int Id { get; set; }
        public string HeroName { get; set; }
        public string Introduce { get; set; }
        public Nullable<int> InitHealthPoint { get; set; }
        public Nullable<int> InitMagicPower { get; set; }
        public Nullable<int> InitArmor { get; set; }
        public Nullable<int> InitCriticalValue { get; set; }
    }
}
