﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiDataAccess
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        /// <summary>
        /// 增加单个实体
        /// </summary>
        /// <param name="entity">The entity</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        TEntity Insert(TEntity entity);
        /// <summary>
        /// 根据主键id 删除实体
        /// </summary>
        /// <param name="id">The id</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        void Delete(object id);
        /// <summary>
        /// 根据实体 删除实体
        /// </summary>
        /// <param name="entity">The entity</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        void Delete(TEntity entity);
        /// <summary>
        /// 根据实体 修改实体
        /// </summary>
        /// <param name="entity">The entity</param>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        void Update(TEntity entity);

        /// <summary>
        /// 执行sql
        /// </summary>
        /// <param name="sql">The sql</param>
        /// <param name="parameters">The parameters</param>
        /// <returns>
        /// IEnumerable{`0}
        /// </returns>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        IEnumerable<TEntity> ExecuteSqlQuery(string sql, params object[] parameters);

        /// <summary>
        /// 根据id 获取实体
        /// </summary>
        /// <param name="id">The id</param>
        /// <returns>
        /// `0
        /// </returns>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        TEntity GetById(object id);

        /// <summary>
        /// 根据查询条件获取单个实体
        /// </summary>
        /// <param name="expression">The expression</param>
        /// <returns>
        /// `0
        /// </returns>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        TEntity FindSingleByExpression(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// 根据查询条件，排序，是否包含子查询，返回集合实体
        /// </summary>
        /// <param name="filter">The filter</param>
        /// <param name="orderBy">The orderBy</param>
        /// <param name="includeProperties">The includeProperties</param>
        /// <returns>
        /// The IQueryable{`0}
        /// </returns>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");

        /// <summary>
        ///  返回 iqueryable 查询 语句式查询
        /// </summary>
        /// <value>
        /// The table.
        /// </value>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        IQueryable<TEntity> Table { get; }

        /// <summary>
        /// 保存修改
        /// </summary>
        /// 创建者：
        /// 创建日期：
        /// 修改者：
        /// 修改时间：
        /// ----------------------------------------------------------------------------------------
        void SaveChanges();
    }
}
