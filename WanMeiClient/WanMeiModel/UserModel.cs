﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiModel
{
    /// <summary>
    /// 选择角色界面的业务模型
    /// </summary>
    public class ChoseHeroItem
    {
        public int HeroId { get; set; }
        public string HeroIntroduce { get; set; }
        public string HeroName { get; set; }
        /// <summary>
        /// 角色昵称
        /// </summary>
        public string RoleNick { get; set; }
        /// <summary>
        /// 进入过游戏一次后，就有了
        /// </summary>
        public int IsHasNick { get; set; }
        public int GameLevel { get; set; }
        /// <summary>
        /// 等级对应的称号
        /// </summary>
        public string GameLevelTitle { get; set; }
    }
    /// <summary>
    /// 角色基本状态+装备总信息，装备集合
    /// </summary>
    public class UserInfoModelResult
    {
        public UserInfoModelResult()
        {
            this.UserRoleModel = new UserRoleModel();
            this.Equipments = new List<HeroEquipmentItem>();
            this.UserRoleBaseAttributeItem = new UserRoleBaseAttributeItem();
        }
        public UserInfoModelResult(UserRoleModel UserRoleModel, List<HeroEquipmentItem> Equipments, UserRoleBaseAttributeItem UserRoleBaseAttributeItem)
        {
            this.UserRoleModel = UserRoleModel;
            this.Equipments = Equipments;
            this.UserRoleBaseAttributeItem = UserRoleBaseAttributeItem;
        }
        /// <summary>
        /// 角色主要战斗属性（算上装备）
        /// </summary>
        public UserRoleModel UserRoleModel { get; set; }
        public List<HeroEquipmentItem> Equipments { get; set; }
        /// <summary>
        /// 角色一些基本属性模型
        /// </summary>
        public UserRoleBaseAttributeItem UserRoleBaseAttributeItem { get; set; }
    }

    /// <summary>
    /// 用户当前角色信息模型
    /// </summary>
    public class UserRoleModel
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 角色所属账号
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 角色昵称
        /// </summary>
        public string RoleNick { get; set; }
        /// <summary>
        /// 游戏等级
        /// </summary>
        public int GameLevel { get; set; }
        /// <summary>
        /// 职业等级职称
        /// </summary>
        public string LevelTitle { get; set; }
        /// <summary>
        /// VIP等级
        /// </summary>
        public int VipLevel { get; set; }
        /// <summary>
        ///疲劳值
        /// </summary>
        public int DepositValue { get; set; }
        /// <summary>
        /// 当前角色选取的职业ID
        /// </summary>
        public int HeroId { get; set; }
        /// <summary>
        /// 当前角色生命值
        /// </summary>
        public int HealthPoint { get; set; }
        /// <summary>
        /// 当前角色魔法值
        /// </summary>
        public int MagicPower { get; set; }
        /// <summary>
        /// 当前角色护甲
        /// </summary>
        public int Armor { get; set; }
        /// <summary>
        /// 当前角色暴击值
        /// </summary>
        public int CriticalValue { get; set; }
        /// <summary>
        /// 当前角色，当前等级的经验值  经验=60+（等级-1）的3次方 
        /// </summary>
        public long ExValue { get; set; }
        /// <summary>
        /// 角色当前等级所需升级经验
        /// </summary>
        public long NeedExValue
        {
            get
            {
                return 60 + (this.GameLevel - 1) * (this.GameLevel - 1) * (this.GameLevel - 1);
            }
        }
        /// <summary>
        /// 当前角色的历练值
        /// </summary>
        public long PotentialValue { get; set; }
        /// <summary>
        /// 金币
        /// </summary>
        public long Money { get; set; }
        /// <summary>
        /// 总战斗力
        /// </summary>
        public int Power
        {
            get
            {
                int res = this.ApValue + this.HealthPoint + this.MagicPower + this.Armor + this.CriticalValue;
                return res;
            }
            set
            { }
        }

        public int ApValue { get; set; }
    }

    public class UserRoleBaseAttributeItem
    { 
        /// <summary>
        /// 游戏等级
        /// </summary>
        public int GameLevel { get; set; }
        /// <summary>
        /// 当前角色选取的职业ID
        /// </summary>
        public int HeroId { get; set; }
        /// <summary>
        /// 当前角色的历练值
        /// </summary>
        public long PotentialValue { get; set; }
        /// <summary>
        /// 金币
        /// </summary>
        public long Money { get; set; }
        /// <summary>
        /// 当前角色生命值
        /// </summary>
        public int HealthPoint { get; set; }
        /// <summary>
        /// 当前角色魔法值
        /// </summary>
        public int MagicPower { get; set; }
        /// <summary>
        /// 当前角色护甲
        /// </summary>
        public int Armor { get; set; }
        public int ApValue { get; set; }
        /// <summary>
        /// 当前角色暴击值
        /// </summary>
        public int CriticalValue { get; set; }
        /// <summary>
        /// 当前角色，当前等级的经验值  经验=60+（等级-1）的3次方 
        /// </summary>
        public long ExValue { get; set; }
        /// <summary>
        /// 角色当前等级所需升级经验
        /// </summary>
        public long NeedExValue
        {
            get
            {
                return 60 + (this.GameLevel - 1) * (this.GameLevel - 1) * (this.GameLevel - 1);
            }
        }
    }

    /// <summary>
    /// 用户注册模型
    /// </summary>
    public class UserRegisterModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string RePassWord { get; set; }
        public string IntroducerNick { get; set; }
    }
    /// <summary>
    /// 用户登录模型
    /// </summary>
    public class UserLoginModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
}
