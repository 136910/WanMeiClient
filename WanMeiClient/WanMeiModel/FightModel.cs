﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiModel
{
    class FightModel
    {
    }
    /// <summary>
    /// 副本战斗，怪攻击的返回模型
    /// </summary>
    public class RaidMonsterFightResultModel
    {
        /// <summary>
        /// 剩余
        /// </summary>
        public int HealthValue { get; set; }
        /// <summary>
        /// 角色是否死亡
        /// </summary>
        public int IsDead { get; set; }
        /// <summary>
        /// 角色受到的伤害
        /// </summary>
        public int HurtValue { get; set; }
    }

    /// <summary>
    /// 副本一次技能攻击后，返回的业务模型
    /// </summary>
    public class RaidFigthResultModel
    {
        public RaidFigthResultModel(RaidFightRoleItem Role, List<RaidFightMonsterItem> Monsters)
        {
            this.Role = Role;
            this.Monsters = Monsters;
        }
        public RaidFightRoleItem Role { get; set; }
        public List<RaidFightMonsterItem> Monsters { get; set; }
        /// <summary>
        /// 当前战斗状态。0战斗中，1.胜利。2失败
        /// </summary>
        public int FightStatus { get; set; }

    }
    public class RaidDotEventResultModel
    {
        /// <summary>
        /// 当前DOT怪的状态，由于多种DOT并发执行，获取的模型可能为空，这时，给返回-1，客户端进行判断
        /// </summary>
        public int MonsterStatus { get; set; }

        public int MonsterId { get; set; }
        /// <summary>
        /// 剩余
        /// </summary>
        public int HealthValue { get; set; }
        /// <summary>
        /// 是否死亡
        /// </summary>
        public int IsDead { get; set; }
        /// <summary>
        /// 是否暴击
        /// </summary>
        public int IsBj { get; set; }
        /// <summary>
        /// 受到的伤害
        /// </summary>
        public int HurtValue { get; set; }
        /// <summary>
        ///治疗的值
        /// </summary>
        public int BeTreatedValue { get; set; }
        /// <summary>
        /// 当前战斗状态。0战斗中，1.胜利。2失败
        /// </summary>
        public int FightStatus { get; set; }
    }

    /// <summary>
    /// 副本一次技能攻击后，单个怪的状态
    /// </summary>
    public class RaidFightMonsterItem
    {
        public int Id { get; set; }
        /// <summary>
        /// 剩余
        /// </summary>
        public int HealthValue { get; set; }
        /// <summary>
        /// 剩余魔力
        /// </summary>
        public int MagicValue { get; set; }
        /// <summary>
        /// 是否死亡
        /// </summary>
        public int IsDead { get; set; }
        /// <summary>
        /// 受到的伤害
        /// </summary>
        public int HurtValue { get; set; }
        /// <summary>
        /// 是否暴击
        /// </summary>
        public int IsBj { get; set; }
    }

    /// <summary>
    /// 副本一次技能攻击后，角色的状态
    /// </summary>
    public class RaidFightRoleItem
    {
        /// <summary>
        /// 剩余
        /// </summary>
        public int HealthValue { get; set; }
        /// <summary>
        /// 剩余魔力
        /// </summary>
        public int MagicValue { get; set; }
        /// <summary>
        /// 是否死亡
        /// </summary>
        public int IsDead { get; set; }
        /// <summary>
        /// 受到的伤害
        /// </summary>
        public int HurtValue { get; set; }
        /// <summary>
        ///治疗的值
        /// </summary>
        public int BeTreatedValue { get; set; }
        /// <summary>
        /// 是否暴击
        /// </summary>
        public int IsBj { get; set; }
    }
    /// <summary>
    /// 副本战斗结果处理返回的业务模型
    /// </summary>
    public class RaidFightResultModel
    {
        public RaidFightResultModel()
        {
            Props = new List<RaidFightPropItem>();
        }
        /// <summary>
        ///服务器真实战斗结果1胜利，-1失败
        /// </summary>
        public int FightStatus { get; set; }
        /// <summary>
        /// 本次副本获得的经验值
        /// </summary>
        public int ExValues { get; set; }
        /// <summary>
        /// 本次副本获得的历练值
        /// </summary>
        public int PotentialValues { get; set; }
        /// <summary>
        /// 本次副本获得的金币数
        /// </summary>
        public int Money { get; set; }
        /// <summary>
        /// 本次副本获得的装备道具等
        /// </summary>
        public List<RaidFightPropItem> Props { get; set; }
        /// <summary>
        /// 角色是否升级
        /// </summary>
        public int IsLevelUp { get; set; }
        public int GameLevel { get; set; }
        /// <summary>
        /// 存储过程返回参数值
        /// </summary>
        public int ProcResult { get; set; }

    }
    public class RaidFightPropItem
    {
        public string PropName { get; set; }
        public int DropRate { get; set; }
        public int Num { get; set; }
        public string ImgPath { get; set; }
        public int ProType { get; set; }
        public int SalePrice { get; set; }
        public int Place { get; set; }
        public int RoleId { get; set; }
        public int ProId { get; set; }
        /// <summary>
        /// 背包里是否存在该物品
        /// </summary>
        public int IsHas { get; set; }
    }
}
