﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiModel
{
    class RaidModel
    {
    }
    public class RaidItem
    {
        public int Id { get; set; }
        public string RaidName { get; set; }
        public int NeedLevel { get; set; }
        public string Introduce { get; set; }
        public string ImgPath { get; set; }
    }
    /// <summary>
    /// 加载副本场景所需数据模型，这些数据只是当场战斗的数据信息，战斗完成，删除
    /// </summary>
    public class RaidFightManagerModel
    {
        public RaidFightManagerModel(List<StatusSkillItem> RoleSkills, List<MonsterModel> Monsters, UserRoleModel CurrentRoleInfo)
        {
            this.RoleSkills = RoleSkills;
            this.Monsters = Monsters;
            this.CurrentRoleInfo = CurrentRoleInfo;
        }
        /// <summary>
        /// 玩家当前已学的技能列表
        /// </summary>
        public List<StatusSkillItem> RoleSkills { get; set; }
        /// <summary>
        /// 当前场景的怪物模型列表
        /// </summary>
        public List<MonsterModel> Monsters { get; set; }
        /// <summary>
        /// 当前角色数据信息模型(所有战斗属性都是装备+基本+被动技能增幅)
        /// </summary>
        public UserRoleModel CurrentRoleInfo { get; set; }
        /// <summary>
        /// 当前副本战斗的唯一标识
        /// </summary>
        public string FightId { get; set; }
        /// <summary>
        /// 战斗结果0战斗中，1胜利，-1失败
        /// </summary>
        public int FightStatus { get; set; }

        public int RaidId { get; set; }

    }
        
}
