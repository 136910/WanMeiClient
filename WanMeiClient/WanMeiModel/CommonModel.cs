﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiModel
{
    class CommonModel
    {
    }
    public class InputArgs
    {
        public InputArgs(int heroId, string un)
        {
            this.HeroId = heroId;
            this.Un = un;
        }
        public int HeroId { get; set; }
        public string Un { get; set; }
    }

}
