﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiModel
{
    public class MonsterModel
    {
        public int Id { get; set; }
        /// <summary>
        /// 怪名称
        /// </summary>
        public string MonsterName { get; set; }
        /// <summary>
        /// 怪血量
        /// </summary>
        public int HeathValue { get; set; }
        /// <summary>
        /// 怪魔力值
        /// </summary>
        public int MagicValue { get; set; }
        /// <summary>
        /// 怪附带的经验值
        /// </summary>
        public int ExValue { get; set; }
        /// <summary>
        /// 怪等级
        /// </summary>
        public int MonsterLevel { get; set; }
        /// <summary>
        /// 怪图片
        /// </summary>
        public string ImgPath { get; set; }
        /// <summary>
        /// 怪附带的历练值
        /// </summary>
        public int PotentialValue { get; set; }
        /// <summary>
        /// 怪携带的金币数
        /// </summary>
        public int Money { get; set; }
        /// <summary>
        /// 是否拥有技能
        /// </summary>
        public int IsHasSkill { get; set; }
        /// <summary>
        /// 技能类型1伤害，2治疗
        /// </summary>
        public int SkillType { get; set; }
        /// <summary>
        /// 技能图片
        /// </summary>
        public string SkillImg { get; set; }
        /// <summary>
        /// 技能CD
        /// </summary>
        public int SkillCd { get; set; }
        /// <summary>
        /// 技能造成的数值
        /// </summary>
        public int SkillValue { get; set; }
        /// <summary>
        /// 怪基本伤害
        /// </summary>
        public int Ap { get; set; }
        /// <summary>
        /// 普通攻击图片
        /// </summary>
        public string ApImg { get; set; }
        /// <summary>
        /// 攻击间隔秒数
        /// </summary>
        public int ApInteral { get; set; }
    }
}
