﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiModel
{
    public class FightSkillModel
    {
    }
    /// <summary>
    /// 角色状态中的技能模型
    /// </summary>
    public class StatusSkillItem
    {
        /// <summary>
        /// 技能ID
        /// </summary>
        public int Id { get; set; }

        public int HeroId { get; set; }

        public string Un { get; set; }
        public string SkillImg { get; set; }
        /// <summary>
        /// 1.单目标有路径移动攻击，3.全体攻击，左右冲击，4，单目标无路径攻击，5,多目标无路径攻击
        /// </summary>
        public int SpecialEffectType { get; set; }
        /// <summary>
        /// 技能名称
        /// </summary>
        public string SkillName { get; set; }
        /// <summary>
        /// 当前角色是否学习
        /// </summary>
        public int IsStudy { get; set; }
        /// <summary>
        /// 所属技能表1.一次性技能，2持续性技能，3被动技能
        /// </summary>
        public int SkillType { get; set; }
        /// <summary>
        /// 技能图标
        /// </summary>
        public string SkillIcon { get; set; }
        /// <summary>
        /// 技能介绍
        /// </summary>
        public string SkillIntroduce { get; set; }
      
        /// <summary>
        /// 类型为持续性的时候的每几秒跳一次，其他类型为0
        /// </summary>
        public int IntervalTime { get; set; }
        /// <summary>
        /// 需要魔力
        /// </summary>
        public int NeedMagic { get; set; }
        /// <summary>
        /// 技能每等级增加的数值
        /// </summary>
        public int GroupUpValue { get; set; }
        /// <summary>
        /// 技能冷却时间，被动为0
        /// </summary>
        public int CdTime { get; set; }
        /// <summary>
        /// 持续性技能的持续时间，其他为0
        /// </summary>
        public int SkillTime { get; set; }
        /// <summary>
        /// 学习该技能需要的基础等级
        /// </summary>
        public int NeedLevel { get; set; }
        /// <summary>
        /// 学习该技能需要的实际等级
        /// </summary>
        public int OptionNeedLevel {
            get
            { 
                //根据技能等级提升
                if (this.SkillLevel <= 0)
                {
                    return this.NeedLevel;
                }
                return NeedLevel + this.SkillLevel;
            }
        }

        /// <summary>
        /// 该技能等级，没学为0
        /// </summary>
        public int SkillLevel { get; set; }
        /// <summary>
        /// 1伤害，2治疗
        /// </summary>
        public int SkillOption { get; set; }
        /// <summary>
        /// 基础技能数值，伤害或者治疗
        /// </summary>
        public int SkillValue { get; set; }
        /// <summary>
        /// 当前等级实际业务技能数值，伤害或者治疗
        /// </summary>
        /// 
        public int SkillBussinessValue
        {
            get
            {
                //公式为（技能等级-1）*成长值+基础数值
                if (this.SkillLevel <= 1)
                    return this.SkillValue;
                return this.SkillValue + (this.SkillLevel - 1) * this.GroupUpValue;
            }
        }
        /// <summary>
        /// 学习下一等级需要的历练值
        /// </summary>
        public int NeedPotentialValue
        {
            get
            {
                if (this.SkillLevel <= 1)
                    return 60;
                return 60 + (this.SkillLevel - 1) * (this.SkillLevel - 1) * (this.SkillLevel - 1);
                //公式：60+（技能等级-1）的3次方
            }
        }
        /// <summary>
        /// 被动针对的属性1.攻击，2护甲，3暴击。4血量
        /// </summary>
        public int PassivePlace { get; set; }
    }

    
}
