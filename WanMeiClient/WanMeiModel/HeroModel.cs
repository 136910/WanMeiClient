﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiModel
{
    public class HeroModel
    {

    }
    /// <summary>
    /// 用户角色背包道具模型
    /// </summary>
    public class HeroBackPackPropManager
    {
        public HeroBackPackPropManager()
        {
            this.Equipments = new List<HeroEquipmentItem>();
            this.Gemstones = new List<HeroGemstoneItem>();
            this.Materials = new List<HeroMaterialItem>();
        }
        public List<HeroEquipmentItem> Equipments { get; set; }
        public List<HeroGemstoneItem> Gemstones { get; set; }
        public List<HeroMaterialItem> Materials { get; set; }
    }
    /// <summary>
    /// 背包角色材料模型
    /// </summary>
    public class HeroMaterialItem
    {
        /// <summary>
        /// 背包信息ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 道具类型
        /// </summary>
        public int PropType { get; set; }
        /// <summary>
        /// 装备ID
        /// </summary>
        public int PropId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Num { get; set; }
        /// <summary>
        /// 材料名称
        /// </summary>
        public string MaterialName { get; set; }
        /// <summary>
        /// 介绍
        /// </summary>
        public string Introduce { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string ImgPath { get; set; }
        /// <summary>
        /// 出售金额
        /// </summary>
        public int Money { get; set; }

    }

    /// <summary>
    /// 背包角色宝石模型
    /// </summary>
    public class HeroGemstoneItem
    {
        /// <summary>
        /// 背包信息ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 道具类型
        /// </summary>
        public int PropType { get; set; }
        /// <summary>
        /// 装备ID
        /// </summary>
        public int PropId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Num { get; set; }
        /// <summary>
        /// 材料名称
        /// </summary>
        public string GemstoneName { get; set; }
        /// <summary>
        /// 介绍
        /// </summary>
        public string Introduce { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string ImgPath { get; set; }
        /// <summary>
        /// 出售金额
        /// </summary>
        public int Money { get; set; }

    }

    /// <summary>
    /// 用户角色装备模型，背包装备模型
    /// </summary>
    public class HeroEquipmentItem
    {
        /// <summary>
        /// 背包信息ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 道具类型
        /// </summary>
        public int PropType { get; set; }
        /// <summary>
        /// 装备ID
        /// </summary>
        public int PropId { get; set; }
        /// <summary>
        /// 售价
        /// </summary>
        public int SalePrice { get; set; }
        /// <summary>
        /// 强化等级
        /// </summary>
        public int EquipmentLevel { get; set; }
        /// <summary>
        /// 装备位置1头，2衣服，3武器，4护腕，5鞋，6戒指,7项链
        /// </summary>
        public int Place { get; set; }
        /// <summary>
        /// 装备名称
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 介绍
        /// </summary>
        public string Introduce { get; set; }
        /// <summary>
        /// 护甲增益
        /// </summary>
        public int InitArmor { get; set; }
        /// <summary>
        /// 暴击增益
        /// </summary>
        public int InitCriticalValue { get; set; }
        /// <summary>
        /// 血量增益
        /// </summary>
        public int InitHealthPoint { get; set; }
        /// <summary>
        ///魔力增益
        /// </summary>
        public int InitMagicPower { get; set; }
        /// <summary>
        /// 需要等级
        /// </summary>
        public int NeedGameLevel { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string ImgPath { get; set; }
        /// <summary>
        /// 攻击力收益
        /// </summary>
        public int ApValue { get; set; }

        public int Power
        {
            get
            {
                return this.ApValue + this.InitHealthPoint + this.InitMagicPower + this.InitArmor + this.InitCriticalValue;
            }
        }
        /// <summary>
        /// 品质1白色，2绿色，3蓝色，4紫色，5橙色，6粉色
        /// </summary>
        public int Quality { get; set; }
        /// <summary>
        /// 需要职业0不限制，1剑士，2魔法师
        /// </summary>
        public int NeedRole { get; set; }
        /// <summary>
        /// 出售金额
        /// </summary>
        public int Money { get; set; }
    }

    /// <summary>
    /// 角色获取经验后升级返回的最新角色信息模型
    /// </summary>
    public class LevelUpCalculationResultModel
    {
        /// <summary>
        /// 获取副本经验后，当前角色拥有的经验值
        /// </summary>
        public int CurrentLevel { get; set; }
        /// <summary>
        /// 获取副本经验值后当前角色拥有的经验值
        /// </summary>
        public long CurrentExValues { get; set; }
        /// <summary>
        /// 获取副本经验值后当前角色等级需要升级的经验值
        /// </summary>
        public long CurrentLevelNeedExValues { get; set; }

        public int HealthValue { get; set; }

        public int MagicValue { get; set; }

        public int Armor { get; set; }

        public int CriticalValue { get; set; }

    }
    public enum RoleAttribute
    {
        HealthValue = 1,
        MagicValue = 2,
        Armor = 3,
        CriticalValue = 4
    }
}
