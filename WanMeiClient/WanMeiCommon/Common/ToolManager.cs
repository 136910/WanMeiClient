﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace WanMeiCommon
{
    public static class ToolManager
    {
        private static Dictionary<string, string> _levelDic_JS = null;
        private static Dictionary<string, string> _levelDic_MFS = null;
        public static Dictionary<string, string> LevelDic_JS
        {
            get
            {
                if (_levelDic_JS == null)
                {
                    _levelDic_JS = new Dictionary<string, string>();
                    _levelDic_JS.Add("1-10", "见习剑士");
                }
                return _levelDic_JS;
            }
        }
        public static Dictionary<string, string> LevelDic_MFS
        {
            get
            {
                if (_levelDic_MFS == null)
                {
                    _levelDic_MFS = new Dictionary<string, string>();
                    _levelDic_MFS.Add("1-10", "见习魔法师");
                }
                return _levelDic_MFS;
            }
        }
        /// <summary>
        /// 根据英雄名字和等级获取对应的职称
        /// </summary>
        /// <param name="heroName"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static string GetGameLevelTitle(string heroName, int level)
        {
            string result = "";
            Dictionary<string, string> dic = null;
            if (heroName == "剑士")
            {
                dic = LevelDic_JS;
            }
            else if (heroName == "魔法师")
            {
                dic = LevelDic_MFS;
            }
            foreach (var item in dic)
            {
                int tempStart = Convert.ToInt32(item.Key.Split('-')[0]);
                int tempEnd = Convert.ToInt32(item.Key.Split('-')[1]);
                if (level >= tempStart && level <= tempEnd)
                {
                    result = item.Value;
                    break;
                }
            }
            return result;
        }
        /// <summary>
        /// 计算实际技能打出的伤害/治疗。算上暴击，10暴击=1%，效果翻倍
        /// </summary>
        /// <param name="vue"></param>
        /// <param name="bj"></param>
        /// <returns></returns>
        public static int GetRealSkillValue(int vue, int bj, out int isBj)
        {
            int res = vue;
            isBj = 0;
            int bjUpLimit = Convert.ToInt32(bj / GameProConfig.AttributeConversion.CriticalValue);
            var rd = new Random().Next(1, 100);
            if (rd <= bjUpLimit)
            {
                res = vue * 2;
                isBj = 1;
            }
            return res;
        }


        /// <summary>
        /// 根据枚举的值获取枚举名称
        /// </summary>
        /// <typeparam name="T">枚举类型</typeparam>
        /// <param name="status">枚举的值</param>
        /// <returns></returns>
        public static string GetEnumName<T>(this int status)
        {
            return Enum.GetName(typeof(T), status);
        }
        /// <summary>
        /// 根据文章数量计算需要的商品数量，每7个文章1个商品
        /// </summary>
        /// <param name="articleCount"></param>
        /// <returns></returns>
        public static int GetNumForNeedProCount(int articleCount)
        {
            var result = 0;
            if (articleCount == 0)
            {
                result = 0;
            }
            else if (articleCount <= 7)
            {
                result = 1;
            }
            else
            {
                result = Convert.ToInt32(articleCount / 7);
            }
            return result;
        }
        /// <summary>
        /// 使搜索结果包括关键词的部分高亮
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public static string HighKeywords(string keyword, string str)
        {
            if (!str.Contains(keyword))
            {
                return str;
            }
            int startIndex = str.IndexOf(keyword);
            string keyStr = "<font color='red'>" + keyword + "</font>";
            return str.Replace(str.Substring(startIndex, keyword.Length), keyStr);
        }


        /// <summary>
        /// 判断当前会话是否是来自手机
        /// </summary>
        /// <returns></returns>
        public static bool IsMoblie()
        {
            string agent = (HttpContext.Current.Request.UserAgent + "").ToLower().Trim();

            if (agent == "" ||
                agent.IndexOf("mobile") != -1 ||
                agent.IndexOf("mobi") != -1 ||
                agent.IndexOf("nokia") != -1 ||
                agent.IndexOf("samsung") != -1 ||
                agent.IndexOf("sonyericsson") != -1 ||
                agent.IndexOf("mot") != -1 ||
                agent.IndexOf("blackberry") != -1 ||
                agent.IndexOf("lg") != -1 ||
                agent.IndexOf("htc") != -1 ||
                agent.IndexOf("j2me") != -1 ||
                agent.IndexOf("ucweb") != -1 ||
                agent.IndexOf("opera mini") != -1 ||
                agent.IndexOf("mobi") != -1 ||
                agent.IndexOf("android") != -1 ||
                agent.IndexOf("iphone") != -1)
            {
                //终端可能是手机

                return true;

            }

            return false;
        }


        /// <summary>
        /// 获取枚举名称集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string[] GetNamesArr<T>()
        {
            return Enum.GetNames(typeof(T));
        }
        /// <summary>
        /// 将枚举转换成字典集合
        /// </summary>
        /// <typeparam name="T">枚举类型</typeparam>
        /// <returns></returns>
        public static Dictionary<string, int> getEnumDic<T>()
        {

            Dictionary<string, int> resultList = new Dictionary<string, int>();
            Type type = typeof(T);
            var strList = GetNamesArr<T>().ToList();
            foreach (string key in strList)
            {
                string val = Enum.Format(type, Enum.Parse(type, key), "d");
                resultList.Add(key, int.Parse(val));
            }
            return resultList;
        }
        /// <summary>
        /// 将阅读数转换成XX万
        /// </summary>
        /// <param name="readCount"></param>
        /// <returns></returns>
        public static string GetReadCountFomat(int readCount)
        {
            var result = string.Empty;
            if (readCount < 10000)
            {
                result = readCount.ToString();
            }
            else
            {
                result = string.Format("{0}万", Math.Round((double)readCount / (double)10000, 1)).ToString();
            }
            return result;
        }
        /// <summary>
        /// 首页推荐显示时间的规则，只显示带有时，分，秒，刚刚的文章时间
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static bool ShowLasterTimeAction(string time)
        {
            var result = true;
            if (!time.Contains("时") && !time.Contains("分") && !time.Contains("秒") && !time.Contains("刚刚"))
            {
                result = false;
            }
            return result;
        }


        /// 判断一个字符串是否为合法整数(不限制长度)
        /// </summary>
        /// <param name="s">字符串</param>
        /// <returns></returns>
        public static bool IsInteger(string s)
        {
            string pattern = @"^\d*$";
            return Regex.IsMatch(s, pattern);
        }
        /// <summary>
        /// 解密用户ID，文章ID等等
        /// </summary>
        /// <param name="encodeStr"></param>
        /// <returns></returns>
        public static int DecodeParamter(string encodeStr)
        {
            int result = 0;
            try
            {
                result = Convert.ToInt32(HttpRequestManager.Instance.Decode(encodeStr));
            }
            catch
            {


            }
            return result;
        }

        /// <summary>
        /// 时间戳转几小时前
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public static string GetFomatTime(string timeStamp)
        {
            return DateStringFromNow(GetTime(timeStamp));
        }
        /// <summary>
        /// 写入sitemap
        /// </summary>
        /// <param name="strList"></param>
        public static void WriteSiteMap(List<string> strList)
        {
            FileInfo myFile = new FileInfo(@"E:\SiteMap\sitemap.txt");
            StreamWriter sw = myFile.CreateText();
            try
            {
                foreach (var s in strList)
                {
                    sw.WriteLine(s);
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                sw.Close();

            }


        }

        /// 时间戳转为C#格式时间  
        /// </summary>  
        /// <param name=”timeStamp”></param>  
        /// <returns></returns>  
        public static DateTime GetTime(string timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime); return dtStart.Add(toNow);
        }
        /// <summary>
        /// 正则判断是否手机
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool IsMobile(string val)
        {
            return Regex.IsMatch(val, @"^1[34567890]\d{9}$", RegexOptions.IgnoreCase);
        }
        /// <summary>
        /// 将时间转成几小时前。。。。。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string DateStringFromNow(DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.TotalDays > 60)
            {
                return dt.ToShortDateString();
            }
            else
            {
                if (span.TotalDays > 30)
                {
                    return
                    "1个月前";
                }
                else
                {
                    if (span.TotalDays > 14)
                    {
                        return
                        "2周前";
                    }
                    else
                    {
                        if (span.TotalDays > 7)
                        {
                            return
                            "1周前";
                        }
                        else
                        {
                            if (span.TotalDays > 1)
                            {
                                return
                                string.Format("{0}天前", (int)Math.Floor(span.TotalDays));
                            }
                            else
                            {
                                if (span.TotalHours > 1)
                                {
                                    return
                                    string.Format("{0}小时前", (int)Math.Floor(span.TotalHours));
                                }
                                else
                                {
                                    if (span.TotalMinutes > 1)
                                    {
                                        return
                                        string.Format("{0}分钟前", (int)Math.Floor(span.TotalMinutes));
                                    }
                                    else
                                    {
                                        if (span.TotalSeconds >= 1)
                                        {
                                            return
                                            string.Format("{0}秒前", (int)Math.Floor(span.TotalSeconds));
                                        }
                                        else
                                        {
                                            return
                                            "1秒前";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
