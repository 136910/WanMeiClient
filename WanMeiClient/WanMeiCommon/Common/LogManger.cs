﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace WanMeiCommon
{
    public class LogManger
    {
        private LogManger() { }
        private static LogManger _instance = new LogManger();
        public static LogManger Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LogManger();
                }
                return _instance;
            }
        }

        private static ILog log = log4net.LogManager.GetLogger("log4net");
        public void WriteLog(string str)
        {
            Task.Factory.StartNew(() =>
            {
                log.Info(str);
            });
        }
    }
}
