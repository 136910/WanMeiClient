﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    public class GameProConfig
    {
        private static JianShiAttrbuteConfig _JianShiAttrbuteConfig = null;
        public static JianShiAttrbuteConfig JianShiAttrbuteConfig
        {
            get
            {
                if (_JianShiAttrbuteConfig == null)
                {
                    _JianShiAttrbuteConfig = new JianShiAttrbuteConfig();
                }
                return _JianShiAttrbuteConfig;
            }
        }
        private static MoFaShiAttrbuteConfig _MoFaShiAttrbuteConfig = null;
        public static MoFaShiAttrbuteConfig MoFaShiAttrbuteConfig
        {
            get
            {
                if (_MoFaShiAttrbuteConfig == null)
                {
                    _MoFaShiAttrbuteConfig = new MoFaShiAttrbuteConfig();
                }
                return _MoFaShiAttrbuteConfig;
            }
        }
        private static AttributeConversion _AttributeConversion = null;
        public static AttributeConversion AttributeConversion
        {
            get
            {
                if (_AttributeConversion == null)
                {
                    _AttributeConversion = new AttributeConversion();
                }
                return _AttributeConversion;
            }
        }
    }
    /// <summary>
    /// 属性换算配置类
    /// </summary>
    public class AttributeConversion
    {
        private static int _criticalValue = 0;
        /// <summary>
        /// 多少暴击值等于1%暴击
        /// </summary>
        public int CriticalValue
        {
            get
            {
                if (_criticalValue == 0)
                {
                    _criticalValue = Convert.ToInt32(ConfigurationManager.AppSettings["CriticalValue"]);
                }
                return _criticalValue;
            }
        }
        private static int _armorDamageReduction = 0;
        /// <summary>
        /// 一护甲减免多少伤害
        /// </summary>
        public static int ArmorDamageReduction
        {
            get
            {
                if (_armorDamageReduction == 0)
                {
                    _armorDamageReduction = Convert.ToInt32(ConfigurationManager.AppSettings["ArmorDamageReduction"]);
                }
                return _armorDamageReduction;
            }
        }
    }

    public  class JianShiAttrbuteConfig
    {
        private static double _jianshi_ApValue = 0.0;
        /// <summary>
        /// 剑士升级，基础攻击题赠比例
        /// </summary>
        public double Jianshi_ApValue
        {
            get
            {
                if (_jianshi_ApValue == 0.0)
                {
                    _jianshi_ApValue = double.Parse(ConfigurationManager.AppSettings["Jianshi_ApValue"]);
                }
                return _jianshi_ApValue;
            }
        }

        private static double _jianshi_HealthPoint = 0.0;
        /// <summary>
        /// 剑士升级血量递增比例
        /// </summary>
        public  double Jianshi_HealthPoint
        {
            get
            {
                if (_jianshi_HealthPoint == 0.0)
                { 
                    _jianshi_HealthPoint = double.Parse(ConfigurationManager.AppSettings["Jianshi_HealthPoint"]);
                }
                return _jianshi_HealthPoint;
            }
        }
        private static double _jianshi_MagicPower = 0.0;
        /// <summary>
        /// 剑士升级魔力递增比例
        /// </summary>
        public  double Jianshi_MagicPower
        {
            get
            {
                if (_jianshi_MagicPower == 0.0)
                { 
                    _jianshi_MagicPower = double.Parse(ConfigurationManager.AppSettings["Jianshi_MagicPower"]);
                }
                return _jianshi_MagicPower;
            }
        }
        private static int _jianshi_Armor = 0;
        /// <summary>
        /// 剑士升级护甲增数
        /// </summary>
        public  int Jianshi_Armor
        {
            get
            {
                if (_jianshi_Armor == 0)
                { 
                    _jianshi_Armor = Convert.ToInt32(ConfigurationManager.AppSettings["Jianshi_Armor"]);
                }
                return _jianshi_Armor;
            }
        }
        private static int _jianshi_CriticalValue = 0;
        /// <summary>
        /// 剑士升级暴击增数
        /// </summary>
        public  int Jianshi_CriticalValue
        {
            get
            {
                if (_jianshi_CriticalValue == 0)
                { 
                    _jianshi_CriticalValue =  Convert.ToInt32(ConfigurationManager.AppSettings["Jianshi_CriticalValue"]);
                }
                return _jianshi_CriticalValue;
            }
        }
    }
    public class MoFaShiAttrbuteConfig
    {
        private static double _mofashi_ApValue = 0.0;
        public double Mofashi_ApValue
        {
            get
            {
                if (_mofashi_ApValue == 0.0)
                {
                    _mofashi_ApValue = double.Parse(ConfigurationManager.AppSettings["Mofashi_ApValue"]);
                }
                return _mofashi_ApValue;
            }
        }

        private static double _mofashi_HealthPoint = 0.0;
        /// <summary>
        /// 魔法师升级血量递增比例
        /// </summary>
        public double Mofashi_HealthPoint
        {
            get
            {
                if (_mofashi_HealthPoint == 0.0)
                { 
                    _mofashi_HealthPoint = double.Parse(ConfigurationManager.AppSettings["Mofashi_HealthPoint"]);
                }
                return _mofashi_HealthPoint;
            }
        }
        private static double _mofashi_MagicPower = 0.0;
        /// <summary>
        /// 魔法师升级魔力递增比例
        /// </summary>
        public double Mofashi_MagicPower
        {
            get
            {
                if (_mofashi_MagicPower == 0.0)
                { 
                    _mofashi_MagicPower = double.Parse(ConfigurationManager.AppSettings["Mofashi_MagicPower"]);
                }
                return _mofashi_MagicPower;
            }
        }
        private static int _mofashi_Armor = 0;
        /// <summary>
        /// 魔法师升级护甲增数
        /// </summary>
        public int Mofashi_Armor
        {
            get
            {
                if (_mofashi_Armor == 0)
                { 
                    _mofashi_Armor = Convert.ToInt32(ConfigurationManager.AppSettings["Mofashi_Armor"]);
                }
                return _mofashi_Armor;
            }
        }
        private static int _mofashi_CriticalValue = 0;
        /// <summary>
        /// 魔法师升级暴击增数
        /// </summary>
        public int Mofashi_CriticalValue
        {
            get
            {
                if (_mofashi_CriticalValue == 0)
                { 
                    _mofashi_CriticalValue = Convert.ToInt32(ConfigurationManager.AppSettings["Mofashi_CriticalValue"]);
                }
                return _mofashi_CriticalValue;
            }
        }
    }
}
