﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    public class PropUpload : MaxSizeUploader
    {
        public static readonly PropUpload Instance = new PropUpload();
        private PropUpload() { }

        public override int MaxBytesLength
        {
            get { return 200 * 1024; }
        }
        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg", ".gif" }; }
        }

        public override string FolderName
        {
            get { return "prop"; }
        }
    }
    /// <summary>
    /// 装备
    /// </summary>
    public class EnquimentUpload : MaxSizeUploader
    {
        public static readonly EnquimentUpload Instance = new EnquimentUpload();
        private EnquimentUpload() { }

        public override int MaxBytesLength
        {
            get { return 200 * 1024; }
        }
        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg", ".gif" }; }
        }

        public override string FolderName
        {
            get { return "zhuangbei"; }
        }
    }
    /// <summary>
    /// 材料
    /// </summary>
    public class MaterialUpload : MaxSizeUploader
    {
        public static readonly MaterialUpload Instance = new MaterialUpload();
        private MaterialUpload() { }

        public override int MaxBytesLength
        {
            get { return 200 * 1024; }
        }
        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg", ".gif" }; }
        }

        public override string FolderName
        {
            get { return "cailiao"; }
        }
    }
    /// <summary>
    /// 宝石
    /// </summary>
    public class GemstoneUpload : MaxSizeUploader
    {
        public static readonly GemstoneUpload Instance = new GemstoneUpload();
        private GemstoneUpload() { }

        public override int MaxBytesLength
        {
            get { return 200 * 1024; }
        }
        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg", ".gif" }; }
        }

        public override string FolderName
        {
            get { return "baoshi"; }
        }
    }
}
