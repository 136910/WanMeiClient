﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    public class SkillUpload : MaxSizeUploader
    {
        public static readonly SkillUpload Instance = new SkillUpload();
        private SkillUpload() { }

        public override int MaxBytesLength
        {
            get { return 200 * 1024; }
        }
        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg", ".gif" }; }
        }

        public override string FolderName
        {
            get { return "jineng"; }
        }
    }
    public class MonsterUpload : MaxSizeUploader
    {
        public static readonly MonsterUpload Instance = new MonsterUpload();
        private MonsterUpload() { }

        public override int MaxBytesLength
        {
            get { return 200 * 1024; }
        }
        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg", ".gif" }; }
        }

        public override string FolderName
        {
            get { return "guai"; }
        }
    }

    public class RaidUpload : MaxSizeUploader
    {
        public static readonly RaidUpload Instance = new RaidUpload();
        private RaidUpload() { }

        public override int MaxBytesLength
        {
            get { return 500 * 1024; }
        }
        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg", ".gif" }; }
        }

        public override string FolderName
        {
            get { return "Raid"; }
        }
    }
}
