﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    /// <summary>
    /// 微信二维码
    /// </summary>
    public class WxQrCodeUploader : MaxSizeUploader
    {
        public static readonly WxQrCodeUploader Instance = new WxQrCodeUploader();
        private WxQrCodeUploader() { }

        public override int MaxBytesLength
        {
            get { return 512 * 1024; }
        }
        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg", ".gif" }; }
        }

        public override string FolderName
        {
            get { return "WeiXinQR"; }
        }
    }
}
