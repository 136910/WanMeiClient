﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    /// <summary>
    /// 限制最小尺寸的Uploader
    /// </summary>
    public abstract class MinDimensionUploader : FileUploaderBase
    {
        public abstract int MinWidth { get; }

        public abstract int MinHeight { get; }

        public override bool Validate(string fileName, Stream stream, out string reason)
        {
            reason = string.Empty;

            if (!AllowedImageTypes.Any(i => i == Path.GetExtension(fileName).ToLower()))
            {
                reason = string.Format("上传失败！请选择{0}类型的文件", string.Join(",", AllowedImageTypes));
                return false;
            }

            using (Image image = Image.FromStream(stream))
            {
                if (image.Width < MinWidth)
                {
                    reason = string.Format("上传失败！图片宽度不能小于{0}", MinWidth);
                    return false;
                }

                if (image.Height < MinHeight)
                {
                    reason = string.Format("上传失败！图片高度不能小于{0}", MinHeight);
                    return false;
                }
            }

            if (stream.Length > MaxBytesLength)
            {
                reason = string.Format("上传失败！图片大小必须小于{0}k", MaxBytesLength / 1024);
                return false;
            }
            return true;
        }
    }
}
