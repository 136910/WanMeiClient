﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    public class ResultModel
    {
        public ResultModel(bool isSuccess, string message)
            : this(isSuccess, message, null)
        {
        }

        public ResultModel(bool isSuccess, string message, object data)
        {
            this.IsSuccess = isSuccess;
            this.Message = message;
            this.Data = data;
        }

        public bool IsSuccess { get; private set; }
        public string Message { get; private set; }
        public object Data { get; set; }
    }
    public class ResultManager
    {
        /// <summary>
        /// 值类型返回模型构造
        /// </summary>
        /// <param name="isSuccess"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ResultModel CreateReuslt(bool isSuccess, string msg)
        {
            return new ResultModel(isSuccess, msg);
        }
        /// <summary>
        /// 引用类型返回模型构造
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="isSuccess"></param>
        /// <param name="msg"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ResultModel CreateResult<T>(bool isSuccess, T model)
        {
            return new ResultModel(isSuccess, string.Empty, model);
        }
    }
}
