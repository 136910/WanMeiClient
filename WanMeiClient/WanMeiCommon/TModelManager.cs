﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    /// <summary>
    /// 分页数据模型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TModelManager<T>
    {
        public TPagedModelList<T> TPagedModelList { get; set; }
    }
    public class TPagedModelList<T>
    {
        public TPagedModelList(List<T> list, PagingResult PagingResult)
        {
            this.TList = list;
            this.PagingResult = PagingResult;
        }
        public List<T> TList { get; set; }
        public PagingResult PagingResult { get; set; }
    }
}
