﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    public enum CommonStatus
    {
        下架 = 2,
        正常 = 1,
        删除 = -1
    }
    public enum PayStatus
    {
        未支付 = 1,
        已支付 = 2
    }
    public enum WriteOrReadEnum
    {
        //从主库读取
        FromWrite = 0,
        //从库读取
        FromRead = 1
    }
    /// <summary>
    /// 道具类型
    /// </summary>
    public enum PropType
    {
        Equipment = 1,//装备
        Materal=2,//材料
        Gemstone = 3,//宝石类
    }

    public enum ChatType
    { 
        All=1,//所有人
        Single=2,//私聊
        GongHui=3//工会
    }
}
