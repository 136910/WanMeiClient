﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    [Serializable]
    public sealed class PagedList<T> : List<T>
    {
        public PagingResult PagingResult { get; set; }

        /// <summary>
        /// EF返回的数据模型
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        public PagedList(IQueryable<T> source, int pageIndex, int pageSize)
        {
            PagingResult = new PagingResult(pageIndex, pageSize);

            PagingResult.TotalCount = source.Count();

            var skipCount = pageIndex * pageSize;
            var data = source.Skip(skipCount).Take(pageSize).ToList();

            PagingResult.RecordCount = data.Count;

            this.AddRange(data);
        }
    }

    [Serializable]
    public sealed class PagedSqlList<T> : List<T>
    {
        public PagingResult PagingResult { get; set; }
        /// <summary>
        /// SQL返回的数据模型
        /// </summary>
        /// <param name="source"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        public PagedSqlList(IList<T> source, int pageIndex, int pageSize, int totalCount)
        {
            PagingResult = new PagingResult(pageIndex, pageSize);
            PagingResult.TotalCount = totalCount;
            PagingResult.RecordCount = source.Count;
            this.AddRange(source);
        }
    }
}
