﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WanMeiCommon
{
    public sealed class ConfigSettings
    {
        public static readonly ConfigSettings Instance = new ConfigSettings();

        private ConfigSettings() { }

        public string FileUploadPath
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadPath"];
            }
        }
        public string FileUploadPathForChirld
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadPathForChirld"];
            }
        }

        public int SupplierId
        {
            get
            {
                int result;
                if (int.TryParse(ConfigurationManager.AppSettings["SupplierId"], out result))
                    return result;
                return result;
            }
        }

        public string FileUploadFolderNameTemp
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadFolderNameTemp"];
            }
        }

        public string FileUploadFolderNameProject
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadFolderNameProject"];
            }
        }

        /// <summary>
        /// 活动图片保存路径
        /// </summary>
        public string FileUploadFolderNameActivityImage
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadFolderNameActivityImage"];
            }
        }

        public string WebSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        private string[] _keywords;
        private const string KEYWORD_FILENAME = "keywords.txt";

        public string GameProImgHost
        {
            get
            {
                return ConfigurationManager.AppSettings["GameProImgHost"];
            }
        }
        public string RedisHost
        {
            get
            {
                return ConfigurationManager.AppSettings["RedisHost"];
            }
        }
    }
}
