﻿using System;

public class Class1
{
    /// <summary>
    /// 基础服务接口，释放资源
    /// </summary>
    public interface IGamePTService : IDisposable
    {
        void Dispose();
    }
}
