﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WanMeiModel;

namespace WanMeiClient
{
    public interface IRaidMonsterFightBase
    {
        RaidMonsterFightResultModel RaidMonsterAttack(RaidFightManagerModel fightModel,int monsterId,int type);
    }
}