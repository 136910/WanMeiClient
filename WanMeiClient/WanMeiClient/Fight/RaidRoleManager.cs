﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WanMeiCommon;
using WanMeiModel;

namespace WanMeiClient
{
    /// <summary>
    /// 也是有点坑啊，这个抽象类，按道理应该被角色信息类继承，实现这个虚方法。然而，层级的问题，这里给抽象到主程序集做一个副本战斗角色管理类
    /// </summary>
    public class RaidRoleManager : IRaidFightBase
    {
        //private static readonly object locker = new object();//当前怪模型锁； 
        public RaidDotEventResultModel RoleDotEvent(RaidFightManagerModel fightModel, int skillId, int monsterId)
        {
            RaidDotEventResultModel res = new RaidDotEventResultModel();
            int fightStatus = 0;
            StatusSkillItem currentSkill = fightModel.RoleSkills.Where(i => i.Id == skillId).FirstOrDefault();

            //计算实际伤害/治疗值(伤害=技能伤害+角色本身+是否有暴击)
            //暴击计算公式，暂定为10暴击=1%
            int isbj = 0;
            int realSkillValue = ToolManager.GetRealSkillValue((fightModel.CurrentRoleInfo.ApValue + currentSkill.SkillBussinessValue), fightModel.CurrentRoleInfo.CriticalValue, out isbj);
            if (currentSkill.SkillOption == 1)
            {
                //--------------伤害DOT
                if (fightModel.Monsters.Count <= 0)
                {
                    fightStatus = 1;
                    fightModel.FightStatus = 1;
                    return res;
                }
                var theMonster = fightModel.Monsters.Where(p => p.Id == monsterId).FirstOrDefault();
                if (theMonster == null)
                {
                    res.MonsterStatus = -1;
                    return res;
                }
                fightModel.Monsters.Where(p => p.Id == monsterId).FirstOrDefault().HeathValue -= realSkillValue;
                if (fightModel.Monsters.Where(p => p.Id == monsterId).FirstOrDefault().HeathValue > 0)
                {
                    res.IsDead = 0;
                }
                else
                {
                    //怪挂了
                    if (fightModel.Monsters.Count == 1)
                    {
                        //胜利
                        fightStatus = 1;
                        fightModel.FightStatus = 1;
                    }
                    else
                    {
                        fightModel.Monsters.Remove(fightModel.Monsters.Where(p => p.Id == monsterId).FirstOrDefault());
                    }
                    res.IsDead = 1;
                }
                if (res.IsDead == 1)
                    res.HealthValue = 0;
                else
                    res.HealthValue = fightModel.Monsters.Where(p => p.Id == monsterId).FirstOrDefault().HeathValue;
                res.HurtValue = realSkillValue;
                res.IsBj = isbj;
                res.MonsterId = monsterId;
                res.FightStatus = fightStatus;

                //更新redis
                RedisManager.Set<RaidFightManagerModel>(fightModel.FightId, fightModel, DateTime.Now.AddMinutes(20));
            }
            else
            {
                //--------------治疗DOT
                fightModel.CurrentRoleInfo.HealthPoint += realSkillValue;
                res.HealthValue = fightModel.CurrentRoleInfo.HealthPoint;
                res.IsBj = isbj;
                res.BeTreatedValue = realSkillValue;
                //更新redis
                RedisManager.Set<RaidFightManagerModel>(fightModel.FightId, fightModel, DateTime.Now.AddMinutes(20));
            }

            return res;
        }

        /// <summary>
        /// 角色技能效果
        /// </summary>
        /// <param name="fightModel"></param>
        /// <param name="skillId"></param>
        /// <returns></returns>
        public RaidFigthResultModel RoleAttack(RaidFightManagerModel fightModel, int skillId)
        {
            RaidFigthResultModel res = null;
            RaidFightRoleItem roleRes = new RaidFightRoleItem();
            List<RaidFightMonsterItem> monsterRes = new List<RaidFightMonsterItem>();
            int fightStatus = 0;
            List<MonsterModel> deadMonsterIdArr = new List<MonsterModel>();
            StatusSkillItem currentSkill = fightModel.RoleSkills.Where(i => i.Id == skillId).FirstOrDefault();
            //计算实际伤害/治疗值(伤害=技能伤害+角色本身+是否有暴击)
            //暴击计算公式，暂定为10暴击=1%
            int isbj = 0;
            int realSkillValue = ToolManager.GetRealSkillValue((fightModel.CurrentRoleInfo.ApValue + currentSkill.SkillBussinessValue), fightModel.CurrentRoleInfo.CriticalValue, out isbj);
            if (currentSkill.SkillOption == 1)
            {
                //---伤害技
                switch (currentSkill.SpecialEffectType)
                {
                    case 1:
                    case 4:
                        //--单目标攻击，
                        //持续性技能不减
                        if (currentSkill.SkillType == 1)
                            fightModel.Monsters[0].HeathValue -= realSkillValue;
                        if (fightModel.Monsters[0].HeathValue > 0)
                        {
                            //-没死
                            //怪返回模型
                            RaidFightMonsterItem mitem = new RaidFightMonsterItem();
                            mitem.Id = fightModel.Monsters[0].Id;
                            mitem.HealthValue = fightModel.Monsters[0].HeathValue;
                            mitem.MagicValue = fightModel.Monsters[0].MagicValue;
                            mitem.HurtValue = realSkillValue;
                            mitem.IsBj = isbj;
                            monsterRes.Add(mitem);
                        }
                        else
                        {
                            //怪死了
                            //判断是否只有一只，是：战斗胜利
                            if (fightModel.Monsters.Count == 1)
                            {
                                RaidFightMonsterItem mitem = new RaidFightMonsterItem();
                                mitem.Id = fightModel.Monsters[0].Id;
                                mitem.HurtValue = realSkillValue;
                                mitem.IsBj = isbj;
                                mitem.IsDead = 1;
                                monsterRes.Add(mitem);
                                fightStatus = 1;
                                fightModel.FightStatus = 1;
                            }
                            else
                            {
                                RaidFightMonsterItem mitem = new RaidFightMonsterItem();
                                mitem.Id = fightModel.Monsters[0].Id;
                                mitem.HurtValue = realSkillValue;
                                mitem.IsBj = isbj;
                                mitem.IsDead = 1;
                                monsterRes.Add(mitem);
                                fightModel.Monsters.RemoveAt(0);//redis删除第一个怪
                            }
                        }
                        break;
                    case 3:
                    case 5:
                        //--全体攻击
                        for (int i = 0; i < fightModel.Monsters.Count; i++)
                        {
                            if (currentSkill.SkillType == 1)
                                fightModel.Monsters[i].HeathValue -= realSkillValue;
                            if (fightModel.Monsters[i].HeathValue > 0)
                            {
                                RaidFightMonsterItem mitem = new RaidFightMonsterItem();
                                mitem.Id = fightModel.Monsters[i].Id;
                                mitem.HealthValue = fightModel.Monsters[i].HeathValue;
                                mitem.MagicValue = fightModel.Monsters[i].MagicValue;
                                mitem.HurtValue = realSkillValue;
                                mitem.IsBj = isbj;
                                monsterRes.Add(mitem);
                            }
                            else
                            {
                                RaidFightMonsterItem mitem = new RaidFightMonsterItem();
                                mitem.Id = fightModel.Monsters[i].Id;
                                mitem.HurtValue = realSkillValue;
                                mitem.IsBj = isbj;
                                mitem.IsDead = 1;
                                //reids怪集合中移除这个死了的怪,先加入死怪集合
                                deadMonsterIdArr.Add(fightModel.Monsters[i]);
                                monsterRes.Add(mitem);
                            }

                        }
                        //从redis模型移除死怪
                        deadMonsterIdArr.ForEach((item) =>
                        {
                            fightModel.Monsters.Remove(item);
                        });
                        //判断是否战斗胜利
                        if (fightModel.Monsters.Count <= 0)
                        {
                            fightStatus = 1;
                            fightModel.FightStatus = 1;
                        }

                        break;

                }
                //角色返回模型

                fightModel.CurrentRoleInfo.MagicPower -= currentSkill.NeedMagic;
                roleRes.MagicValue = fightModel.CurrentRoleInfo.MagicPower;
                roleRes.HealthValue = fightModel.CurrentRoleInfo.HealthPoint;
                res = new RaidFigthResultModel(roleRes, monsterRes);
                res.FightStatus = fightStatus;
                //更新redis
                RedisManager.Set<RaidFightManagerModel>(fightModel.FightId, fightModel, DateTime.Now.AddMinutes(20));
            }
            else
            {
                //---治疗技
                //只有单目标角色目前
                fightModel.CurrentRoleInfo.MagicPower -= currentSkill.NeedMagic;
                if (currentSkill.SkillType == 1)
                {
                    fightModel.CurrentRoleInfo.HealthPoint += realSkillValue;
                }
                roleRes.MagicValue = fightModel.CurrentRoleInfo.MagicPower;
                roleRes.HealthValue = fightModel.CurrentRoleInfo.HealthPoint;
                roleRes.IsBj = isbj;
                roleRes.BeTreatedValue = realSkillValue;
                //更新redis
                RedisManager.Set<RaidFightManagerModel>(fightModel.FightId, fightModel, DateTime.Now.AddMinutes(20));
                res = new RaidFigthResultModel(roleRes, monsterRes);
            }
            return res;
        }
    }
}