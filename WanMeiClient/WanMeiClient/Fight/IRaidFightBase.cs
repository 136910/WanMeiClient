﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WanMeiModel;

namespace WanMeiClient
{
    public interface IRaidFightBase
    {
        RaidFigthResultModel RoleAttack(RaidFightManagerModel fightModel, int skillId);
        RaidDotEventResultModel RoleDotEvent(RaidFightManagerModel fightModel, int skillId, int monsterId);
        
    }
}