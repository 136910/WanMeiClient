﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WanMeiCommon;
using WanMeiModel;

namespace WanMeiClient
{
    public class RaidMonsterManager : IRaidMonsterFightBase
    {

        #region IRaidMonsterFightBase 成员

        public RaidMonsterFightResultModel RaidMonsterAttack(RaidFightManagerModel fightModel, int monsterId, int type)
        {
            RaidMonsterFightResultModel res = new RaidMonsterFightResultModel();
            MonsterModel monster = fightModel.Monsters.Where(p => p.Id == monsterId).FirstOrDefault();
            if (monster != null)
            {
                var baseAp = type == 1 ? monster.Ap : monster.SkillValue;
                fightModel.CurrentRoleInfo.HealthPoint -= baseAp;
                res.HurtValue = baseAp;
                if (fightModel.CurrentRoleInfo.HealthPoint <= 0)
                {
                    //角色战斗失败，死亡
                    res.IsDead = 1;
                    res.HealthValue = 0;
                    fightModel.FightStatus = -1;
                }
                else
                {
                    res.HealthValue = fightModel.CurrentRoleInfo.HealthPoint;
                }

                //更新redis
                RedisManager.Set<RaidFightManagerModel>(fightModel.FightId, fightModel, DateTime.Now.AddMinutes(20));
            }

            return res;
        }

        #endregion
    }
}