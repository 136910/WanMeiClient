﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WanMeiModel;

namespace WanMeiClient
{
    public class SkillExtend
    {
        /// <summary>
        ///副本 战斗时角色的被动技能加成,暂时只有加成
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static RaidFightManagerModel UseingPassiveSkills(RaidFightManagerModel model)
        {
            List<StatusSkillItem> passiveBuffersList = model.RoleSkills.Where(i => i.SkillType == 3 && i.SkillOption==1).ToList();
            List<StatusSkillItem> passiveDebuffsList = model.RoleSkills.Where(i => i.SkillType == 3 && i.SkillOption == 2).ToList();
            foreach (var item in passiveBuffersList)
            {
                //增益
                switch (item.PassivePlace)
                { 
                    case 1:
                        model.CurrentRoleInfo.ApValue += item.SkillBussinessValue;
                        break;
                    case 2:
                        model.CurrentRoleInfo.Armor += item.SkillBussinessValue;
                        break;
                    case 3:
                        model.CurrentRoleInfo.CriticalValue += item.SkillBussinessValue;
                        break;
                    case 4:
                        model.CurrentRoleInfo.HealthPoint += item.SkillBussinessValue;
                        break;
                }
            }
            foreach (var item in passiveDebuffsList)
            {
                //减益,减益效果对怪只作用AP
                switch (item.PassivePlace)
                {
                    case 1:
                        model.Monsters.ForEach((monster) =>
                        {
                            monster.Ap -= item.SkillBussinessValue;
                        });
                        break;
                }
            }
            return model;

        }
     


    }
}