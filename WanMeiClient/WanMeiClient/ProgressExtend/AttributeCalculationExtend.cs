﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WanMeiCommon;
using WanMeiModel;

//1:a
//2:1.1a
//3:1.1a+1.1a*0.1 = 1.1a*1.1 = 1.1的2次方*a
//4：1.1a*1.1*1.1 = 1.1的3次方*a
//n:1.1的(n-1)次方*a


//1.1的99次方

//1：a
//2:a+2 
//3:a+2+2 = a+2*2
//4:a+2+2+2 = a+2*3
//n:a+2(n-1)
namespace WanMeiClient.ProgressExtend
{
    /// <summary>
    /// 角色属性计算算法类
    /// 
    /// </summary>
    public class AttributeCalculationExtend
    {
        /// <summary>
        /// 如果获取的经验导致角色升级，计算升级后经验属性的方法
        /// </summary>
        /// <param name="model"></param>
        /// <param name="exValue"></param>
        /// <returns></returns>
        public static UserRoleBaseAttributeItem LevelUpCalculation(UserRoleBaseAttributeItem model, int exValue,out int isLevelUp)
        {
            isLevelUp = 0;
            long _tempValue = exValue;
            while (_tempValue > 0)
            {
                //计算当前剩余经验是否可让角色升级
                _tempValue -= (model.NeedExValue - model.ExValue);
                if (_tempValue >= 0)
                  {
                    //可升级
                    model.GameLevel++;
                    //让升级后角色当前经验值为0，方便计算
                    model.ExValue = 0;
                    //提升基本属性
                    model = PromoteAttribute(model);
                    isLevelUp = 1;
                }
                else
                { 
                    //不够继续升级了
                    model.ExValue += (_tempValue += (model.NeedExValue - model.ExValue));//不能直接赋值，要加回去减掉的
                    _tempValue = 0;
                }
            }
            return model;
        }
        /// <summary>
        /// 为角色基本属性提升，（1个等级）
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ra"></param>
        public static UserRoleBaseAttributeItem PromoteAttribute(UserRoleBaseAttributeItem model)
        {
            switch (model.HeroId)
            {
                case 1:
                    model.HealthPoint = Convert.ToInt32(model.HealthPoint * (1 + GameProConfig.JianShiAttrbuteConfig.Jianshi_HealthPoint));
                    model.MagicPower = Convert.ToInt32(model.MagicPower * (1 + GameProConfig.JianShiAttrbuteConfig.Jianshi_MagicPower));
                    model.ApValue = Convert.ToInt32(model.ApValue * (1 + GameProConfig.JianShiAttrbuteConfig.Jianshi_ApValue));
                    model.Armor = model.Armor + GameProConfig.JianShiAttrbuteConfig.Jianshi_Armor;
                    model.CriticalValue = model.CriticalValue + GameProConfig.JianShiAttrbuteConfig.Jianshi_CriticalValue;
                    break;
                case 2:
                    model.HealthPoint = Convert.ToInt32(model.HealthPoint * (1 + GameProConfig.MoFaShiAttrbuteConfig.Mofashi_HealthPoint));
                    model.MagicPower = Convert.ToInt32(model.MagicPower * (1 + GameProConfig.MoFaShiAttrbuteConfig.Mofashi_MagicPower));
                    model.ApValue = Convert.ToInt32(model.ApValue * (1 + GameProConfig.MoFaShiAttrbuteConfig.Mofashi_ApValue));
                    model.Armor = model.Armor + GameProConfig.MoFaShiAttrbuteConfig.Mofashi_Armor;
                    model.CriticalValue = model.CriticalValue + GameProConfig.MoFaShiAttrbuteConfig.Mofashi_CriticalValue;
                    break;
            }
            return model;
        }
    }


}