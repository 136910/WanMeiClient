﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WanMeiClient
{
    public class BussinessFactory
    {
        public static T CreateInstance<T>(string bussinessName) where T : class
        {
            try
            {
                object ect = Assembly.Load("WanMeiBussinessService")
                                     .CreateInstance("WanMeiBussinessService." + bussinessName);
                return ect as T;
            }
            catch
            {
                return default(T);
            }
        }
    }
}