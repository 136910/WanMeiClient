﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WanMeiBussinessService;

namespace WanMeiClient
{
    public class BussinessInstance
    {
        protected readonly static IHeroService _IHeroService = BussinessFactory.CreateInstance<IHeroService>("HeroService");
        protected readonly static IRaidService _IRaidService = BussinessFactory.CreateInstance<IRaidService>("RaidService");
        protected readonly static ISkillService _ISkillService = BussinessFactory.CreateInstance<ISkillService>("SkillService");
        protected readonly static IUserService _IUserService = BussinessFactory.CreateInstance<IUserService>("UserService");
    }
}