﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WanMeiCommon;
using WanMeiModel;

namespace WanMeiClient
{
    /// <summary>
    /// 公共的组件化数据管理类。一些从redis读取的过程，要多个控制器用到，就放到这里。
    /// </summary>
    public class AssemblyData:BussinessInstance
    {
        private AssemblyData() { }
        //定义一个线程安全标识
        private static readonly object locker = new object();
        private static AssemblyData _instance = null;
        public static AssemblyData Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (locker)
                    {
                        if (_instance == null)
                        {
                            _instance = new AssemblyData();
                        }
                    }
                }
                return _instance;
            }
        }
        /// <summary>
        /// 获取装备+基本属性的角色信息和当前装备列表
        /// </summary>
        /// <param name="heroId"></param>
        /// <param name="un"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        internal UserInfoModelResult GetUserInfoModelResult(int heroId, string un)
        {
            UserInfoModelResult result = null;
            string key = "UserInfoModel_" + heroId + "_" + un;
            Func<InputArgs, UserInfoModelResult> func = delegate(InputArgs ia)
            {
                return _IHeroService.GetUserInfoModel(ia.Un, ia.HeroId);
            };
            result = DataIntegration.GetData<UserInfoModelResult>(key, func, new InputArgs(heroId, un));
            return result;
        }
        /// <summary>
        /// 获取角色状态中全部技能列表
        /// </summary>
        /// <param name="heroId"></param>
        /// <param name="un"></param>
        /// <returns></returns>
        internal List<StatusSkillItem> GetStatusSkillList(int heroId, string un)
        {
            List<StatusSkillItem> result = null;
            string key = "StatusSkill_" + heroId + "_" + un;
            Func<InputArgs, List<StatusSkillItem>> func = delegate(InputArgs ia)
            {
                return _ISkillService.GetStatusSkillList(ia.HeroId, ia.Un);
            };
            result = DataIntegration.GetData<List<StatusSkillItem>>(key, func, new InputArgs(heroId, un));
            return result;
        }
        /// <summary>
        /// 获取所有副本
        /// </summary>
        /// <returns></returns>
        internal List<RaidItem> GetRaidList()
        {
            List<RaidItem> result = null;
            string key = "AllRaidList";
            Func<List<RaidItem>> func = delegate()
            {
                return _IRaidService.GetRaidList();
            };
            result = DataIntegration.GetData<List<RaidItem>>(key, func);
            return result;
        }
    }
}