﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.Redis;
using WanMeiCommon;

namespace WanMeiClient
{
    public static class RedisManager
    {
        #region static field
        private static IRedisClient _redisClient = null;
        private static IRedisClient RedisClient
        {
            get
            {
                if (_redisClient == null)
                {
                    _redisClient = new RedisClient(ConfigSettings.Instance.RedisHost, 6379);
                    _redisClient.Password = "caonimadaohao123";
                }
                return _redisClient;
            }
        }
        #endregion
        #region
        /// <summary>
        /// 获取信息
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <returns>对象</returns>
        public static T Get<T>(string token)
        {
            try
            {
                return RedisClient.Get<T>(token);
            }
            catch
            {
                LogManger.Instance.WriteLog("redis挂了,请求支援");
                return default(T);
            }

        }
        /// <summary>
        /// 设置信息
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="token">key</param>
        /// <param name="obj">对象</param>
        public static void Set<T>(string token, T obj, DateTime expressTime)
        {
            try
            {
                RedisClient.Set<T>(token, obj, expressTime);
            }
            catch
            {

                LogManger.Instance.WriteLog("redis挂了,请求支援");
            }

        }
        /// <summary>
        /// 删除单个缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public static bool Remove(string key)
        {
            try
            {
                return RedisClient.Remove(key);
            }
            catch
            {

                LogManger.Instance.WriteLog("redis挂了,请求支援");
                return false;
            }
        }
        /// <summary>
        /// 删除多个缓存
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public static bool RemoveList(List<string> keys)
        {
            try
            {
                RedisClient.RemoveAll(keys);
                return true;
            }
            catch
            {

                LogManger.Instance.WriteLog("redis挂了,请求支援");
                return false;
            }
        }
        #endregion
    }
}