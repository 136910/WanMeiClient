var MAX_SHOP = 100;
var currentRoom, packitems, isFromPack = false,
titles = [];
var personList = [];
var noticeList = [];
var isFighting = false;
var isKilling = false;
var isFromItem = false;
var isFromNpc = false;
var isFromPlayer = false;
var isFromMe = false;
var isJiali = false;
var fightType = "";
var unreadMessage = 0;
var skillNpc = "";
var currentSkill = "";
var isFromContent = false;
var back = false;
var defaultExits = ["northwest", "north", "northeast", "west", "look", "east", "southwest", "south", "southeast"];
//var sock = io.connect();
//var send = function (a) {
//    if (sock) {
//        sock.emit("cmd", a + "\n")
//    }
//};
function formatData(k) {
    var b = [];
    if (k != null) {
        k = k.replace("\ufeff", "")
    }
    var j = ansi_up.ansi_to_html(k);
    j = j.replace(/#/g, "#");
    j = j.replace(/style="/g, "style='").replace(/\)"/g, ")'");
    if (j.length == 0) {
        return b
    }
    var a = j.indexOf("json:");
    var e = j.lastIndexOf("}");
    var g = j.indexOf("{") == 0 && e == j.length - 3 && j.length > 2;
    if (j.indexOf("你上次离线是") > 0) {
        j = j.substring(0, e + 1);
        window.localStorage.setItem("actions", "")
    }
    if (a != -1) {
        var f = j.split("json:");
        for (var c = 0; c < f.length; c++) {
            var h = f[c];
            e = h.lastIndexOf("}");
            if (!!h && h.length > 0) {
                if (e <= h.length - 3) {
                    var l = h.substring(0, e + 1);
                    var d = h.substring(e + 1, h.length);
                    b.push(l);
                    if (d.length > 4) {
                        d = d.replace(/\n/g, "<br/>");
                        b.push(d)
                    }
                } else {
                    b.push(h)
                }
            }
        }
    } else {
        if (!g) {
            j = j.replace(/\n/g, "<br/>")
        }
        b.push(j)
    }
    return b
}
var htmlDecode = function (a) {
    var b = document.createElement("div");
    b.innerHTML = a;
    return b.childNodes.length === 0 ? "" : b.childNodes[0].nodeValue
};
//sock.on("data",
//function (d) {
//    if (d.indexOf("您的英文名字") >= 0 || d.indexOf("这是一个已注册的帐号") >= 0 || d.indexOf("请输入密码") >= 0) { } else {
//        if (d.indexOf("(y/n)") != -1) {
//            var g = ansi_up.ansi_to_html(d);
//            var f = g.substring(0, g.indexOf("？") + 1);
//            showConfirmDialog(f)
//        } else {
//            if (d.indexOf("重新连线完毕") != -1) {
//                send("l");
//                send("checkfight")
//            } else {
//                if (d.indexOf("{") == -1) {
//                    var g = formatData(d) || "";
//                    showMessage(g[0])
//                } else {
//                    var g = formatData(d);
//                    for (var c = 0; c < g.length; c++) {
//                        var h = g[c];
//                        if (h.indexOf("{") == -1) {
//                            showMessage(h)
//                        } else {
//                            if (!!h) {
//                                h = JSON.parse(h);
//                                if (h.type == "room") {
//                                    showRoom(h)
//                                } else {
//                                    if (h.type == "npc") {
//                                        showNpc(h)
//                                    } else {
//                                        if (h.type == "player") {
//                                            showPlayer(h)
//                                        } else {
//                                            if (h.type == "item") {
//                                                showItem(h)
//                                            } else {
//                                                if (h.type == "inventory") {
//                                                    loadPack(h)
//                                                } else {
//                                                    if (h.type == "fuben" || h.type == "roombid") {
//                                                        showRank(h)
//                                                    } else {
//                                                        if (h.type == "board") {
//                                                            showBoard(h)
//                                                        } else {
//                                                            if (h.type == "chawu") {
//                                                                loadChawu(h)
//                                                            } else {
//                                                                if (h.type == "combine") {
//                                                                    showCombine(h)
//                                                                } else {
//                                                                    if (h.type == "basicstatus") {
//                                                                        loadBasicstatus(h)
//                                                                    } else {
//                                                                        if (h.type == "mapshop") {
//                                                                            shopMapShop(h)
//                                                                        } else {
//                                                                            if (h.type == "shoplist") {
//                                                                                showShop(h)
//                                                                            } else {
//                                                                                if (h.type == "vipshop" || h.type == "fmshop") {
//                                                                                    loadSpecialshop(h)
//                                                                                } else {
//                                                                                    if (h.type == "vipitem" || h.type == "fmitem") {
//                                                                                        showVipitem(h)
//                                                                                    } else {
//                                                                                        if (h.type == "msg") {
//                                                                                            showChatMessage(h, true)
//                                                                                        } else {
//                                                                                            if (h.type == "updateStatus") {
//                                                                                                updateStatus(h)
//                                                                                            } else {
//                                                                                                if (h.type == "combatstatus") {
//                                                                                                    loadCombatstatus(h)
//                                                                                                } else {
//                                                                                                    if (h.type == "jiali") {
//                                                                                                        setJiali(h)
//                                                                                                    } else {
//                                                                                                        if (h.type == "listweapon") {
//                                                                                                            loadWeapon(h)
//                                                                                                        } else {
//                                                                                                            if (h.type == "listweaponset") {
//                                                                                                                loadWeaponSet(h)
//                                                                                                            } else {
//                                                                                                                if (h.type == "quest") {
//                                                                                                                    loadQuests(h)
//                                                                                                                } else {
//                                                                                                                    if (h.type == "title") {
//                                                                                                                        loadTitles(h)
//                                                                                                                    } else {
//                                                                                                                        if (h.type == "team") {
//                                                                                                                            loadTeam(h)
//                                                                                                                        } else {
//                                                                                                                            if (h.type == "introducer") {
//                                                                                                                                loadIntroducer(h)
//                                                                                                                            } else {
//                                                                                                                                if (h.type == "invite") {
//                                                                                                                                    showInviteMsg(h)
//                                                                                                                                } else {
//                                                                                                                                    if (h.type == "startfight") {
//                                                                                                                                        loadFightInfo(h)
//                                                                                                                                    } else {
//                                                                                                                                        if (h.type == "joinfight") {
//                                                                                                                                            showJoinFight(h)
//                                                                                                                                        } else {
//                                                                                                                                            if (h.type == "fight") {
//                                                                                                                                                loadFightMessage(h)
//                                                                                                                                            } else {
//                                                                                                                                                if (h.type == "escape") {
//                                                                                                                                                    showEscape(h)
//                                                                                                                                                } else {
//                                                                                                                                                    if (h.type == "endfight") {
//                                                                                                                                                        showEndFightTip(h)
//                                                                                                                                                    } else {
//                                                                                                                                                        if (h.type == "pfmcd") {
//                                                                                                                                                            showPfmCD(h)
//                                                                                                                                                        } else {
//                                                                                                                                                            if (h.type == "tip") {
//                                                                                                                                                                showWarnTip(h)
//                                                                                                                                                            } else {
//                                                                                                                                                                if (h.type == "maze") {
//                                                                                                                                                                    showMazemapDialog(h)
//                                                                                                                                                                } else {
//                                                                                                                                                                    if (h.type == "nowmap") {
//                                                                                                                                                                        loadNowMap(h)
//                                                                                                                                                                    } else {
//                                                                                                                                                                        if (h.type == "listmap") {
//                                                                                                                                                                            loadMapset(h)
//                                                                                                                                                                        } else {
//                                                                                                                                                                            if (h.type == "listpfmset") {
//                                                                                                                                                                                showFightSettingContent(h)
//                                                                                                                                                                            } else {
//                                                                                                                                                                                if (h.type == "listmed") {
//                                                                                                                                                                                    showSetFightOptions(h, true)
//                                                                                                                                                                                } else {
//                                                                                                                                                                                    if (h.type == "listpfm") {
//                                                                                                                                                                                        showSetFightOptions(h, false)
//                                                                                                                                                                                    } else {
//                                                                                                                                                                                        if (h.type == "skills") {
//                                                                                                                                                                                            showSkills(h)
//                                                                                                                                                                                        } else {
//                                                                                                                                                                                            if (h.type == "researchinfo") {
//                                                                                                                                                                                                showResearch(h)
//                                                                                                                                                                                            } else {
//                                                                                                                                                                                                if (h.type == "checkskill") {
//                                                                                                                                                                                                    showSkillDetail(h)
//                                                                                                                                                                                                } else {
//                                                                                                                                                                                                    if (h.type == "options") {
//                                                                                                                                                                                                        var b = h.options;
//                                                                                                                                                                                                        if (!!b && b.length > 0) {
//                                                                                                                                                                                                            for (var a = 0; a < b.length; a++) {
//                                                                                                                                                                                                                var e = b[a].type;
//                                                                                                                                                                                                                if (e == "addPlayer") {
//                                                                                                                                                                                                                    addPlayer(b[a])
//                                                                                                                                                                                                                } else {
//                                                                                                                                                                                                                    if (e == "removePlayer") {
//                                                                                                                                                                                                                        removePlayer(b[a])
//                                                                                                                                                                                                                    } else {
//                                                                                                                                                                                                                        if (e == "addAction") {
//                                                                                                                                                                                                                            addAction(b[a])
//                                                                                                                                                                                                                        } else {
//                                                                                                                                                                                                                            if (e == "removeAction") {
//                                                                                                                                                                                                                                removeAction(b[a])
//                                                                                                                                                                                                                            } else {
//                                                                                                                                                                                                                                if (e == "addItem") {
//                                                                                                                                                                                                                                    addItem(b[a])
//                                                                                                                                                                                                                                } else {
//                                                                                                                                                                                                                                    if (e == "removeItem") {
//                                                                                                                                                                                                                                        removeNpc(b[a])
//                                                                                                                                                                                                                                    } else {
//                                                                                                                                                                                                                                        if (e == "addNpc") {
//                                                                                                                                                                                                                                            addItem(b[a])
//                                                                                                                                                                                                                                        } else {
//                                                                                                                                                                                                                                            if (e == "removeNpc") {
//                                                                                                                                                                                                                                                removeNpc(b[a])
//                                                                                                                                                                                                                                            } else {
//                                                                                                                                                                                                                                                if (e == "addExits") {
//                                                                                                                                                                                                                                                    addExists(b[a])
//                                                                                                                                                                                                                                                } else {
//                                                                                                                                                                                                                                                    if (e == "removeExits") {
//                                                                                                                                                                                                                                                        removeExits(b[a])
//                                                                                                                                                                                                                                                    }
//                                                                                                                                                                                                                                                }
//                                                                                                                                                                                                                                            }
//                                                                                                                                                                                                                                        }
//                                                                                                                                                                                                                                    }
//                                                                                                                                                                                                                                }
//                                                                                                                                                                                                                            }
//                                                                                                                                                                                                                        }
//                                                                                                                                                                                                                    }
//                                                                                                                                                                                                                }
//                                                                                                                                                                                                            }
//                                                                                                                                                                                                        }
//                                                                                                                                                                                                    }
//                                                                                                                                                                                                }
//                                                                                                                                                                                            }
//                                                                                                                                                                                        }
//                                                                                                                                                                                    }
//                                                                                                                                                                                }
//                                                                                                                                                                            }
//                                                                                                                                                                        }
//                                                                                                                                                                    }
//                                                                                                                                                                }
//                                                                                                                                                            }
//                                                                                                                                                        }
//                                                                                                                                                    }
//                                                                                                                                                }
//                                                                                                                                            }
//                                                                                                                                        }
//                                                                                                                                    }
//                                                                                                                                }
//                                                                                                                            }
//                                                                                                                        }
//                                                                                                                    }
//                                                                                                                }
//                                                                                                            }
//                                                                                                        }
//                                                                                                    }
//                                                                                                }
//                                                                                            }
//                                                                                        }
//                                                                                    }
//                                                                                }
//                                                                            }
//                                                                        }
//                                                                    }
//                                                                }
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//});
//sock.on("status",
//function (b) {
//    if (b.indexOf("Telnet disconnected.") != -1) {
//        var a = window.location.href;
//        window.location.href = a + "login"
//    }
//});
//sock.on("connected",
//function () {
//    console.log("connected")
//});
//sock.on("disconnect",
//function () {
//    console.log("disconnected")
//});
function stopAnimate(a) {
    $(a).stop(false, true).animate()
}
function showWarnTip(c) {
    var b = c.content;
    if (!!b) {
        var a = $(".tip");
        if (a.length == 0) {
            $("body").append('<div class="tip" onclick=""></div>')
        }
        if (c.subtype == "unconcious" || c.subtype == "sleep") {
            if ($("#overlay").length == 0) {
                $("body").append('<div id="overlay"></div>')
            }
            $("#overlay").height(function () {
                return document.body.scrollHeight
            });
            $("#overlay").width(function () {
                return document.body.scrollWidth
            });
            $("#overlay").fadeTo(200, 0.5);
            $("body").find(".tip").html(b).fadeTo();
            return
        }
        if (c.subtype == "revive" || c.subtype == "awake" || c.subtype == "dead") {
            if ($("#overlay").length != 0) {
                $("#overlay").fadeOut(200)
            }
        }
        $("body").find(".tip").html(b).fadeIn();
        setTimeout(function () {
            $(".tip").fadeOut()
        },
        2000)
    }
}
function showOverlay() {
    $("#overlay").height(function () {
        return document.body.scrollHeight
    });
    $("#overlay").width(function () {
        return document.body.scrollWidth
    });
    $("#overlay").fadeTo(200, 0.5)
}
function hideOverlay() {
    $("#overlay").fadeOut(200)
}
function addPage(a) {
    $(".content").hide();
    $("#content").append(a);
    $(".content").last().show();
    showBackBtn()
}
function showRoom(k) {
    var m = $("#content"),
    a = k.room;
    isFromPack = false;
    currentRoom = a;
    hideBackBtn();
    var g;
    if (decode(a.area) != "0") {
        g = a.area + "-" + a.name
    } else {
        g = a.name
    }
    $("#title").html(g);
    titles = [];
    titles.push(g);
    var o = !!a.desc ? a.desc : "";
    var b = [];
    b.push('<div class="content">');
    b.push('<div class="desc">', o, "</div>");
    b.push('<div class="ditection">');
    var p = 0;
    for (var l = 0; l < 3; l++) {
        b.push('<div class="direction-item">');
        for (var f = 0; f < 3; f++) {
            b.push('<div class="', defaultExits[p], '" id="', defaultExits[p], '" onclick="clickDirection(this);"></div>');
            p++
        }
        b.push("</div>")
    }
    b.push("</div>");
    b.push('<div class="shortcut" onclick="openBag();">背包</div>');
    b.push('<div class="shortcut" style="top: 27%" onclick="mySkills();">技能</div>');
    b.push('<div class="shortcut active" style="top: 35%" id="dazuo" onclick="dazuo(this);">打坐</div>');
    b.push('<div class="shortcut" style="top: 43%" onclick="liaoshang();">疗伤</div>');
    b.push('<div class="npclist"><div class="label">这儿有：</div><ul id="npc_list">');
    if (!!a.npcs && a.npcs.length > 0) {
        for (var f = 0; f <= a.npcs.length; f++) {
            var h = a.npcs[f];
            if (!!h) {
                b.push('<li id="', h.id, '" onclick="clickObject(this);" class="npc-item"><span>', h.name, "</span></li>")
            }
        }
    }
    if (!!a.items && a.items.length > 0) {
        for (var f = 0; f <= a.items.length; f++) {
            var h = a.items[f];
            if (!!h) {
                b.push('<li id="', h.id, '" onclick="clickItem(this);" class="item-item"><span>', h.name, "</span></li>")
            }
        }
    }
    if (!!a.players && a.players.length > 0) {
        for (var f = 0; f <= a.players.length; f++) {
            var h = a.players[f];
            if (!!h) {
                b.push('<li id="', h.id, '" onclick="clickObject(this);"><span>', h.name, "</span></li>")
            }
        }
    }
    b.push("</ul></div><div>");
    m.html(b.join(""));
    $(".direction-item > div", m).html("").removeClass("active");
    var e = a.exits;
    if (!!e) {
        $("#look", m).html(a.name).addClass("active");
        $("#look").attr("onclick", "showMapDialog()");
        var r = e.split("|");
        var t = [];
        for (var l = 0; l < r.length; l++) {
            var q = [r[l].split(":")[0], r[l].split(":").slice(1).join(":")];
            var s = false;
            for (var f = 0; f < defaultExits.length; f++) {
                if (q[0] == defaultExits[f]) {
                    $("#" + defaultExits[f]).html(q[1]).addClass("active");
                    s = true
                } else {
                    if (q[0] == defaultExits[f] + "up") {
                        s = true;
                        $("#" + defaultExits[f]).html(q[1]).addClass("active2")
                    } else {
                        if (q[0] == defaultExits[f] + "down") {
                            $("#" + defaultExits[f]).html(q[1]).addClass("active3");
                            s = true
                        }
                    }
                }
            }
            if (!s) {
                t.push(q)
            }
        }
        if (!!t && t.length > 0) {
            var c = [];
            c.push('<div class="otherdirects">');
            c.push('<div class="label">房间内另有出口：</div><ul id="otherDir">');
            for (var d = 0; d < t.length; d++) {
                var q = t[d];
                c.push('<li id="', q[0], '" onclick="clickDirection(this);" class="other"><span>', q[1], "</span></li>")
            }
            c.push("</ul></div>");
            $(c.join("")).insertAfter($(".ditection", m))
        }
    }
    refreshActions();
    $(".npclist").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    });
    $(".desc").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    });
    $("#loading").hide();
    send("checkfight")
}
function clickDirection(c) {
    var a = $(c);
    if (!a.hasClass("active") && !a.hasClass("active2") && !a.hasClass("active3") && !a.hasClass("other")) {
        return
    }
    var b = a.attr("id");
    if (!!b) {
        if (a.hasClass("active2")) {
            send(b + "up")
        } else {
            if (a.hasClass("active3")) {
                send(b + "down")
            } else {
                send(b)
            }
        }
        clearActions()
    }
}
function addExists(d) {
    var f = d.exits;
    if (!!f) {
        var a = f.split(":");
        a[0].replace("up", "").replace("down", "");
        var b = $("#" + a[0]);
        if (b.length > 0) {
            b.html(a[1]).addClass("active")
        } else {
            var c = $(".content").first();
            var b = c.find(".otherdirects");
            var e = [];
            if (b.length == 0) {
                e.push('<div class="otherdirects"><div class="label">房间内另有出口：</div><ul id="otherDir">');
                e.push('<li id="', a[0], '" onclick="clickDirection(this);" class="other"><span>', a[1], "</span></li>");
                e.push("</ul></div></div>");
                $(e.join("")).insertAfter($(".ditection", c))
            } else {
                e.push('<li id="', a[0], '" onclick="clickDirection(this);" class="other"><span>', a[1], "</span></li>");
                b.find("ul").append(e.join(""))
            }
        }
    }
}
function removeExits(b) {
    var c = b.exits;
    if (!!c) {
        var a = $(".ditection #" + c);
        if (a.length > 0) {
            $("#" + c).html("").removeClass("active")
        }
    }
}
function clickObject(c) {
    var a = $(c);
    var b = a.attr("id");
    if (!!b) {
        send("look " + b)
    }
}
function addItem(b) {
    var a = '<li id="' + b.id + '" onclick="clickItem(this);" class="item-item"><span>' + b.name + "</span></li>";
    $("#npc_list").append(a)
}
function clickItem(c) {
    var a = $(c);
    var b = a.attr("id");
    if (!!b) {
        send("look " + b)
    }
    isFromPack = false
}
function showMessage(d) {
    if (!! !d) {
        return
    }
    var c = "<li>" + d + "</li>";
    $("#messagelist").append(c);
    var a = $("#messagelist li");
    if (a.length > 50) {
        for (var b = 0; b < (a.length - 50) ; b++) {
            a[b].remove()
        }
    }
    $("#messageinner").mCustomScrollbar("scrollTo", "bottom")
}
function shrinkAndExpandMessage(a) {
    if ($(a).hasClass("active")) {
        $(".page").addClass("shrink-message");
        $(a).removeClass("active").html("展开")
    } else {
        $(".page").removeClass("shrink-message");
        $(a).addClass("active").html("收起")
    }
}
function openBag() {
    showStatus();
    $(".statuslist li").eq(2).click()
}
function showNpc(f) {
    $("#title").html(f.name);
    titles.push(f.name);
    var h = f.desc;
    var d = h.split("|");
    var a = [];
    a.push('<div class="content">');
    a.push('<div class="desc" style="height: 7em;margin-bottom:15px;">');
    for (var g = 0; g < d.length - 1; g++) {
        a.push(d[g] + "</br>")
    }
    a.push("</div>");
    a.push('<div class="npccontent"><ul>');
    a.push('<li style="list-style:none;">', d[g], "</li>");
    var b = f.content;
    if (!!b && b.length > 0) {
        for (var c = 0; c < b.length; c++) {
            a.push('<li data-id="', b[c].id, '"><span class="contentname">', b[c].name, "</span>");
            if (b[c].status == "worn" || b[c].status == "wielded") {
                a.push('<span class="contentbutton" data-command="look ', b[c].id, " of ", f.id, '" onclick="clickContentButton(this);">查看</span>')
            }
            if (f.status == "unconcious") {
                a.push('<span class="contentbutton" data-command="get ', (!!b[c].amount) ? b[c].amount : "", " ", b[c].id, " from ", f.id, '" onclick="clickContentButton(this);" data-id="', f.id, '">捡取</span>')
            }
            a.push("</li>")
        }
    }
    a.push("</ul></div>");
    var e = [{
        direct: "ask",
        label: "询问"
    },
    {
        direct: "fight",
        label: "比试"
    },
    {
        direct: "kill",
        label: "杀死"
    }];
    a.push('<div class="npclist"><ul class="actions">');
    for (var g = 0; g < e.length; g++) {
        a.push('<li data-id="', f.id, '" data-direct="', e[g].direct, '" onclick="doAction(this);"><span>', e[g].label, "</span></li>")
    }
    if (f.subtype == "vendor") {
        a.push('<li data-direct="list" onclick="doAction(this);"><span>购买</span></li>')
    }
    var j = f.id.substring(0, f.id.indexOf("#"));
    if (f.subtype == "master") {
        a.push('<li data-direct="bai" data-id="' + j + '" onclick="doAction(this);"><span>拜师</span></li>')
    }
    if (f.canlearn == "yes") {
        a.push('<li data-direct="skills" data-fullid="' + f.id + '" data-id="' + j + '" data-name="', f.name, '" onclick="xueyi(this);"><span>学艺</span></li>')
    }
    if (f.status == "unconcious") {
        if (!isFromPack) {
            a.push('<li data-direct="get" data-id="', f.id, '" onclick="doAction(this);"><span>扛起</span></li>')
        } else {
            a.push('<li data-direct="drop" data-id="', f.id, '" onclick="doAction(this);"><span>放下</span></li>')
        }
        a.push('<li data-direct="ge" data-id="', f.id, '" onclick="doAction(this);"><span>斩首</span></li>');
        a.push('<li data-direct="get all from" data-id="', f.id, '" onclick="doAction(this);"><span>搜身</span></li>')
    }
    a.push("</ul></div></div>");
    addPage(a.join(""));
    isFromNpc = true;
    $(".desc").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    });
    $(".npccontent").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function xueyi(d) {
    var b = $(d).data("name");
    var c = $(d).data("id");
    var a = $(d).data("fullid");
    skillNpc = {
        name: b + "的技能",
        id: a
    };
    send("skills " + c)
}
function removeNpc(b) {
    var a = $("#npc_list").find('li[id="' + b.id + '"]');
    a.remove()
}
function showPlayer(f) {
    var h = $("#content2");
    $("#title").html(f.name);
    titles.push(f.name);
    var j = f.desc;
    var d = j.split("|");
    var a = [];
    a.push('<div class="content">');
    a.push('<div class="desc" style="height:auto;">');
    for (var g = 0; g < d.length; g++) {
        a.push(d[g] + "</br>")
    }
    a.push("</div>");
    a.push('<div class="npccontent"><ul>');
    var b = f.content;
    if (!!b && b.length > 0) {
        for (var c = 0; c < b.length; c++) {
            a.push('<li data-id="', b[c].id, '"><span class="contentname">', b[c].name, "</span>");
            if (b[c].status == "worn" || b[c].status == "wielded") {
                a.push('<span class="contentbutton" data-command="look ', b[c].id, " of ", f.id, '" onclick="clickContentButton(this);">查看</span>')
            }
            a.push("</li>")
        }
    }
    a.push("</ul></div>");
    if (isFromMe == false) {
        var e = [{
            direct: "fight",
            label: "比试"
        },
        {
            direct: "kill",
            label: "杀死"
        }];
        a.push('<div class="npclist"><ul class="actions">');
        for (var g = 0; g < e.length; g++) {
            a.push('<li data-id="', f.id, '" data-direct="', e[g].direct, '" onclick="doAction(this);"><span>', e[g].label, "</span></li>")
        }
        a.push('<li data-id="', f.id.split("#")[0], '" data-direct="team invite" onclick="doAction(this);"><span>组队</span></li>');
        a.push("</ul></div>")
    } else {
        isFromMe = false
    }
    a.push("</div>");
    addPage(a.join(""));
    isFromPlayer = true;
    $(".npccontent").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function showItem(f) {
    $("#title").html(f.name);
    titles.push(f.name);
    var l = f.desc;
    var d = l.split("|");
    var a = [];
    a.push('<div class="content">');
    a.push('<div class="desc" style="height:auto;">');
    for (var g = 0; g < d.length; g++) {
        a.push(d[g] + "</br>")
    }
    a.push("</div>");
    a.push('<div class="npccontent"><ul>');
    var b = f.content;
    if (!!b && b.length > 0) {
        for (var c = 0; c < b.length; c++) {
            a.push('<li data-id="', b[c].id, '"><span class="contentname">', b[c].name, "</span>");
            if (b[c].status == "worn" || b[c].status == "wielded") {
                a.push('<span class="contentbutton" data-command="look ', b[c].id, " of ", f.id, '" onclick="clickContentButton(this);">查看</span>')
            }
            a.push('<span class="contentbutton" data-command="get ', (!!b[c].amount) ? b[c].amount : "", " ", b[c].id, " from ", f.id, '" onclick="clickContentButton(this);" data-id="', f.id, '">捡取</span>');
            a.push("</li>")
        }
    }
    a.push("</ul></div>");
    var o = f.noget;
    var m = f.canbind;
    var q = f.saved;
    if (isFromPack && !isFromContent) {
        var h = 1;
        if (!!packitems) {
            for (var g = 0; g < packitems.length; g++) {
                if (packitems[g].id == f.id) {
                    h = !!packitems[g].amount ? packitems[g].amount : 1
                }
            }
        }
    }
    if (!isFromContent) {
        var e = [];
        if (!isFromPack) {
            if (o != 1) {
                e.push({
                    direct: "get",
                    label: "捡起"
                })
            }
            if (!!f.content) {
                e.push({
                    direct: "get all from ",
                    label: "搜身"
                })
            }
            if (f.id.indexOf("corpse") >= 0) {
                e.push({
                    direct: "ge",
                    label: "斩首"
                })
            }
        } else {
            e.push({
                direct: "chuwu",
                label: "储物"
            });
            e.push({
                direct: "drop",
                label: "丢弃"
            });
            if (m == 1 && q != 1) {
                e.push({
                    direct: "bind",
                    label: "绑定"
                })
            }
            if (f.combine == 1) {
                e.push({
                    direct: "combine show 1",
                    label: "合成"
                })
            }
            if (!!f.subtype) {
                var k = f.subtype.split("|");
                for (var p = 0; p < k.length; p++) {
                    var j = k[p];
                    if (j == "weapon") {
                        if (!! !f.status) {
                            e.push({
                                direct: "wield",
                                label: "装备"
                            })
                        }
                        if (!!f.status && f.status == "wielded") {
                            e.push({
                                direct: "unwield",
                                label: "脱下"
                            })
                        }
                        if (!!f.spare) {
                            for (var g = 0; g < f.spare; g++) {
                                e.push({
                                    direct: "setweapon " + g + " " + f.id,
                                    label: "备用" + (g + 1)
                                })
                            }
                            e.push({
                                direct: "setweapon augment",
                                label: "扩展备用"
                            })
                        }
                    }
                    if (j == "armor" && !!!f.status) {
                        e.push({
                            direct: "wear",
                            label: "装备"
                        })
                    }
                    if (j == "armor" && !!f.status && f.status == "worn") {
                        e.push({
                            direct: "remove",
                            label: "脱下"
                        })
                    }
                    if (j == "open") {
                        e.push({
                            direct: "open",
                            label: "打开"
                        })
                    }
                    if (j == "use") {
                        e.push({
                            direct: "use",
                            label: "使用"
                        })
                    }
                    if (j == "food") {
                        e.push({
                            direct: "eat",
                            label: "服用"
                        })
                    }
                    if (j == "liquid") {
                        e.push({
                            direct: "drink",
                            label: "饮用"
                        })
                    }
                    if (j == "book") {
                        e.push({
                            direct: "study",
                            label: "阅读"
                        })
                    }
                    if (j == "medicine") {
                        var r = f.id.substring(0, f.id.indexOf("#"));
                        e.push({
                            direct: "setpfm 0 " + decode(f.name) + " eat " + r + "#medicine",
                            label: "快捷1"
                        });
                        e.push({
                            direct: "setpfm 1 " + decode(f.name) + " eat " + r + "#medicine",
                            label: "快捷2"
                        });
                        e.push({
                            direct: "eat",
                            label: "吃"
                        })
                    }
                }
            }
        }
        a.push('<div class="npclist"><ul class="actions">');
        for (var g = 0; g < e.length; g++) {
            if (e[g].direct == "chuwu" || e[g].direct == "drop") {
                a.push('<li data-id="', f.id, '" data-actname="', e[g].label, '" data-command="', e[g].direct, '" data-amount="', h, '" onclick="showNumDialog(this);"><span>', e[g].label, "</span></li>")
            } else {
                a.push('<li data-id="', f.id, '" data-saved="', f.saved, '" data-direct="', e[g].direct, '" onclick="doItemAction(this);"><span>', e[g].label, "</span></li>")
            }
        }
        a.push("</ul></div></div>")
    }
    isFromContent = false;
    addPage(a.join(""));
    isFromItem = true
}
function clickContentButton(c) {
    isFromContent = true;
    var a = $(c).data("command");
    if (!!a) {
        send(a);
        if (a.indexOf("get") != -1) {
            $("#back").trigger("click");
            var b = $(c).data("id");
            send("look " + b)
        }
    }
}
function showVipitem(f) {
    $("#title").html(f.name);
    titles.push(f.name);
    var a, h;
    h = f.command;
    if (h == "shop") {
        a = "元宝"
    } else {
        if (h == "fmshop") {
            switch (f.label) {
                case "门贡":
                    a = "门贡";
                    break;
                case "经验":
                    a = "经验";
                    break;
                case "喜鹊羽":
                    a = "喜鹊羽";
                    break;
                default:
                    return
            }
        }
    }
    var g = f.desc;
    var e = g.split("|");
    var d = [];
    d.push('<div class="content">');
    d.push('<div class="desc" style="height:auto;"><span>');
    for (var c = 0; c < e.length; c++) {
        d.push(e[c] + "</br>")
    }
    d.push('</span><span class="vipbalance" onclick="showDepositDialog();">您剩余可用', a, "：", f.balance, '</span><span class="vipvalue">该物品单价为', f.value, a, "</span></div>");
    if (!!f.balance) {
        var b = parseInt(f.balance / f.value)
    }
    d.push('<div class="numberbox">');
    d.push('<div class="numbers"><span class="reduce" onclick="reduceItem()">-</span><input type="tel" min="0" max="', b, '" onchange="intMax(this)" class="number" id="amount" value="0"/><span class="plus" onclick="plusItem()">+</span></div>');
    d.push('<div class="max" data-id="', f.id, '" data-value="', f.value, '" onclick="showVipConfirmDialog(2,&#39;', h, "&#39;,&#39;", a, '&#39;)">购买</div>');
    d.push("</div>");
    addPage(d.join(""))
}
function maxItem() {
    var a = $("#amount").attr("max");
    $("#amount").val(a)
}
function reduceItem() {
    var a;
    if ($(".combinerecipe").length) {
        a = $("#amount").text();
        a = parseInt(a, 10);
        if ((a - 1) < 0) {
            return
        } else {
            $("#amount").text(a - 1);
            combinecount()
        }
    } else {
        a = $("#amount").val();
        a = parseInt(a, 10);
        if ((a - 1) < 0) {
            return
        } else {
            $("#amount").val(a - 1)
        }
    }
}
function plusItem() {
    var a = $("#amount").attr("max");
    var b;
    if (!!$(".combinerecipe").length) {
        b = $("#amount").text();
        b = parseInt(b, 10);
        $("#amount").text(b + 1);
        combinecount()
    } else {
        b = $("#amount").val();
        b = parseInt(b, 10);
        if ((b + 1) > a) {
            return
        } else {
            $("#amount").val(b + 1)
        }
    }
}
function doItemAction(e) {
    var d = $(e).data("direct");
    var c = $(e).data("id");
    if (isFromPack) {
        var a = $("#amount").html();
        a = parseInt(a, 10);
        if ((!!d && d != "drop") || (d == "drop" && a > 0)) {
            if (d == "drop") {
                var b = $(e).data("saved");
                send(d + " " + a + " " + c);
                if (b != "1") {
                    $("#back").trigger("click")
                } else {
                    back = true
                }
            } else {
                if (!!d && (d.indexOf("#medicine") > 0 || d.indexOf("setweapon") >= 0)) {
                    send(d)
                } else {
                    send(d + " " + c)
                }
            }
            if (d != "eat" && d != "drink" && d != "drop" && d != "combine show 1" && d.indexOf("setweapon") < 0) {
                $("#back").trigger("click")
            }
        }
    } else {
        send(d + " " + c);
        send("look")
    }
}
function doCloseAction(b) {
    var a = $(b).data("command");
    if (!!a) {
        if (a == "team dismiss") {
            send(a);
            send("team")
        } else {
            send(a)
        }
    }
}
function doVipbuy(d, b) {
    var a = $("#amount").val();
    a = parseInt(a, 10);
    var c = $(".max").data("id");
    if (a > 0 && typeof (c) != "undefined") {
        send(b + " buy " + a + " " + c)
    }
    closeDialog()
}
function doResearch(c) {
    var b = $(c).data("direct");
    var a = b.split("of ")[1];
    send(b);
    send("researchinfo " + a)
}
function doAction(c) {
    var b = $(c).data("direct");
    if (!!b) {
        var a = $(c).data("id");
        if (b == "ask") {
            send(b + " " + a + " about something")
        } else {
            if (b == "get" || b == "ge") {
                send(b + " " + a);
                send("look");
                $("#back").trigger("click")
            } else {
                if (b == "look me") {
                    isFromMe = true;
                    send(b)
                } else {
                    if (b.indexOf("fight") != -1) {
                        send("fight " + a);
                        fightType = "fight"
                    } else {
                        if (b.indexOf("kill") != -1) {
                            isKilling = true;
                            fightType = "kill";
                            send(b + " " + a)
                        } else {
                            if (b.indexOf("perform") != -1) {
                                if (!a || a == "undefined") {
                                    send(b)
                                } else {
                                    send(b + " " + a)
                                }
                            } else {
                                if (b.indexOf("yun") != -1) {
                                    send(b)
                                } else {
                                    if (b.indexOf("#medicine") > 0) {
                                        send(b)
                                    } else {
                                        if (b == "listweapon") {
                                            showWeapon();
                                            send(b);
                                            send("listweaponset")
                                        } else {
                                            if (b == "map set") {
                                                send(b + " " + a);
                                                $(c).data("used", 1);
                                                checkMapUsed()
                                            } else {
                                                send(b + " " + a)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
function doAction2(b) {
    var a = $(b).data("command");
    if (!!a) {
        send(a)
    }
}
function addPlayer(b) {
    var a = '<li id="' + b.id + '" onclick="clickObject(this);"><span>' + b.name + "</span></li>";
    $("#npc_list").append(a)
}
function removePlayer(b) {
    var a = $("#npc_list").find('li[id="' + b.id + '"]');
    a.remove()
}
function addAction(c) {
    if (isFromNpc || isFromItem || isFromPlayer) {
        var b = $(".content").last().find(".actions");
        b = b.length == 0 ? $(".content").last().find("#npc_list") : b;
        var d = b.find('li[data-command="' + c.command + '"]');
        if (d.length == 0) {
            if (c.command == "withdraw" || c.command == "deposit" || c.command == "roombid bid") {
                var a = '<li data-id="gold" data-actname="' + c.name + '" data-command="' + c.command + '" onclick="showNumDialog(this);"><span>' + c.name + "</span></li>"
            } else {
                if (c.command == "answer") {
                    var a = '<li data-command="' + c.command + '" onclick="showInputDialog(this);"><span>' + c.name + "</span></li>"
                } else {
                    var a = '<li data-command="' + c.command + '" onclick="doAction2(this);" class="actionitem"><span>' + c.name + "</span></li>"
                }
            }
            b.append(a)
        }
    } else {
        storageAction(c);
        refreshActions()
    }
}
function refreshActions() {
    $(".actionitem").remove();
    if (!!window.localStorage && !!window.localStorage.getItem("actions") && window.localStorage.getItem("actions").length > 0) {
        var b = window.localStorage.getItem("actions");
        var a = JSON.parse(b);
        var d = [];
        for (var c = 0; c < a.length; c++) {
            if (currentRoom.room_id == a[c].roomId) {
                d.push('<li data-command="', a[c].command, '" onclick="doAction2(this);" class="actionitem"><span>', a[c].name, "</span></li>")
            }
        }
        if (d.length > 0) {
            var e = $("#npc_list").find(".npc-item");
            if (e.length > 0) {
                $(d.join("")).insertBefore(e.first())
            } else {
                $("#npc_list").append(d.join(""))
            }
        }
    }
}
function storageAction(d) {
    if (!!window.localStorage) {
        if (!! !window.localStorage.getItem("actions")) {
            window.localStorage.setItem("actions", "")
        }
        d.roomId = currentRoom.room_id;
        var b = window.localStorage.getItem("actions");
        var e = [];
        var a = true;
        if (b.length > 0) {
            e = JSON.parse(b);
            for (var c = 0; c < e.length; c++) {
                if (e[c].roomId == currentRoom.room_id && d.name == e[c].name && d.command == e[c].command) {
                    a = false
                }
            }
        }
        if (a) {
            e.push(d);
            window.localStorage.setItem("actions", JSON.stringify(e))
        }
    }
}
function removeAction(d) {
    if (isFromNpc || isFromItem) {
        var c = $(".content").last().find(".actions");
        c = c.length == 0 ? $(".content").last().find("#npc_list") : c;
        c.find("li").each(function () {
            var e = $(this).find("span").html();
            e = e.replace(/\"/g, "'");
            if (e == d.name) {
                $(this).remove();
                return
            }
        })
    } else {
        if (!!window.localStorage && window.localStorage.getItem("actions").length > 0) {
            var a = JSON.parse(window.localStorage.getItem("actions"));
            for (var b = 0; b < a.length; b++) {
                if (a[b].name == d.name) {
                    a.splice(b, 1)
                }
            }
            window.localStorage.setItem("actions", JSON.stringify(a))
        }
    }
}
function clearActions() {
    window.localStorage.setItem("actions", "")
}
function showStage() {
    $("#title").html("快速驿站");
    titles.push("快速驿站");
    var d = ["无锡城", "武当山", "北京城", "桃花岛", "成都城", "峨嵋山", "大理城", "青城山", "少室山", "洛阳城", "开封城", "福州城", "终南山", "华山村", "昆明城", "临安城", "山海关", "双旗镇", "苏州城", "新兰州", "扬州城", "云中界"];
    var c = [];
    c.push('<div class="content"><div id="stagebox" style="height:95%;width:100%">');
    c.push('<ul class="stagelist">');
    for (var b = 0; b < d.length; b++) {
        var a = b % 2 == 0 ? "yellowtext" : "bluetext";
        c.push('<li class="', a, '" onclick="doAction(this);" data-direct="fumove" data-id="', d[b], '">', d[b], "</li>")
    }
    c.push("</ul></div></div>");
    addPage(c.join(""));
    $("#stagebox").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function showStatus() {
    $("#title").html("状态");
    resetTitles("状态");
    var a = $(".statusContent");
    if (a.length == 0) {
        var b = [];
        b.push('<div class="content statusContent">');
        b.push('<ul class="statuslist">');
        b.push('<li onclick="clickButton(this);" data-command="score">基本属性</li>');
        b.push('<li onclick="clickButton(this);" data-command="score1">战斗属性</li>');
        b.push('<li onclick="clickButton(this);" data-command="i">背包</li>');
        b.push('<li onclick="clickButton(this);" >常用按钮</li>');
        b.push('</ul><div id="statusbox"><div id="statusboxcontent"></div></div></div>');
        addPage(b.join(""));
        $(".statuslist > li").eq(0).trigger("click")
    }
}
function showRank(e) {
    var g = e.name;
    $("#title").html(g);
    titles.push(g);
    var c = [];
    c.push('<div class="content" id="toprank">');
    c.push('<div class="currenttitle">', e.msg.replace("|", "</br>"), "</div>");
    c.push('<ul class="ranklist">');
    if (!!e.toplist) {
        var f = e.toplist;
        for (var b = 0; b < f.length; b++) {
            c.push('<li><span style="width:20%;">', f[b].rank, '</span><span style="width:20%;">', f[b].value, '</span><span style="width:50%;position:relative;"><span class="rankteam"></span></span></li>')
        }
    }
    c.push("</ul></div>");
    addPage(c.join(""));
    $("#toprank").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    });
    if (!!f) {
        for (var b = 0; b < f.length; b++) {
            $(".rankteam").eq(b).html(f[b].team);
            var a = $(".rankteam").eq(b).width();
            var d = $(".rankteam").eq(b).parent().width();
            if (a > d) {
                $(".rankteam").eq(b).parent().animate({
                    scrollLeft: a - d
                },
                (a - d) * 1000 / 12)
            }
        }
    }
}
function showQuests() {
    $("#title").html("任务和互动");
    resetTitles("任务和互动");
    var a = $(".questContent");
    if (a.length == 0) {
        var b = [];
        b.push('<div class="content questContent">');
        b.push('<ul class="questlist">');
        b.push('<li onclick="clickQuestButton(this);" data-command="quest">任务</li>');
        b.push('<li onclick="clickQuestButton(this);" data-command="team">队伍</li>');
        b.push('<li onclick="clickQuestButton(this);" data-command="introducer">推广</li>');
        b.push('</ul><div id="statusbox"><div id="statusboxcontent"></div></div></div>');
        addPage(b.join(""));
        $(".questlist > li").eq(0).trigger("click")
    }
}
function showVipshop() {
    send("shop")
}
function loadSpecialshop(e) {
    var b, g;
    g = e.command;
    if (e.type == "vipshop") {
        $("#title").html("会员商城");
        resetTitles("会员商城");
        b = "元宝"
    } else {
        if (e.type == "fmshop") {
            if (e.label == "门贡") {
                $("#title").html("门派贡献兑换");
                resetTitles("门派贡献兑换");
                b = "门贡"
            } else {
                if (e.label == "经验") {
                    $("#title").html("经验商店");
                    resetTitles("经验商店");
                    b = "经验"
                } else {
                    if (e.label == "喜鹊羽") {
                        $("#title").html("喜鹊羽兑换");
                        resetTitles("喜鹊羽兑换");
                        b = "喜鹊羽"
                    }
                }
            }
        }
    }
    var d = [];
    d.push('<div class="content">');
    d.push('<div class="shoptitle"><span>物品列表</span></div>');
    d.push('<div id="shoplist"><ul class="shoplist">');
    var a = e.detail.items;
    if (!!a && a.length > 0) {
        for (var c = 0; c < a.length; c++) {
            var f = a[c];
            d.push('<li data-id="', f.id, '"><span data-id="', f.id, '" data-value="', f.value, '" data-direct="', g, ' look" onclick="doAction(this)">', f.name, "</span><span>", f.value, b, "</span>");
            if (!!f.canbuy) {
                d.push('<span><span class="shop-plus" onclick="reduceItemsNum(this)"><a>-</a></span><input type="tel" min="0" max="', f.canbuy, '" value="0" onchange="intMax(this)" class="shop-num"/><span class="shop-add" onclick="addItemsNum(this)"><a>+</a></span></span>')
            } else {
                d.push('<span><span class="shop-plus" onclick="reduceItemsNum(this)"><a>-</a></span><input type="tel" min="0" max="', MAX_SHOP, '" value="0" onchange="intMax(this)" class="shop-num"/><span class="shop-add" onclick="addItemsNum(this)"><a>+</a></span></span>')
            }
            d.push("</li>")
        }
    }
    d.push('</ul></div><div class="buybutton">');
    if (e.type == "vipshop") {
        d.push('<a class="recharge-btn" onclick="showDepositDialog()">充值</a>')
    }
    d.push('<a class="balance">可用', b, "余额：", e.detail.balance, "</a>");
    d.push('<a class="buy-btn" onclick="showVipConfirmDialog(1,&#39;', g, "&#39;,&#39", b, '&#39;)">购买</a></div>');
    d.push('<a class="vipmsg">友情提示：点击物品名称可查看物品详细信息。</a>');
    d.push("</div>");
    addPage(d.join(""));
    $("#shoplist").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function showBoard(d) {
    $("#title").html(d.title);
    titles.push(d.title);
    var a = d.msg.split("|");
    var c = [];
    c.push('<div class="content">');
    c.push('<div class="board">');
    for (var b = 0; b < a.length; b++) {
        c.push("<p>" + a[b] + "</p>")
    }
    c.push("</div>");
    c.push("</div>");
    addPage(c.join(""));
    $(".board").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function showCombine(d) {
    $("#title").html("合成");
    resetTitles("合成");
    var c = [];
    c.push('<div class="content" style="text-align:center;">');
    var e = d.recipe;
    c.push('<div class="combinerecipe" data-status="1"><table width="100%"><tr><th>合成材料</th><th>所需数量</th><th>拥有数量</th></tr>');
    for (var b = 0; b < e.length; b++) {
        c.push('<tr><td width="40%">', e[b].name, "</td>");
        var a = e[b].num.split("|");
        c.push('<td width="30%" data-rqnum="', a[1], '">', a[1], "</td>");
        c.push('<td width="30%" data-ownnum="', a[0], '">', a[0], "</td></tr>")
    }
    c.push('</table></div><div class="combinearrow"></div><div class="combinetarget"><table width="100%" data-id="', d.nowid, '"><tr><th>合成结果</th><th>获得数量</th></tr><tr><td width="60%">', d.name, '</td><td id="resultnum">1</td width="40%"></tr></table></div>');
    c.push('<div class="combinenum"><span onclick="reduceItem();">-</span><span id="amount">1</span><span onclick="plusItem();">+</span></div>');
    c.push('<div class="combinebtn" onclick="doCombine();">合成</div></div>');
    addPage(c.join(""));
    combinecount()
}
function combinecount() {
    var b = $("#amount").html();
    b = parseInt(b, 10);
    if (b >= 0) {
        var a = 0;
        $(".combinerecipe").find("tr").each(function () {
            var d = $(this).find("td").eq(1).data("rqnum");
            $(this).find("td").eq(1).html(d * b);
            var c = $(this).find("td").eq(2).data("ownnum");
            if (d * b > c) {
                $(this).find("td").eq(1).html("数量不足");
                a += 1
            }
        });
        if (a == 0) {
            $(".combinerecipe").data("status", 1)
        } else {
            $(".combinerecipe").data("status", 0)
        }
        $("#resultnum").html(b)
    }
}
function doCombine() {
    var a = $("#amount").text();
    a = parseInt(a, 10);
    var b = $(".combinetarget table").data("id");
    if ($(".combinerecipe").data("status") == 0) {
        $("#amount").text(0);
        return
    }
    if (a > 0 && !!b) {
        send("combine do " + a + " " + b);
        $("#amount").text(0);
        combinecount()
    }
}
function resetTitles(c) {
    var a = titles[0];
    titles = [a];
    titles.push(c);
    var d = $(".content");
    for (var b = 1; b < d.length; b++) {
        d.eq(b).remove()
    }
}
function clickButton(b) {
    $(".statuslist li.active").removeClass("active");
    $(b).addClass("active");
    if ($(b).text() == "常用按钮") {
        loadButtonList();
        return
    }
    var a = $(b).data("command");
    if (!!a) {
        if (a == "i") {
            send("i");
            send("chawu")
        } else {
            send(a)
        }
    }
}
function clickSkillsButton(b) {
    $(".skillslist li.active").removeClass("active");
    $(b).addClass("active");
    $("#skillsbox ul.active").removeClass("active");
    var a = $(b).data("target");
    $("#skillsbox").find("#" + a).addClass("active")
}
function clickQuestButton(b) {
    $(".questlist li.active").removeClass("active");
    $(b).addClass("active");
    var a = $(b).data("command");
    send(a)
}
function clickInviteButton(b) {
    if ($(b).parent(".invitecontent").length > 0) {
        $(".invitecontent li").removeAttr("style");
        $(b).attr("style", "background:#5B3416");
        var a = $(b).attr("data-uid");
        $(".dialog-buttons a:first").attr("data-command", "team invite " + a)
    }
    $(b).addClass("active")
}
function chuwuAction(h) {
    var b = $("#amount").html();
    b = parseInt(b, 10);
    var g = $(h).data("direct");
    if (!!g && b > 0) {
        var f = $(h).data("id");
        if (g == "get") {
            send(g + " " + f)
        } else {
            send(g + " " + b + " " + f)
        }
    }
    var e = $(".content").find("#amount");
    var c = e.html();
    e.html("0");
    var a = $(".numbers").find("#maxNumber").val();
    var d = parseInt(a, 10) - parseInt(c, 10);
    $(".numbers").find("#maxNumber").val(d)
}
function doNumAction(d) {
    var a = $("#amount", "#dialog").val();
    var c = $(d).data("direct");
    if (c == "jiali" || c == "roombid bid") {
        send(c + " " + a);
        closeDialog();
        return
    }
    if (a > 0) {
        var a = $("#amount").val();
        a = parseInt(a, 10);
        if (!!c && a > 0) {
            var b = $(d).data("id");
            send(c + " " + a + " " + b)
        }
        closeDialog();
        if (c == "quwu") {
            setTimeout(function () {
                $(".statuslist li.active").trigger("click")
            },
            500)
        }
    }
}
function loadPack(f) {
    if (!! !f.inventory) {
        return
    }
    f = f.inventory;
    packitems = f.items;
    var e = [];
    e.push('<div class="packcontent">');
    e.push("<div>", f.desc, "</div>");
    e.push('<ul class="packlist">');
    if (!!f.items && f.items.length > 0) {
        for (var d = 0; d < f.items.length; d++) {
            var b = f.items[d];
            if (!!b) {
                var c = !!b.amount ? b.amount : 1;
                e.push('<li id="', b.id, '" data-status="', a, '" onclick="clickPackItem(this);">');
                var a = !!b.status ? b.status : "";
                if (a == "wielded" || a == "worn") {
                    e.push('<span class="diamond"></span>')
                }
                e.push("<span>", b.name, "(", c, ")</span>");
                if (a == "weapon" || a == "armor") {
                    e.push('<span class="minibtn" data-id="', b.id, '" onclick="wield(this);">装备</span>')
                }
                e.push("</li>")
            }
        }
    } else {
        e.push("<li>你的背包暂无东西</li>")
    }
    e.push("</ul></div>");
    $("#statusbox #statusboxcontent").html(e.join(""));
    $("#statusbox").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function wield(b) {
    var a = $(b).data("id");
    if (!!a) {
        send("wield " + a);
        send("i")
    }
}
function clickPackItem(a) {
    clickObject(a);
    isFromPack = true
}
function loadChawu(e) {
    if (!! !e.chawu) {
        return
    }
    e = e.chawu;
    chawuitems = e.items;
    var d = [];
    d.push('<div class="packcontent">');
    d.push("<div>", e.desc, "</div>");
    d.push('<ul class="packlist">');
    if (!!e.items && e.items.length > 0) {
        for (var c = 0; c < e.items.length; c++) {
            var a = e.items[c];
            if (!!a) {
                var b = !!a.amount ? a.amount : 1;
                d.push('<li data-id="', a.id, '" data-command="quwu" data-actname="取出" data-amount="', b, '" onclick="showNumDialog(this);"><span>', a.name, "</span>(", b, ")</li>")
            }
        }
    } else {
        d.push("<li>你的储物袋暂无东西</li>")
    }
    d.push("</ul></div>");
    $("#statusbox #statusboxcontent").append(d.join(""))
}
function loadTitles(d) {
    $("#title").html("称号列表");
    titles.push(title);
    var c = [];
    c.push('<div class="content" id="titlelist">');
    c.push('<div class="currenttitle">当前称号：', d.current, "</div>");
    d = d.title;
    for (var b = 0; b < d.length; b++) {
        c.push('<ul class="titles"><li>', d[b].title, '</li><div data-id="', d[b].id, '" data-direct="title set" class="titlebtn" onclick="changeTitle(this)">更换</div>');
        var a = d[b].desc.replace("|", "</br>");
        c.push("<li>", a, "</li></ul>")
    }
    c.push("</div>");
    addPage(c.join(""));
    $("#titlelist").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function changeTitle(d) {
    var c = $(d).data("direct");
    var b = $(d).data("id");
    send(c + " " + b);
    var a = $(d).prev().html();
    $(".currenttitle").html("当前称号：" + a)
}
function showWeapon() {
    $("#title").html("备用武器");
    titles.push(title);
    var a = [];
    a.push('<div class="content" id="weaponset"></div>');
    addPage(a.join(""))
}
function loadWeapon(d) {
    var c = [];
    c.push('<div class="packcontent">');
    if (!!d.listweapon) {
        c.push('<div style="height:2em;">你的背包中有以下武器：</div>')
    } else {
        c.push('<div style="height:2em;">你的背包中没有武器。</div>')
    }
    c.push('<ul id="weapon0" class="packlist">');
    if (!!d.listweapon && d.listweapon.length > 0) {
        for (var b = 0; b < d.listweapon.length; b++) {
            var a = d.listweapon[b];
            if (!!a) {
                c.push('<li data-id="', a.id, '" onclick="chooseWeapon(this)"><span>', a.name, "</span></li>")
            }
        }
    }
    c.push("</ul></div>");
    $("#weaponset").html(c.join(""))
}
function loadWeaponSet(d) {
    var c = [];
    c.push('<div class="packcontent">');
    c.push('<div style="height:2em;">备用武器设置如下：</div>');
    c.push('<ul id="weapon1" class="packlist">');
    if (!!d.listweaponset && d.listweaponset.length > 0) {
        for (var b = 0; b < d.listweaponset.length; b++) {
            var a = d.listweaponset[b];
            if (a.name == 0) {
                c.push('<li data-id="', a.id, '" onclick="chooseWeapon(this)"><span>备用武器', b + 1, "</span></li>")
            } else {
                c.push('<li data-id="', a.id, '" onclick="chooseWeapon(this)"><span>', a.name, "</span></li>")
            }
        }
        if (b < 6) {
            c.push('<li data-command="setweapon augment" onclick="doAction2(this)" style="color:red;font-weight: bold;"><span>开启备用槽</span></li>');
            b++
        }
        for (; b < 6; b++) {
            c.push('<li onclick=""><span>未开启备用槽</span></li>')
        }
    }
    c.push("</ul></div>");
    $("#weaponset").append(c.join(""))
}
function chooseWeapon(c) {
    if ($(c).parents("#weapon0").length > 0) {
        $("#weapon0").find(".choosen").removeClass("choosen");
        $(c).addClass("choosen")
    } else {
        if ($(c).parents("#weapon1").length > 0) {
            $("#weapon1").find(".choosen").removeClass("choosen");
            $(c).addClass("choosen")
        }
    }
    if ($(".choosen").length == 2) {
        var a = $("#weapon0").find(".choosen").data("id");
        var b = $("#weapon1").find(".choosen").data("id");
        send("setweapon " + b + " " + a);
        send("listweapon");
        send("listweaponset")
    }
}
function showNumDialog(d) {
    var g = $(d);
    var c = g.data("amount");
    if (typeof (c) == "undefined") {
        c = 99999
    }
    var a = g.data("id");
    var b = g.data("command");
    var i = g.data("actname");
    var h = [];
    h.push('<div class="numberbox" style="height: 20%;margin: auto;padding-top: 20%;">');
    h.push('<div class="numbers">');
    h.push('<span class="reduce" onclick="reduceItem();">-</span><input type="tel" min="0" max="', c, '" onchange="intMax(this)" class="number" id="amount" value="0"/><span class="plus" onclick="plusItem();">+</span></div>');
    h.push('<div class="max" onclick="maxItem();">MAX</div>');
    h.push("</div>");
    var e = $("#dialog");
    if (b == "quwu" || b == "jiali" || b == "withdraw" || b == "deposit" || b == "roombid bid") {
        e.find(".dialog-title").html(g.html())
    } else {
        e.find(".dialog-title").html($("#title").html())
    }
    e.find(".dialog-content").html(h.join(""));
    var f = [];
    f.push('<div class="dialogbtn"><a data-direct="', b, '" data-id="', a, '" onclick="doNumAction(this);">', i, "</a></div>");
    f.push('<div class="dialogbtn"><a onclick="closeDialog();">取消</a></div>');
    e.find(".dialog-buttons").html(f.join(""));
    e.show()
}
function showInputDialog(g) {
    var e = $(g);
    var f = e.data("command");
    var d = e.data("actname");
    var c = [];
    c.push('<input type="text" class="msginput"/>');
    var a = $("#dialog");
    a.find(".dialog-title").html(e.html());
    a.find(".dialog-content").html(c.join(""));
    var b = [];
    b.push('<div class="dialogbtn"><a data-direct="', f, '" onclick="sendAnswer(this);">', e.html(), "</a></div>");
    b.push('<div class="dialogbtn"><a onclick="closeDialog();">取消</a></div>');
    a.find(".dialog-buttons").html(b.join(""));
    a.show()
}
function sendAnswer(d) {
    var a = $("#dialog").find("input");
    if (!! !a) {
        return
    }
    var c = a.val();
    var b = /^[0-9a-zA-Z\u2E80-\u9FFF]+$/;
    if (b.test(c)) {
        send($(d).data("direct") + " " + c);
        closeDialog()
    } else {
        a.val("请输入信息")
    }
}
function showBackBtn() {
    $("#back").show();
    $("#nav-leftbtn").hide();
    $("#nav-rightbtn").hide()
}
function hideBackBtn() {
    $("#back").hide();
    $("#nav-leftbtn").show();
    $("#nav-rightbtn").show()
}
function addBasicstatus(b) {
    var a = $(b).data("command");
    if (a == "levelup") {
        send(a)
    } else {
        send("fen " + a + " 1")
    }
    $(".statuslist > li").eq(0).trigger("click")
}
function loadButtonList() {
    var a = [];
    a.push('<div class="statusbuttons">');
    a.push('<span class="iconbutton" onclick="mySkills();"><span>我的</br>技能</span></span>');
    a.push('<span class="iconbutton" data-direct="look me" onclick="doAction(this);"><span>我的</br>容貌</span></span>');
    a.push('<span class="iconbutton" data-command="title" onclick="doAction2(this);"><span>更换</br>称号</span></span>');
    a.push('<span class="iconbutton" data-command="jiali" data-amount="0" data-actname="加力" onclick="showNumDialog(this);">加力</span>');
    a.push('<span class="iconbutton" data-command="map list" onclick="doAction2(this);"><span>地图</br>设置</span></span>');
    a.push('<span class="iconbutton active" onclick="dazuo(this);">打坐</span>');
    a.push('<span class="iconbutton" data-command="halt" onclick="doAction2(this);"><span>停止</br>动作</span></span>');
    a.push('<span class="iconbutton" onclick="liaoshang();" >疗伤</span>');
    a.push("</div>");
    $("#statusbox #statusboxcontent").html(a.join(""));
    send("jiali info")
}
function loadBasicstatus(f) {
    var n = f.levelup;
    f = f.basicstatus;
    if (!!f) {
        var c = [];
        var k = 0;
        c.push('<ul class="basicstatuslist">');
        var j = 0;
        for (var g = 0; g < f.length; g++) {
            if (f[g].key == "giftpoint") {
                j = f[g].content
            }
        }
        j = parseInt(j, 10);
        for (var g = 0; g < f.length; g++) {
            var d = f[g].key == "title" ? "" : f[g].name;
            var e = k % 2 != 0 ? "yellowtext" : "";
            var b = k % 2 != 0 ? "65%" : "35%";
            e = k == 0 ? "nameitem" : e;
            if (k == 0) {
                c.push('<li class="', e, '" data-key="', f[g].key, '"><span></span><span>', f[g].content, "</span></li>")
            } else {
                if (j > 0 && (f[g].key == "str" || f[g].key == "int" || f[g].key == "con" || f[g].key == "dex")) {
                    switch (f[g].key) {
                        case "str":
                            tltp = "膂力：影响物理精通，招架精通，偏斜精通，以及基础物理伤害量。";
                            break;
                        case "int":
                            tltp = "悟性：影响内力精通，内力防御，恢复效率，少量影响抵抗等级。一些门派武功需要较高悟性。";
                            break;
                        case "con":
                            tltp = "根骨：影响物理减伤，大量抵抗等级，气血量，内力量。";
                            break;
                        case "dex":
                            tltp = "身法：影响闪避精通，偏斜精通，暴击等级。";
                            break
                    }
                    c.push('<li style="width:', b, '", class="', e, '" data-key="', f[g].key, '" id="basic" data-tltp="', tltp, '"><span>【', d, "】</span><span>", f[g].content, '</span><span class="add" data-command="', f[g].key, '" onclick="addBasicstatus(this);"></span></li>')
                } else {
                    if (f[g].key == "level" && n == 1) {
                        c.push('<li style="width:', b, '", class="', e, '" data-key="', f[g].key, '" id="basic"><span>【', d, "】</span><span>", f[g].content, '</span><span class="add" data-command="levelup" onclick="addBasicstatus(this);"></span></li>')
                    } else {
                        c.push('<li style="width:', b, '", class="', e, '" data-key="', f[g].key, '"><span>【', d, "】</span><span>", f[g].content, "</span></li>")
                    }
                }
            }
            k++;
            if (f[g].key == "neili") {
                var l = f[g].content;
                var a = l.indexOf("(+");
                var h = l.lastIndexOf(")");
                var m = l.substring(a + 2, h);
                if (parseInt(m, 10) == 0) {
                    isJiali = false
                } else {
                    isJiali = true
                }
            }
        }
        c.push("</ul>")
    } else {
        c.push("<div>暂无内容</div>")
    }
    c.push('<div class="blank"></div>');
    $("#statusbox #statusboxcontent").html(c.join(""));
    $("#statusboxcontent").mCustomScrollbar({
        axis: "y",
        setWidth: "100%",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function liaoshang() {
    send("yun recover");
    send("yun heal")
}
function setJiali(a) {
    if ($('.iconbutton[data-command="jiali"]').length > 0) {
        $('.iconbutton[data-command="jiali"]').attr("data-amount", a.max)
    }
}
function dazuo(a) {
    if ($(a).hasClass("active")) {
        $(a).html("停止").removeClass("active");
        send("dazuo")
    } else {
        $(a).html("打坐").addClass("active");
        send("halt")
    }
}
function updateStatus(b) {
    if (b.subtype == "dazuo") {
        if (!!b.result && b.result == "stop") {
            $("#dazuo").html("打坐").addClass("active")
        } else {
            var a = b.updateStatus;
            $("#statusbox .basicstatuslist").find('li[data-key="' + a.key + '"]').find("span").find("span").html(a.content);
            $("#dazuo").html("停止").removeClass("active")
        }
    } else {
        if (b.subtype == "research") {
            if (b.result == "progress") {
                $("#research").html("停止<br/>研究");
                $("#research").attr("onclick", "doAction2(this);")
            } else {
                if (b.result == "stop") {
                    $("#research").html("开始<br/>研究");
                    $("#research").attr("onclick", "doAction(this);")
                }
            }
        }
    }
}
function loadCombatstatus(c) {
    c = c.combatstatus;
    if (!!c) {
        var a = [];
        var b = 0;
        a.push('<ul class="basicstatuslist">');
        for (key in c) {
            if (b % 2 != 0) {
                a.push('<li class="yellowtext"><span>【', key, "】</span><span>", c[key], "</span></li>")
            } else {
                a.push("<li><span>【", key, "】</span><span>", c[key], "</span></li>")
            }
            b++
        }
        a.push("</ul>")
    } else {
        a.push("<div>暂无内容</div>")
    }
    a.push('<div class="fightsetting" onclick="showFightSetting();"><span>战斗</br>设置</span></div>');
    a.push('<div class="fightsetting" data-direct="listweapon" onclick="doAction(this);"><span>备用</br>武器</span></div>');
    $("#statusbox #statusboxcontent").html(a.join(""));
    $("#statusboxcontent").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function showFightSetting() {
    $("#title").html("战斗设置");
    titles.push(title);
    var a = [];
    a.push('<div class="content" id="fightSettingPage">');
    a.push("</div>");
    addPage(a.join(""));
    send("listpfmset")
}
function showFightSettingContent(f) {
    var a = [];
    var j = f.listpfmset;
    a.push('<ul class="fightbuttons">');
    var e = [-1, 2, 5, 8, 0, 3, 6, 9, 1, 4, 7, -1];
    for (var g = 0; g < 12; g++) {
        var h = e[g];
        if (g == 0) {
            if (isFighting) {
                a.push('<li onclick="doAction2(this);" data-command="yun recover"><span>疗伤</span></li>')
            } else {
                a.push('<li class="disabled"><span>疗伤</span></li>')
            }
        } else {
            if (g == 11) {
                if (isFighting) {
                    a.push('<li onclick="doAction2(this);" data-command="flee"><span>逃跑</span></li>')
                } else {
                    a.push('<li class="disabled"><span>逃跑</span></li>')
                }
            } else {
                var b = "",
                d = "";
                if (g == 4 || g == 8) {
                    d = "listmed"
                } else {
                    if (g == 1 || g == 2 || g == 3 || g == 5 || g == 6 || g == 7 || g == 9 || g == 10) {
                        d = "listpfm"
                    }
                }
                if (h >= 0 && !!j[h]) {
                    var c = j[h].name;
                    if (c != "0") {
                        var k = c.split("|");
                        b = k[0];
                        if (isFighting) {
                            d = k[1]
                        }
                    }
                }
                b = b == "0" ? "" : b;
                d = !!d ? d : "";
                if (isFighting) {
                    a.push('<li onclick="doAction(this);" data-direct="', d, '" data-index="', h, '"><span>', b, "</span></li>")
                } else {
                    a.push('<li onclick="clickSetFightButton(this);" data-command="', d, '" data-index="', h, '"><span>', b, "</span></li>")
                }
            }
        }
    }
    a.push("</ul>");
    if (isFighting) {
        $("#fightPage .fightbuttons").html("").append(a.join(""))
    } else {
        a.push('<div id="fightButtonsDialog">');
        a.push('<div class="setfighttitle">');
        a.push('<input type="hidden" id="isMedicine"/>');
        a.push('<a class="emotebtn" onclick="selectFightButton();">确定</a>');
        a.push("</div>");
        a.push('<div id="setfightbtns">');
        a.push("</div>");
        a.push("</div>");
        $("#fightSettingPage").html(a.join(""))
    }
}
function showPfmCD(a) {
    $(".fightbuttons").find("li").each(function () {
        var b = $(this).data("direct");
        if (a.scd > 0 && !!b) {
            if (b.indexOf(a.name) >= 0) {
                showCDcover(this, a.scd)
            }
        }
        if (a.pcd > 0) {
            var c = $(this).data("index");
            if (c == 2 || c == 5 || c == 8 || c == 3 || c == 6 || c == 9 || c == 4 || c == 7) {
                if (!!$(this).attr("onclick")) {
                    showCDcover(this, a.pcd)
                }
            }
        }
    })
}
function showCDcover(c, b) {
    if ($(c).next().tagName != "DIV") {
        var a = $(c).position();
        $(c).attr("onclick", "");
        $(c).after('<div class="cdCover"></div>');
        $(c).next().css("left", a.left);
        $(c).next().css("top", a.top);
        $(c).next().css("margin-left", $(c).css("margin-left"));
        $(c).next().css("margin-top", $(c).css("margin-top"));
        $(c).next().width($(c).width());
        $(c).next().height($(c).height());
        $(c).next().animate({
            width: 0
        },
        b * 1000,
        function () {
            $(this).hide();
            $(this).prev().attr("onclick", "doAction(this)")
        })
    }
}
function loadQuests(e) {
    var d = e.master.split("|")[0];
    var c = [];
    c.push('<div class="questleft">今日任务：</div>');
    c.push('<div class="questright">宗师任务(', e.master.split("|")[0], "/100)[", e.master.split("|")[1], "]<br>师门任务(", e.family.split("|")[0], "/100)[", e.family.split("|")[1], "]</div>");
    e = e.quests;
    if (!!e) {
        for (var b = 0; b < e.length; b++) {
            var a = e[b].msg.replace("|", "<br>");
            c.push('<div class="questname"><span>', e[b].subtype, "</span> <span>", e[b].name, "</span></div>");
            c.push('<div class="questcontent"><span>', a, "</span></div>");
            c.push('<div class="questbutton">');
            if (!!e[b].isfugoto && e[b].isfugoto == "1") {
                c.push('<span data-command="fugoto ', e[b].taskid, '" onclick="doAction2(this);" class="iconbutton" style="width:40%">传送</span>')
            }
            c.push('<span data-command="giveup ', e[b].taskid, '" onclick="doCloseAction(this);" class="iconbutton" style="width:40%">放弃</span></div>')
        }
    }
    $("#statusbox #statusboxcontent").html(c.join(""));
    $("#statusboxcontent").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function loadTeam(d) {
    d = d.team;
    var b = [];
    b.push('<div class="teammsg"></div>');
    if (!!d) {
        b.push('<table class="teamtable">');
        for (var a = 0; a < 4; a++) {
            if (!!d[a]) {
                b.push('<tr bgcolor="#332518"><td width="20%" align="center">', d[a].family, "</td>");
                b.push('<td width="20%" align="center">', d[a].level, "级</td>");
                if (d[a].leader == "1") {
                    b.push('<td width="40%" align="center">', d[a].name, "【队长】</td>");
                    var c = d[a].name
                } else {
                    b.push('<td width="40%" align="center">', d[a].name, "</td>")
                }
                b.push('<td width="20%" align="center">暂无操作</td></tr>');
                b.push("<tr><td> </td><td> </td><td> </td><td> </td></tr>")
            } else {
                b.push('<tr bgcolor="#332518"><td> </td><td> </td><td> </td><td> </td></tr>');
                b.push("<tr><td> </td><td> </td><td> </td><td> </td></tr>")
            }
        }
        b.push("</table>")
    }
    b.push('<div class="teambutton"><ul class="actions">');
    b.push('<li onclick="showInviteDialog();" class="longbutton">邀请组队</li>');
    if (!!d && d.length > 0) {
        b.push('<li data-command="team dismiss" onclick="doCloseAction(this);" class="longbutton">离开队伍</li>')
    }
    b.push("</ul></div>");
    $("#statusbox #statusboxcontent").html(b.join(""));
    if (d.length == 0) {
        $(".teammsg").html("<li>你现在并没有参加任何队伍。</li>")
    } else {
        $(".teammsg").html('<li style="color:#0CF3FF";>' + c + "的队伍</li>")
    }
}
function loadIntroducer(b) {
    var a = [];
    a.push('<div class="introducer"><li class="introducertitle">我的推广成果：</li><li>我已经邀请了', b.intronum, "人进入游戏。</li><li>我已经累计获利", b.earn, "元宝。</li>");
    var c = window.location.href + "enroll?inv=" + b.myid;
    a.push('<li class="introducertitle">我的推广链接：</li><li class="introducerurl" onclick="goCopy(this)">', c, '</li><li>点击以上链接可复制到剪贴板</li><li style="text-align:left;">1.使用你的推广链接或者使用你的邮箱作为邀请人注册的用户将被记录为你的推广成果。</br>2.推广来的用户，只要在游戏中充值，那么其充值额的10%将作为你的获利，直接出现在你的元宝账户内。</br>3.如有疑问请联系官方群的客服。</li></div>');
    $("#statusbox #statusboxcontent").html(a.join(""));
    $(".introducer").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function goCopy(b) {
    var a = new Clipboard(b, {
        text: function () {
            return $(b).text()
        }
    });
    a.on("success",
    function (c) {
        alert("您已成功复制推广链接，请使用CTRL+V进行粘贴。")
    });
    a.on("error",
    function (c) {
        alert("复制失败，请重试。。。")
    })
}
function clickSetFightButton(b) {
    var a = $(b).data("command");
    if (!!a) {
        $(".fightbuttons li.active").removeClass("active");
        $(b).addClass("active");
        send(a)
    }
}
function showSetFightOptions(d, f) {
    var b = [];
    d = f ? d.listmed : d.listpfm;
    $("#setfightbtns").html("");
    if (d.length == 0) {
        $("#fightButtonsDialog").hide();
        return
    }
    var c = $(document).width();
    var e = Math.ceil(c - 60) / 5;
    for (var a = 0; a < d.length; a++) {
        b.push('<span data-name="' + d[a].name + '" data-id="' + d[a].id + '"style="width:' + e + 'px" onclick="selectButton(this);">' + d[a].name + "</span>")
    }
    $("#isMedicine").val(f);
    $("#setfightbtns").append(b.join(""));
    $("#fightButtonsDialog").show()
}
function selectButton(a) {
    $("#setfightbtns span.active").removeClass("active");
    $(a).addClass("active")
}
function selectFightButton() {
    var c = $("#setfightbtns span.active");
    if (c.length == 0) {
        $("#fightButtonsDialog").hide();
        return
    }
    var b = c.data("name");
    var e = c.data("id");
    var a = $(".fightbuttons li.active").data("index");
    var d = $("#isMedicine").val();
    if (d == "true") {
        send("setpfm " + a + " " + b + " eat " + e + "#medicine")
    } else {
        send("setpfm " + a + " " + b + " " + e)
    }
    send("listpfmset")
}
function closeDialog() {
    $("#dialog").find(".dialog-content").html("");
    $("#dialog").find(".dialog-buttons").html("");
    $("#dialog").find(".dialog-content").css("background-image", "");
    $("#dialog").hide()
}
function closeDialog1() {
    $(".dialogbox").removeAttr("style");
    $(".dialog-title").removeAttr("style");
    $(".dialog-content").removeAttr("style");
    $("#dialog").find(".dialog-content").html("");
    $("#dialog").find(".dialog-buttons").html("");
    $("#dialog").find(".dialog-content").css("background-image", "");
    $("#dialog").hide()
}
function showConfirmDialog(c) {
    var a = $("#dialog");
    a.find(".dialog-title").html("提示");
    c = c.replace(/\n/g, "<br/>");
    c = c.replace("|", "<br/>");
    a.find(".dialog-content").html('<div class="dialog-message">' + c + "</div>");
    var b = [];
    b.push('<div class="dialogbtn"><a data-command="y" onclick="clickConfirmDialogButton(this);">是</a></div>');
    b.push('<div class="dialogbtn"><a data-command="n" onclick="clickConfirmDialogButton(this);">否</a></div>');
    a.find(".dialog-buttons").html(b.join(""));
    a.show()
}
function showVipConfirmDialog(i, c, j) {
    var f = $("#dialog");
    var g;
    if (c == "shop") {
        g = "元宝";
        f.find(".dialog-title").html("商城购买提示")
    } else {
        if (c == "fmshop") {
            switch (j) {
                case "门贡":
                    g = "门贡";
                    f.find(".dialog-title").html("门派贡献兑换提示");
                    break;
                case "经验":
                    g = "经验";
                    f.find(".dialog-title").html("经验商店购买提示");
                    break;
                case "喜鹊羽":
                    g = "喜鹊羽";
                    f.find(".dialog-title").html("喜鹊羽兑换提示");
                    break;
                default:
                    return
            }
        }
    }
    var e = [];
    e.push('<div class="dialog-message" style="padding-top: 1em;"> </div><table class="viptable">');
    e.push("<tr><th>名称</th><th>数量</th><th>价格</th></tr>");
    var a = 0;
    var b = 0;
    if (i == "1") {
        $(".shoplist li").each(function () {
            var m = $(this).find(".shop-num").val();
            m = parseInt(m, 10);
            if (m > 0) {
                var o = $(this).find("span").eq(0);
                var p = o.data("id");
                var n = o.data("value");
                n = parseInt(n, 10);
                var l = o.html();
                e.push("<tr><td>", o.html(), '</td><td id="vipnum" data-id="', p, '">', m, '</td><td id="vipsum">', m * n, g, "</td></tr>");
                a += m;
                b += m * n
            }
        })
    } else {
        if (i == "2") {
            var d = $("#amount").val();
            d = parseInt(d, 10);
            var k = $(".max").data("value");
            b = d * k;
            e.push("<tr><td>", $("#title").html(), '</td><td id="vipnum">', d, '</td><td id="vipsum">', d * k, g, "</td></tr>")
        }
    }
    if (b == 0) {
        return
    }
    f.find(".dialog-content").html(e.join(""));
    $(".dialog-message").html("您预计总消费为" + b + g + "。<br/>您将购买以下商城物品：");
    f.find(".dialog-content").css({
        "overflow-y": "auto"
    });
    var h = [];
    h.push('<div class="dialogbtn"><a onclick="closeDialog();">取消</a></div>');
    if (i == "1") {
        h.push('<div class="dialogbtn"><a data-command="', c, ' buy" onclick="buyItems();">确定购买</a></div>')
    }
    if (i == "2") {
        h.push('<div class="dialogbtn"><a onclick="doVipbuy(this,&#39;', c, '&#39;);">确定购买</a></div>')
    }
    f.find(".dialog-buttons").html(h.join(""));
    f.find(".dialogbtn a").css({
        "font-size": "1.2em",
        "line-height": "1.9em"
    });
    f.show()
}
function showInviteMsg(a) {
    showConfirmDialog(a.msg);
    $(".dialog-buttons a:first").attr("data-command", "team accept " + a.fromid);
    $(".dialog-buttons").find("a").eq(1).attr("onclick", "closeDialog()")
}
function showMazemapDialog(c) {
    var a = $("#dialog");
    a.find(".dialog-title").html("迷宫地图");
    a.find(".dialog-content").html('<div class="dialog-maze"><canvas id="maze_canvas" style="width: 100%;"></canvas></div>');
    var b = [];
    b.push('<div class="dialogbtnclose"><a onclick="closeDialog();">关闭</a></div>');
    a.find(".dialog-buttons").html(b.join(""));
    a.show();
    draw_all(c.size, c.current_room, c.enter_room, c.out_room, c.hwall, c.wall)
}
function showDepositDialog() {
    var a = $("#dialog");
    a.find(".dialog-title").html("充值方式");
    a.find(".dialog-content").html('<div class="depositbtn" onclick="showAlipay()">支付宝支付</div><div class="depositbtn" onclick="showWechat()">微信支付</div>');
    var b = [];
    b.push('<div class="dialogbtnclose"><a onclick="closeDialog();">关闭</a></div>');
    a.find(".dialog-buttons").html(b.join(""));
    a.show()
}
function showMapDialog() {
    var a = $("#dialog");
    a.find(".dialog-title").html('<span>选择地图显示</span><div class="clsDlgbtn" onclick="closeDialog();">X</div>');
    a.find(".dialog-content").html('<div class="depositbtn" onclick="showMap(&#39;world&#39;)"  style="margin-top:5%;">世界地图</div><div class="depositbtn" data-command="map show" onclick="doAction2(this)" style="margin-top:5%;">区域地图</div><div class="depositbtn" data-command="look" onclick="doAction2(this);closeDialog();" style="margin-top:5%;">查看场景</div>');
    a.find(".dialog-content").css("overflow", "visible");
    a.find(".dialog-buttons").html("");
    a.show()
}
function loadNowMap(a) {
    showMap("area", a.nowmap)
}
function showMap(b, a) {
    var c = $("#map");
    closeDialog();
    if (b == "world") {
        $(".mapbox").html('<img id="mappic" src="../images/world.png"/>')
    } else {
        if (b == "area") {
            $(".mapbox").html('<img id="mappic" src="../images/' + a + '.png"/>')
        }
    }
    c.show();
    document.getElementById("mappic").onload = function () {
        new RTP.PinchZoom($(".mapbox").find("img"), {
            tapZoomFactor: 4,
            maxZoom: 8
        })
    }
}
function closeMap() {
    $("#map").hide()
}
function showAlipay() {
    var a = $("#dialog");
    a.find(".dialog-content").html("");
    a.find(".dialog-content").css("background-image", "url(../images/alipay.jpg)")
}
function showWechat() {
    var a = $("#dialog");
    a.find(".dialog-content").html("");
    a.find(".dialog-content").css("background-image", "url(../images/wechat.jpg)")
}
function showInviteDialog() {
    var c = $("#dialog");
    $(".dialogbox").attr("style", "height:60%");
    c.find(".dialog-title").html("邀请组队");
    $(".dialog-title").attr("style", "height:10%");
    var d = [];
    d.push('<ul class="invitelist">');
    d.push('<li onclick="clickInviteButton(this);">最近联系人</li>');
    d.push('</ul><div class="invitecontent">');
    var f = window.localStorage.getItem("friends");
    var b = JSON.parse(f);
    for (var a = 0; a < b.length; a++) {
        d.push('<li data-uid="', b[a].id, '" onclick="clickInviteButton(this);">', b[a].name, "</li>")
    }
    d.push("</div>");
    c.find(".dialog-content").html("").append(d.join(""));
    $(".dialog-content").attr("style", "height:75%");
    var e = [];
    e.push('<div class="dialogbtn"><a data-command="team invite" onclick="clickConfirmDialogButton(this);">邀请</a></div>');
    e.push('<div class="dialogbtn"><a onclick="closeDialog1()">关闭</a></div>');
    c.find(".dialog-buttons").html(e.join(""));
    c.show();
    $(".invitelist > li").eq(0).trigger("click")
}
function clickConfirmDialogButton(b) {
    var a = $(b).data("command");
    send(a);
    closeDialog();
    if (a == "y") {
        if (!!back) {
            $("#back").trigger("click");
            back = false
        }
        if ($(".questleft").length == 1) {
            send("quest")
        }
        if ($("#weapon0").length == 1) {
            send("listweapon");
            send("listweaponset")
        }
    }
}
function shopMapShop(d) {
    $("#title").html("地图商店");
    titles.push(title);
    var c = [];
    c.push('<div class="content">');
    c.push('<div id="maplist"><ul class="maplist">');
    var a = d.items;
    if (!!a && a.length > 0) {
        for (var b = 0; b < a.length; b++) {
            var e = a[b];
            c.push("<li><span>", e.name, "</span></br><span>价格：", e.price, "</span><span>  已售：", e.saled, "份</span>");
            c.push('<div data-id="', e.id, '" data-direct="buy" onclick="doAction(this)">购买</div></li>')
        }
    }
    c.push("</ul></div>");
    c.push("</div>");
    addPage(c.join(""));
    $("#maplist").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function showShop(d) {
    var f = titles[titles.length - 1] + "贩卖的物品";
    $("#title").html(f);
    titles.push(f);
    var c = [];
    c.push('<div class="content">');
    c.push('<div class="shoptitle"><span>物品列表</span></div>');
    c.push('<div id="shoplist"><ul class="shoplist">');
    var a = d.shoplist.items;
    if (!!a && a.length > 0) {
        for (var b = 0; b < a.length; b++) {
            var e = a[b];
            c.push('<li data-id="', e.id, '"><span>', e.name, "</span><span>", e.value, "</span>");
            c.push('<span><span class="shop-plus" onclick="reduceItemsNum(this)"><a>-</a></span><input type="tel" min="0" max="', MAX_SHOP, '" value="0" onchange="intMax(this)" class="shop-num"/><span class="shop-add" onclick="addItemsNum(this);"><a>+</a></span></span>');
            c.push("</li>")
        }
    }
    c.push("</ul></div>");
    c.push('<div class="buybutton"><a class="buy-btn" onclick="buyItems();">购买</a></div>');
    c.push("</div>");
    addPage(c.join(""));
    $("#shoplist").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function addItemsNum(d) {
    var b = $(d).parent();
    var c = $(".shop-num", b).val();
    var a = $(".shop-num", b).attr("max");
    c = parseInt(c, 10) + 1;
    if (a != "undefined" && c >= a) {
        c = a
    }
    if (c > MAX_SHOP) {
        c = MAX_SHOP
    }
    $(".shop-num", b).val(c)
}
function reduceItemsNum(c) {
    var a = $(c).parent();
    var b = $(".shop-num", a).val();
    b = parseInt(b, 10);
    if (b == 0) {
        return
    }
    b = b - 1;
    $(".shop-num", a).val(b)
}
function buyItems() {
    $(".shoplist li").each(function () {
        var b = $(this);
        var c = b.data("id");
        targetEl = b.find(".shop-num");
        var a = targetEl.val();
        a = parseInt(a, 10);
        if ($("#title").text() == "会员商城" && a > 0) {
            command = "shop buy " + a + " " + c;
            send(command);
            targetEl.val("0");
            closeDialog()
        } else {
            if ($("#title").text() == "门派贡献兑换" || $("#title").text() == "经验商店" || $("#title").text() == "喜鹊羽兑换" && a > 0) {
                command = "fmshop buy " + a + " " + c;
                send(command);
                targetEl.val("0");
                closeDialog()
            } else {
                if (a > 0) {
                    command = "buy " + a + " " + c;
                    send(command);
                    targetEl.val("0")
                }
            }
        }
    })
}
function mySkills() {
    skillNpc = {
        name: "我的技能",
        id: ""
    };
    send("skills")
}
function showSkills(e) {
    $("#title").html(skillNpc.name);
    titles.push(skillNpc.name);
    var b = $(".skillsContent");
    if (b.length == 0) {
        var a = [];
        a.push('<div class="content skillsContent">');
        a.push('<ul class="skillslist">');
        a.push('<li onclick="clickSkillsButton(this);" data-target="basicskills">基础技能</li>');
        a.push('<li onclick="clickSkillsButton(this);" data-target="martialkills">特殊技能</li>');
        a.push('<li onclick="clickSkillsButton(this);" data-target="knowledgeskills">知识技能</li>');
        a.push('</ul><div id="skillsbox"><ul id="basicskills"></ul><ul id="martialkills"></ul><ul id="knowledgeskills"></ul></div></div>');
        addPage(a.join(""));
        var g = e.skills;
        if (!!g && g.length > 0) {
            for (var f = 0; f < g.length; f++) {
                var h = g[f].subtype;
                var k = g[f].skills;
                var d = [];
                if (!!k && k.length > 0) {
                    for (var c = 0; c < k.length; c++) {
                        d.push('<li data-id="', k[c].id, '" data-direct="checkskill" onclick="doAction(this);";><span class="skillname">', k[c].name, '</span><span class="skilldesc">', k[c].desc, '</span><span class="skilllevel">', k[c].level, "级</span></li>")
                    }
                } else {
                    d.push("<li>暂无</li>")
                }
                if (h == "basic") {
                    $("#basicskills").html("").append(d.join(""))
                } else {
                    if (h == "martial") {
                        $("#martialkills").html("").append(d.join(""))
                    } else {
                        if (h == "knowledge") {
                            $("#knowledgeskills").html("").append(d.join(""))
                        }
                    }
                }
            }
        }
        $(".skillslist > li").eq(0).trigger("click");
        $("#skillsbox").mCustomScrollbar({
            axis: "y",
            mouseWheel: {
                enable: true
            },
            advanced: {
                updateOnContentResize: true
            }
        })
    }
}
function showSkillDetail(c) {
    $("#title").html("技能详情");
    titles.push(title);
    var b = [];
    b.push('<div class="content">');
    b.push('<div id="skilldetailbox" style="width:100%;height:95%;"><div class="skillsdesc">');
    var e = c.checkskill.split("|");
    for (var a = 0; a < e.length; a++) {
        b.push(e[a] + "</br>")
    }
    b.push("</div>");
    b.push('<div class="npclist"><ul class="actions">');
    if (!!skillNpc.id) {
        b.push('<li data-command="xue ', skillNpc.id, " about ", c.skillid, '" onclick="doAction2(this);"><span>学习</span></li>');
        b.push('<li data-command="xue ', skillNpc.id, " about 10 ", c.skillid, '" onclick="doAction2(this);"><span class="nowrap">学习</br>十次</span></li>')
    } else {
        if (!!c.button && c.button.length > 0) {
            for (var a = 0; a < c.button.length; a++) {
                var d = c.button[a];
                b.push('<li data-command="', d.command, '" onclick="doAction2(this);" class="longbutton"><span>', d.name, "</span></li>")
            }
        }
        b.push('<li data-command="fangqi ', c.skillid, '" onclick="doAction2(this);" class="longbutton"><span>放弃</span></li>')
    }
    b.push("</ul>");
    b.push("</div></div>");
    addPage(b.join(""));
    $("#skilldetailbox").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    })
}
function loadMapset(f) {
    $("#title").html("地图设置");
    titles.push(title);
    var e = [];
    e.push('<div class="content">');
    e.push('<div class="researchbox" style="margin-top: 1em;"><ul class="researchinfo">');
    f = f.areas;
    if (!!f && f.length > 0) {
        for (var d = 0; d < f.length; d++) {
            e.push("<li><span>", f[d].area, "</span></li>");
            e.push('<ul class="researchdetail">');
            for (var b = 0; b < f[d].maps.length; b++) {
                e.push('<li><span class="mapname">', f[d].maps[b].name, "</span>");
                e.push('<div class="researchbtn" style="margin-right:2em;" data-id="', f[d].maps[b].mapid, '" data-direct="map set" data-used="', f[d].maps[b].used, '" onclick="doAction(this);">使用</div>');
                e.push("</li>")
            }
            e.push("</ul>")
        }
    }
    e.push("</ul></div></div>");
    addPage(e.join(""));
    $(".researchbox").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    });
    checkMapUsed();
    var a = $(".researchdetail");
    var c = $(".researchinfo");
    a.hide();
    a.first().delay(400).slideDown(700);
    a.on("click", "li",
    function () {
        a.siblings().find("li").removeClass("chosen");
        $(this).addClass("chosen")
    });
    c.on("click", "li",
    function () {
        $(this).next(".researchdetail").slideToggle().siblings(".researchdetail").slideUp()
    });
    c.children("li:last-child").on("click",
    function () {
        c.fadeOut().delay(500).fadeIn()
    })
}
function checkMapUsed() {
    $(".researchdetail").find(".researchbtn").each(function () {
        if (!!$(this).data("used")) {
            $(this).hide()
        }
    })
}
function showResearch(f) {
    $("#title").html(f.name);
    titles.push(title);
    var d = f.id;
    var b = [];
    b.push('<div class="content">');
    var n = f.points.split("|");
    b.push('<div class="researchpoint"><span class="researchspan">当前研究等级', n[0], "/", n[1], '</span><span class="researchspan">剩余研究点：', n[2], "</span>");
    if (f.progress == 0) {
        var o = "width: 100%;border-bottom-left-radius: 3.5em;border-top-left-radius: 3.5em;"
    } else {
        o = "width: " + (100 - f.progress) + "%;"
    }
    b.push('<span>研究进度：</span><span class="researchprogbox"><span class="researchpgbg"></span><span class="researchpgnow" style="', o, '"></span><span class="researchvalue">', f.progress, "%</span></div>");
    b.push('<div class="startresearch" id="research" data-direct="research" data-command="halt" data-id="', d, '" onclick="doAction(this);">开始<br/>研究</div>');
    b.push('<div class="researchbox"><ul class="researchinfo">');
    f = f.researchinfo;
    for (var g = 0; g < f.length; g++) {
        var a = f[g].level.split("|");
        b.push("<li><span>", f[g].name, '</span><div class="level">', a[0], "/", a[1], "</div></li>");
        b.push('<ul class="researchdetail">');
        for (var e = 0; e < f[g].detail.length; e++) {
            var m = f[g].detail[e].value.split("|");
            var l = "";
            var k = m[0] * 100 / m[1];
            k = k == "NaN" ? 0 : Math.floor(k);
            if (k == 100) {
                l = "green"
            } else {
                if (k >= 50) {
                    l = "orange"
                } else {
                    if (k > 0) {
                        l = "red"
                    }
                }
            }
            b.push('<li><span class="researchlevel" style="color:', l, '">', e + 1, '级</span><div class="valuebox">');
            b.push('<div class="valueprogress" style="width:', k, "%;background:", l, '"></div><div class="valuebg"></div><div class="researchvalue">', m[0], "%/", m[1], "%</div>");
            b.push("</div>");
            if (k == 0 && n[2] != 0) {
                b.push('<div class="researchbtn" data-direct="researchset ', f[g].id, " ", e + 1, " of ", d, '" onclick="doResearch(this);">分配</div>')
            } else {
                if (k < 100 && k > 0) {
                    b.push('<div class="researchbtn" data-direct="researchset ', f[g].id, " ", e + 1, " of ", d, '" onclick="doResearch(this);">补缀</div>')
                }
            }
            b.push("</li>")
        }
        b.push("</ul>")
    }
    b.push("</ul></div>");
    b.push("</div>");
    addPage(b.join(""));
    $(".researchbox").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    });
    var h = $(".researchdetail");
    var c = $(".researchinfo");
    h.hide();
    h.first().delay(400).slideDown(700);
    h.on("click", "li",
    function () {
        h.siblings().find("li").removeClass("chosen");
        $(this).addClass("chosen")
    });
    c.on("click", "li",
    function () {
        $(this).next(".researchdetail").slideToggle().siblings(".researchdetail").slideUp()
    });
    c.children("li:last-child").on("click",
    function () {
        c.fadeOut().delay(500).fadeIn()
    })
}
function showChat() {
    var a = $("#chatPage");
    if (a.length == 0) {
        var b = [];
        b.push('<div id="chatPage" class="page">');
        b.push('<div class="title" id="nav">');
        b.push('<a class="btn left icon-map" id="nav-leftbtn" style="display: none;"></a>');
        b.push('<span id="title">聊天</span>');
        b.push('<a class="btn right icon-area" id="nav-rightbtn" style="display: none;"></a>');
        b.push('<a class="btn right icon-back" id="chatback" onclick="chatBack();"></a>');
        b.push("</div>");
        b.push('<div id="chatContent">');
        b.push('<div class="content chatContent">');
        b.push('<ul class="chattype">');
        b.push('<li onclick="clickChatButton(this);" data-type="all">全部</li>');
        b.push('<li onclick="clickChatButton(this);" data-type="chat">闲聊</li>');
        b.push('<li onclick="clickChatButton(this);" data-type="music">传闻</li>');
        b.push('<li onclick="clickChatButton(this);" data-type="tell">私聊</li>');
        b.push('<li onclick="clickChatButton(this);" data-type="say">场景</li>');
        b.push('<li onclick="clickChatButton(this);" data-type="family">师门</li>');
        b.push('<li onclick="clickChatButton(this);" data-type="rumor">谣言</li>');
        b.push("</ul>");
        b.push("</div>");
        b.push('<div id="innercontent">');
        b.push('<ul class="chatlist"></ul>');
        b.push("</div>");
        b.push("</div>");
        b.push('<div class="sendbox">');
        b.push('<span class="lookbox" onclick="showEmoteBox();">表情</span><span class="personbox" onclick="chooseFriend();">对象(无)</span>');
        b.push('<span class="inputbox"><input type="text" id="sendmessage"/></span>');
        b.push('<span class="sendbtn"  onclick="sendChatMessage();">发送</span>');
        b.push('<div class="emotebox" id="emotebox">');
        b.push('<div class="emotebtnbox">');
        b.push('<a class="emotebtn" onclick="clearEmote();">清除表情</a><a class="emotebtn" onclick="closeEmote();">关闭</a>');
        b.push("</div>");
        b.push('<div id="semote">');
        b.push(addEmote());
        b.push("</div>");
        b.push("</div>");
        b.push('<div id="friendbox">');
        b.push('<ul id="friendlist"></ul>');
        b.push("</div>");
        b.push("</div>");
        b.push("</div>");
        $("body").append(b.join(""));
        $(".chattype > li").eq(0).trigger("click");
        $(".dialog").hide();
        $("#chatPage").show();
        $("#innercontent").mCustomScrollbar({
            axis: "y",
            mouseWheel: {
                enable: true
            },
            advanced: {
                updateOnContentResize: true
            }
        });
        $("#innercontent").mCustomScrollbar("scrollTo", "bottom");
        $("#semote").mCustomScrollbar({
            axis: "y",
            mouseWheel: {
                enable: true
            },
            advanced: {
                updateOnContentResize: true
            }
        });
        refreshFriend();
        unreadMessage = 0;
        $(".messageNum").html("0").hide()
    }
}
function addEmote() {
    var e = ["back", "welcome", "hammer", "rou", "139", "me", "baixi", "ball", "blood", "miss", "mo", "18mo", "bao", "nali", "dahua", "buyao", "bye3", "cut1", "maimai", "xbc"];
    var b = [];
    var c = $(document).width();
    var d = Math.ceil(c - 60) / 5;
    for (var a = 0; a < e.length; a++) {
        b.push('<span data-emote="' + e[a] + '" style="width:' + d + 'px" onclick="clickEmote(this);">' + e[a] + "</span>")
    }
    return b.join("")
}
function showEmoteBox() {
    var a = $(".chattype .active").data("type");
    if (a == "tell") {
        return
    }
    $("#friendbox").removeClass("active");
    if ($("#emotebox").hasClass("active")) {
        $("#emotebox").removeClass("active")
    } else {
        $("#emotebox").addClass("active")
    }
}
function clickEmote(b) {
    var a = $(b).data("emote");
    $(".sendbox .lookbox").html(a);
    $("#sendmessage").attr("readonly", "readonly").addClass("disabled");
    $("#emotebox").removeClass("active")
}
function clearEmote() {
    $("#emotebox").removeClass("active");
    $("#sendmessage").val("");
    $(".sendbox .lookbox").html("表情");
    $("#sendmessage").removeAttr("readonly").removeClass("disabled")
}
function closeEmote() {
    $("#emotebox").hide()
}
function clearPerson() {
    $(".sendbox .personbox").html("对象(无)").data("uid", "")
}
function sendChatMessage() {
    var a = $(".chattype .active").data("type");
    if (a == "all") {
        a = "chat"
    }
    var d = $(".sendbox .lookbox").html();
    var b = $("#sendmessage").val();
    if (a == "say") {
        var c = $(".sendbox .personbox").data("uid");
        if (d != "表情" && !!!c) {
            send(d)
        } else {
            if (d != "表情" && !!c) {
                send(d + " " + c)
            } else {
                send("say " + b)
            }
        }
    } else {
        if (a == "tell") {
            var c = $(".sendbox .personbox").data("uid");
            if (!! !c) {
                alert("请选择私聊对象");
                return
            } else {
                send("tell " + c + " " + b)
            }
        } else {
            if (d != "表情") {
                var c = $(".sendbox .personbox").data("uid");
                if (!!c) {
                    send(a + "* " + d + " " + c)
                } else {
                    send(a + "* " + d)
                }
            } else {
                if (!!b) {
                    send(a + " " + b)
                }
            }
        }
    }
    clearEmote();
    clearPerson()
}
function showChatMessage(f, i) {
    var c = f.msg;
    if (!!c) {
        var d = f.fromname;
        var b = "";
        if (f.fromid == "0") {
            b = '<span class="chatperson disabled" data-uid="' + f.fromid + '">' + d + "</span>"
        } else {
            b = '<span class="chatperson" data-uid="' + f.fromid + '">' + d + "</span>"
        }
        c = c.replace(new RegExp(/%fromname%/g), b);
        var j = f.toname;
        var h = '<span class="chatperson" data-uid="' + f.toid + '">' + j + "</span>";
        c = c.replace(new RegExp(/%toname%/g), h);
        var a = '<li class="' + f.subtype + '">' + c + "</li>";
        $("#innercontent .chatlist").append(a);
        if (i) {
            addChatHistory(f);
            if (f.subtype == "say" || f.subtype == "tell" || f.subtype == "chat") {
                var g = c.replace(new RegExp(/%fromname%/g), d);
                g = g.replace(new RegExp(/%toname%/g), j);
                showMessage(g)
            }
            if (f.subtype == "music") {
                showNotification(c);
                if ($("#chatPage").length == 0) {
                    return
                }
            }
        }
        if (!!$("#innercontent").length) {
            $("#innercontent").mCustomScrollbar("scrollTo", "bottom")
        }
        var e = $("#chatPage");
        if (e.length == 0) {
            unreadMessage++;
            $(".messageNum").html(unreadMessage).show()
        }
    }
}
function showNotification(e) {
    if (!!e) {
        var a = $("#notification");
        if (a.children(":first").is(":animated")) {
            noticeList.push(e)
        }
        if (a.length == 0) {
            $("body").append('<div id="notification"><span></span></div>');
            a = $("#notification")
        }
        a.children(":first").html(e);
        a.show();
        var d, c, b;
        c = a.width();
        b = a.children(":first").width();
        a.children(":first").css("left", c);
        if (b > 0) {
            d = (c + b) * 1000 / 48;
            a.children(":first").animate({
                left: 0 - b
            },
            d,
            function () {
                if (noticeList.length == 0) {
                    $(this).parent().hide()
                } else {
                    var f = noticeList[0];
                    noticeList.shift();
                    showNotification(f)
                }
            })
        }
    }
}
function clickChatButton(g) {
    var f = $(g);
    $(".chattype li.active").removeClass("active");
    f.addClass("active");
    var d = f.data("type");
    if (d == "music") {
        $("#innercontent li").hide();
        $(".sendbox").hide();
        return
    } else {
        $(".sendbox").show()
    }
    $("#innercontent li").remove();
    if (!!window.localStorage && !!window.localStorage.getItem("chathistory") && window.localStorage.getItem("chathistory").length > 0) {
        var e = window.localStorage.getItem("chathistory");
        var a = JSON.parse(e);
        var c = [];
        for (var b = 0; b < a.length; b++) {
            showChatMessage(a[b], false)
        }
    }
    if (d == "all") {
        $("#innercontent li").show()
    } else {
        $("#innercontent li").hide();
        $("#innercontent").find("." + d).show()
    }
}
function addChatHistory(c) {
    if (!!window.localStorage) {
        if (!! !window.localStorage.getItem("chathistory")) {
            window.localStorage.setItem("chathistory", "")
        }
        var a = window.localStorage.getItem("chathistory");
        var b = a.length == 0 ? [] : JSON.parse(a);
        b.push(c);
        if (b.length > 40) {
            b.splice(0, 10)
        }
        window.localStorage.setItem("chathistory", JSON.stringify(b))
    }
}
function chatBack() {
    $("#chatPage").remove()
}
function storageFriend(d) {
    if (!!window.localStorage) {
        if (!! !window.localStorage.getItem("friends")) {
            window.localStorage.setItem("friends", "")
        }
        var e = window.localStorage.getItem("friends");
        var c = [];
        var a = true;
        if (e.length > 0) {
            c = JSON.parse(e);
            for (var b = 0; b < c.length; b++) {
                if (c[b].id == d.id) {
                    a = false
                }
            }
        }
        if (a && c.length < 20) {
            c.push(d);
            window.localStorage.setItem("friends", JSON.stringify(c))
        }
    }
}
function refreshFriend() {
    $("#friendbox").mCustomScrollbar("destroy");
    $(".frienditem").remove();
    if (!!window.localStorage && !!window.localStorage.getItem("friends") && window.localStorage.getItem("friends").length > 0) {
        var d = window.localStorage.getItem("friends");
        var c = JSON.parse(d);
        var b = [];
        for (var a = 0; a < c.length; a++) {
            b.push('<li class="frienditem" data-uid="', c[a].id, '" onclick="selectFriend(this);">', c[a].name, "</li>")
        }
        $("#friendlist").html("").append(b.join(""));
        $("#friendbox").mCustomScrollbar({
            axis: "y",
            mouseWheel: {
                enable: true
            },
            advanced: {
                updateOnContentResize: true
            }
        })
    }
}
function chooseFriend() {
    $("#emotebox").removeClass("active");
    var a = $("#friendbox .frienditem");
    if (a.length == 0) {
        $("#friendbox").removeClass("active")
    } else {
        if ($("#friendbox").hasClass("active")) {
            $("#friendbox").removeClass("active")
        } else {
            $("#friendbox").addClass("active")
        }
    }
}
function selectFriend(c) {
    var b = $(c).data("uid");
    var a = $(c).html();
    $(".sendbox .personbox").html(a).data("uid", b);
    $("#friendbox").removeClass("active")
}
function fightBack() {
    $("#fightPage").remove();
    isFighting = false;
    send("look");
    send("checkfight")
}
function showEscape(c) {
    if (c.result == "true") {
        if (!!c.team) {
            var b = $("#fightContent").find('ul[data-team="' + c.team + '"]');
            var a = b.find('li[data-uid="' + c.id + '"]');
            if (a.length > 0) {
                a.remove()
            } else {
                return
            }
        } else {
            fightBack()
        }
    }
}
function loadFightInfo(f) {
    isFighting = true;
    var o = $("#fightPage");
    if (o.length == 0) {
        var k = new Array;
        var n = new Array;
        for (var g = 0; g < f.startfight.length; g++) {
            if (f.startfight[g].team == f.startfight[0].team) {
                k.push(f.startfight[g])
            } else {
                n.push(f.startfight[g])
            }
        }
        var b = [];
        b.push('<div id="fightPage" class="page">');
        b.push('<div class="title" id="nav">');
        b.push('<span id="title">比试</span>');
        b.push('<a class="btn right icon-back" id="fightback" onclick="fightBack();"></a>');
        b.push("</div>");
        b.push('<div id="fightContent">');
        b.push('<ul class="otherside teamside" data-team="', n[0].team, '">');
        for (var e = 0; e < (n.length < 4 ? n.length : 4) ; e++) {
            var d = parseInt(n[e].effqixue, 10) / parseInt(n[e].maxqixue, 10) * 100;
            d = d == "NaN" ? 0 : Math.floor(d);
            var l = parseInt(n[e].qixue, 10) / parseInt(n[e].maxqixue, 10) * 100;
            l = l == "NaN" ? 0 : Math.floor(l);
            var a = parseInt(n[e].neili, 10) / parseInt(n[e].maxneili, 10) * 100;
            a = a == "NaN" ? 0 : Math.floor(a);
            a = a >= 100 ? 100 : a;
            b.push('<li data-uid="', n[e].id, '"  onclick="chooseEnemy(this);"><span class="membername">', n[e].name, "</span>");
            b.push('<span class="progressbox red" data-qixue="', n[e].qixue, '" data-effqixue="', n[e].effqixue, '"><span class="memberqixue">', n[e].qixue, '</span><span class="progress" style="width:', d, '%"></span>');
            b.push('<span class="progressshadow" style="width:', l, '%"></span><span class="progressbg"></span></span>');
            b.push('<span class="progressbox blue" data-neili="', n[e].maxneili, ' currentneili="', n[e].neili, '"><span class="progress" style="width:', a, '%"></span><span class="progressbg"></span></span>');
            b.push("</li>")
        }
        b.push("</ul>");
        b.push('<div class="fightmessage" id="fightmessage">');
        b.push('<ul id="fightmessageContent">');
        b.push("</ul>");
        b.push("</div>");
        b.push('<ul class="ourside teamside" data-team="', k[0].team, '">');
        for (e = 0; e < (k.length < 4 ? k.length : 4) ; e++) {
            b.push('<li data-uid="', k[e].id, '"><span class="membername">', k[e].name, "</span>");
            d = parseInt(k[e].effqixue, 10) / parseInt(k[e].maxqixue, 10) * 100;
            d = d == "NaN" ? 0 : Math.floor(d);
            l = parseInt(k[e].qixue, 10) / parseInt(k[e].maxqixue, 10) * 100;
            l = l = "NaN" ? 0 : Math.floor(l);
            a = parseInt(k[e].neili, 10) / parseInt(k[e].maxneili, 10) * 100;
            a = a == "NaN" ? 0 : Math.floor(a);
            a = a >= 100 ? 100 : a;
            b.push('<span class="progressbox red" data-qixue="', k[e].qixue, '" data-effqixue="', k[e].effqixue, '"><span class="memberqixue">', k[e].qixue, '</span><span class="progress" style="width:', d, '%"></span>');
            b.push('<span class="progressshadow" style="width:', l, '%"></span><span class="progressbg"></span></span>');
            b.push('<span class="progressbox blue" data-neili="', k[e].maxneili, ' currentneili="', k[e].neili, '"><span class="progress" style="width:', a, '%"></span><span class="progressbg"></span></span>');
            b.push("</li>")
        }
        b.push("</ul>");
        b.push('<ul class="fightbuttons"></ul>');
        b.push("</div>");
        b.push("</div>");
        $("body").append(b.join(""));
        $(".dialog").hide();
        $("#fightmessage").mCustomScrollbar({
            axis: "y",
            mouseWheel: {
                enable: true
            },
            advanced: {
                updateOnContentResize: true
            }
        });
        $("#fightPage").show();
        send("listpfmset")
    } else {
        for (var g = 0; g < f.startfight.length; g++) {
            var h = $("#fightContent").find('ul[data-team="' + f.startfight[g].team + '"]');
            var m = h.find('li[data-uid="' + f.startfight[g].id + '"]');
            if (h.find("li").length >= 4) {
                continue
            }
            if (m.length > 0) {
                console.log("该id已存在")
            } else {
                var c = [];
                var d = parseInt(f.startfight[g].effqixue, 10) / parseInt(f.startfight[g].maxqixue, 10) * 100;
                d = d == "NaN" ? 0 : Math.floor(d);
                var l = parseInt(f.startfight[g].qixue, 10) / parseInt(f.startfight[g].maxqixue, 10) * 100;
                l = l == "NaN" ? 0 : Math.floor(l);
                var a = parseInt(f.startfight[g].neili, 10) / parseInt(f.startfight[g].maxneili, 10) * 100;
                a = a == "NaN" ? 0 : Math.floor(a);
                a = a >= 100 ? 100 : a;
                if (h.attr("class") == "otherside teamside") {
                    c.push('<li data-uid="', f.startfight[g].id, '" onclick="chooseEnemy(this);">')
                } else {
                    c.push('<li data-uid="', f.startfight[g].id, '">')
                }
                c.push('<span class="membername">', f.startfight[g].name, "</span>");
                c.push('<span class="progressbox red" data-qixue="', f.startfight[g].qixue, '" data-effqixue="', f.startfight[g].effqixue, '"><span class="memberqixue">', f.startfight[g].qixue, '</span><span class="progress" style="width:', d, '%"></span>');
                c.push('<span class="progressshadow" style="width:', l, '%"></span><span class="progressbg"></span></span>');
                c.push('<span class="progressbox blue" data-neili="', f.startfight[g].maxneili, ' currentneili="', f.startfight[g].neili, '"><span class="progress" style="width:', a, '%"></span><span class="progressbg"></span></span>');
                c.push("</li>");
                h.append(c.join(""));
                continue
            }
        }
    }
}
function showJoinFight(e) {
    var c = [];
    var g = e.joinfight;
    var d = $("#fightContent").find('ul[data-team="' + g.team + '"]');
    var b = d.find('li[data-uid="' + g.id + '"]');
    if (b.length > 0) {
        return
    }
    if (d.find("li").length >= 4) {
        return
    }
    var f = parseInt(g.effqixue, 10) / parseInt(g.maxqixue, 10) * 100;
    f = f == "NaN" ? 0 : Math.floor(f);
    var a = parseInt(g.qixue, 10) / parseInt(g.maxqixue, 10) * 100;
    a = a == "NaN" ? 0 : Math.floor(a);
    var h = parseInt(g.neili, 10) / parseInt(g.maxneili, 10) * 100;
    h = h == "NaN" ? 0 : Math.floor(h);
    h = h >= 100 ? 100 : h;
    if (d.attr("class") == "otherside teamside") {
        c.push('<li data-uid="', g.id, '" onclick="chooseEnemy(this);">')
    } else {
        c.push('<li data-uid="', g.id, '">')
    }
    c.push('<span class="membername">', g.name, "</span>");
    c.push('<span class="progressbox red" data-qixue="', g.qixue, '" data-effqixue="', g.effqixue, '"><span class="memberqixue">', g.qixue, '</span><span class="progress" style="width:', f, '%"></span>');
    c.push('<span class="progressshadow" style="width:', a, '%"></span><span class="progressbg"></span></span>');
    c.push('<span class="progressbox blue" data-neili="', g.maxneili, ' currentneili="', g.neili, '"><span class="progress" style="width:', h, '%"></span><span class="progressbg"></span></span>');
    c.push("</li>");
    d.append(c.join(""))
}
function chooseEnemy(b) {
    var a = $(b).data("uid");
    $(".teamside li").removeAttr("style");
    $(b).attr("style", "border: 1px solid red");
    $('.fightbuttons li[data-direct^="perform"]').each(function () {
        $(this).data("id", a)
    })
}
function loadFightMessage(f) {
    if ($("#fightPage").length == 0) {
        send("checkfight");
        return
    }
    var h = f.fight;
    var a = h.msg;
    var g = a.split("|");
    var d = [];
    var b = f.fromname;
    var c = f.fromid;
    var l = f.toname;
    var k = f.toid;
    for (var e = 0; e < g.length; e++) {
        if (g[e].length > 3) {
            d.push("<li>", g[e], "</li>")
        }
    }
    var j = $("#fightmessageContent").find("li");
    if (j.length > 800) {
        for (var e = 0; e <= 100; e++) {
            j.eq(e).remove()
        }
    }
    $("#fightmessageContent").append(d.join(""));
    $("#fightmessage").mCustomScrollbar("scrollTo", "bottom");
    var c = h.fromid;
    addQixue(c, h.fromqixue, 1);
    addQixue(c, h.fromeffqixue, 2);
    $('li[data-uid="' + c + '"]').find(".memberqixue").html(h.fromqixue);
    addQixue(c, h.fromneili, 3);
    var k = h.toid;
    if (h.toqixue <= 0 && $('li[data-uid="' + k + '"]').attr("style") != "undefined") {
        $('li[data-uid="' + k + '"]').removeAttr("style");
        $('.fightbuttons li[data-direct^="perform"]').each(function () {
            $(this).removeAttr("data-id")
        })
    }
    addQixue(k, h.toqixue, 1);
    addQixue(k, h.toeffqixue, 2);
    $('li[data-uid="' + k + '"]').find(".memberqixue").html(h.toqixue);
    addQixue(k, h.toneili, 3)
}
function showFightData(b) {
    var a = "<li>" + b + "</li>";
    $("#fightmessageContent").append(a);
    $("#fightmessage").mCustomScrollbar("scrollTo", "bottom")
}
function addQixue(h, f, c) {
    var e = $('li[data-uid="' + h + '"]');
    if (c == 1) {
        var d = e.find(".red").data("qixue");
        var b = parseInt(f, 10) / parseInt(d, 10) * 100;
        b = b == "NaN" ? 0 : Math.floor(b);
        b = f <= 0 ? 0 : b;
        b = b >= 100 ? 100 : b;
        e.find(".red .progress").width(b + "%");
        var a = $('li[data-uid="' + h + '"]').find(".memberqixue").text();
        var g = f - parseInt(a, 10);
        g = !!g ? g : 0;
        if (g == 0) {
            return
        }
        g = g > 0 ? "+" + g : g;
        e.append('<span class="addRed show">' + g + "</span>")
    } else {
        if (c == 2) {
            var d = e.find(".red").data("effqixue");
            var b = parseInt(f, 10) / parseInt(d, 10) * 100;
            b = b == "NaN" ? 0 : Math.floor(b);
            b = f <= 0 ? 0 : b;
            e.find(".red .progressshadow").width(b + "%")
        } else {
            if (c == 3) {
                var d = e.find(".blue").data("neili");
                var b = parseInt(f, 10) / parseInt(d, 10) * 100;
                b = b == "NaN" ? 0 : Math.floor(b);
                b = b >= 100 ? 100 : b;
                b = f <= 0 ? 0 : b;
                e.find(".blue .progress").width(b + "%");
                var a = e.find(".blue").attr("currentneili");
                e.find(".blue").attr("currentneili", f);
                var g = f - parseInt(a, 10);
                g = !!g ? g : 0;
                if (g == 0) {
                    return
                }
                g = g > 0 ? "+" + g : g;
                e.append('<span class="addBlue showblue">' + g + "</span>")
            }
        }
    }
}
function showEndFightTip(c) {
    showFightData(c.msg);
    var a = c.subtype == "win" ? "胜利！" : "失败！";
    var b = [];
    b.push('<li class="fightresulttitle">战斗结束</li>');
    if (isKilling && c.subtype == "win") {
        isKilling = false
    }
    $("#fightmessageContent").append(b.join(""));
    $("#fightback").show();
    setTimeout(function () {
        $("#fightmessage").mCustomScrollbar("scrollTo", "bottom")
    },
    200)
}
function clickFightBtn(c) {
    var b = $(c).data("direct");
    if (!!b) {
        var a = $(c).data("npcid");
        send(b + " " + a)
    }
}
function decode(a) {
    var b = document.createElement("div");
    b.innerHTML = a;
    a = b.innerText || b.textContent;
    b = null;
    return a
}
function resize() {
    if ($(window).height() <= $(window).width()) {
        $("#page1").width($(window).height() * 0.563);
        $("#dialog").width($(window).height() * 0.563);
        $("#fightPage").width($(window).height() * 0.563)
    }
}
function intMax(b) {
    var a;
    a = parseInt($(b).val(), 10);
    if (isNaN(a)) {
        a = 0
    } else {
        if (a > $(b).attr("max")) {
            a = $(b).attr("max")
        }
    }
    $(b).val(a)
}
$(document).ready(function () {
    $("#back").click(function () {
        isFromItem = false;
        isFromNpc = false;
        isFromPlayer = false;
        isFromMe = false;
        var a = $(".content");
        if (a.length > 1) {
            a.last().remove()
        }
        if (a.length == 2) {
            hideBackBtn()
        }
        var c = titles.length - 1;
        titles.splice(c, 1);
        var b = titles[titles.length - 1];
        if (b == "状态") {
            $(".statuslist li.active").trigger("click")
        }
        $("#title").html(titles[titles.length - 1]);
        $(".content").last().show()
    });
    $("#nav-rightbtn").off("click").on("click",
    function () {
        showStage()
    });
    $("#messageinner").mCustomScrollbar({
        axis: "y",
        mouseWheel: {
            enable: true
        },
        advanced: {
            updateOnContentResize: true
        }
    });
    $("#nav-leftbtn").off("click").on("click",
    function () {
        send("quit");
        window.localStorage.setItem("actions", "")
    });
    $("body").on("click", ".chatperson",
    function () {
        $("#friendbox").removeClass("active");
        var c = $(this).data("uid");
        var a = $(this).html();
        var b = {
            id: c,
            name: a
        };
        if (c == "0") {
            return
        }
        $(".sendbox .personbox").html(a).data("uid", c);
        storageFriend(b);
        refreshFriend();
        $("#messageinner").mCustomScrollbar("scrollTo", "bottom")
    })
});