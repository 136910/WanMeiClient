﻿(function () {
    ToolManager = {};
    ToolManager.UI = {
        //打开界面层，隐藏顶部图标层
        OpenTitle: function (title) {
            $(".tubiaoul").hide();
            $("#title").html(title);
            $(".title").show();
        },
        CloseTitle: function () {
            $(".tubiaoul").show();
            $("#title").html("");
            $(".title").hide();
        }
    };
    ToolManager.Layer = {
        //提示，2秒消失
        ErrorTimeMsg: function (msg, time) {
            layer.open({
                content: msg,
                skin: 'msg',
                type: 1,
                time: time //time秒后自动关闭
            });
        },
        //转圈
        Loding: function (msg) {
            var index = layer.open({
                type: 1,
                content: msg
            });
            return index;
        },
        //关闭层
        Close: function (o) {
            layer.close(o);
        },
        //信息框
        Message: function (msg, fc) {
            layer.open({
                content: msg,
                btn: '朕知道了',
                yes: fc
            });
        },
        //自定义提示层
        SelfSet: function (content, yesname, noname, yescall) {
            layer.open({
                content: content,
                btn: [yesname, noname],
                yes: yescall
            })
        },
        TilleUI: function (title, msg) {
            //自定义标题风格
            layer.open({
                title: [
                  title,
                  'background-color: #FF5722; color:#fff;'
                ]
              , content: msg
            });
        }
    };


    ToolManager.Common = {
        //获取URL参数
        UrlParms: function (key) {
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
            var r = window.location.search.substr(1).match(reg); //匹配目标参数
            if (r != null) return unescape(r[2]); return null; //返回参数值
        },
        AddCache: function (un) {
            var result = false;
            webExpress.utility.ajax.request("/Login/AddCache", { "un": un }, function (data) {
                if (data.IsSuccess) {
                    result = true;
                }
            });
            return result;
        },
        //获取随机token
        Token: function () {
            return Math.random().toString(36).substr(2);
        },
        //向主界面信息框里添加信息
        AddMsg: function (msg) {
            var arr = [];
            arr.push('<li>');
            arr.push(msg);
            arr.push('</li>');
            $("#messagelist").append(arr.join(''));
            $("#messageinner").mCustomScrollbar("scrollTo", "bottom")
        },
        //获取当前角色等级对应的下一级所需的经验值
        NextJy: function (level) {
            return 60 + (level - 1) * (level - 1) * (level - 1);
        },
        IsPInt: function (str) {

        },
        JumpChose: function (msg) {
            ToolManager.Layer.Message(msg, function () {
                setTimeout(function () {
                    window.location.href = "/Login/ChoseRole";
                }, 1)
            });
        },
        ///从几个颜色随机获取
        GetRandomColor: function () {
            var rgb = ['#FFB800', '#FF00FF', '#8B1A1A', '#FF5722', '#1E9FFF', '#00cc00', '#99ff00', '#ff33cc', '#00ffff', '#cc3333', '#EE82EE', '#F08080', '#B8860B']
            return rgb[Math.round(Math.random() * 4)];
        },
        //职称对应的颜色
        GetColorForLevel: function (level) {
            var res = "";
            if (level >= 1 && level < 10)
                res = "#EEDC82";
            if (level >= 10 && level < 20)
                res = "#EED2EE";
            if (level >= 20 && level < 30)
                res = "#EEC591";
            if (level >= 30 && level < 40)
                res = "#EEAD0E";
            if (level >= 40 && level < 50)
                res = "#EE7AE9";
            if (level >= 50 && level < 60)
                res = "#EE5C42";
            if (level >= 60 && level < 70)
                res = "#EE30A7";
            if (level >= 70 && level < 80)
                res = "#EE0000";
            if (level >= 80 && level < 90)
                res = "#EE00EE";
            if (level >= 90 && level < 101)
                res = "#68228B";
            return res;

        }

    };
    //正整数
    function isPInt(str) {
        var g = /^[1-9]*[1-9][0-9]*$/;
        return g.test(str);
    }
    //整数
    function isInt(str) {
        var g = /^-?\d+$/;
        return g.test(str);
    }
    _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    ToolManager.Base64 = {
        Encode: function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = this._utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
            }
            return output;
        },
        Decode: function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {
                enc1 = _keyStr.indexOf(input.charAt(i++));
                enc2 = _keyStr.indexOf(input.charAt(i++));
                enc3 = _keyStr.indexOf(input.charAt(i++));
                enc4 = _keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = this._utf8_decode(output);
            return output;
        },
        _utf8_encode: function (string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }
            return utftext;
        },
        _utf8_decode: function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            while (i < utftext.length) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                } else if ((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i + 1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = utftext.charCodeAt(i + 1);
                    c3 = utftext.charCodeAt(i + 2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    };
})();