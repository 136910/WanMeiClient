﻿(function () {
    SkillModel.Bussiness = {
        //给角色添加被动技能特效
        SetPassiveCss: function (skills) {
            for (var i = 0; i < skills.length; i++) {
                if (skills[i].SkillType == 3) {
                    var css = this.GetPassiveCss(skills[i].SkillName);
                    if (css != "")
                        $("#zhujiao").addClass(css);
                }
            }
        },
        //根据被动技能名称，获取CSS名称
        GetPassiveCss: function (skillName) {
            var res = "";
            switch (skillName) {
                case "强击光环":
                    res = "zhujiaoBeidong_bingdun";
                    break;
            }
            return res;
        },
        //根据ID获取技能模型
        GetSkillModel: function (id) {
            var res = null;
            for (var i = 0; i < UserBase.StatusSkills.length; i++) {
                if (id == UserBase.StatusSkills[i].Id) {
                    res = UserBase.StatusSkills[i];
                    break;
                }
            }
            return res;
        }
    };


})()
//怪技能模型，只从角色脚下出来
function MonsterSkill(monsterModel, type,roleModel) {//type=1,普通攻击，2技能攻击
    this.Path = type == 1 ? monsterModel.ApImg : monsterModel.SkillImg;
    this.JinengObj = undefined;
    this.CreateSkill = function () {
        var guid = Guid();
        roleModel.JqueryObj.after('<div id="jineng_texiao_' + guid + '" style="z-index: 9999;margin-top: -' + roleModel.JqueryObj.height() + ';display:;position: absolute;left: 41%;top: 11%;"><img height="70";width="70"  src="' + this.Path + '" ><div>');

        this.JinengObj = $("#jineng_texiao_" + guid);
        
    }
    this.MoveTarget = function () {
        this.JinengObj.show(500)
        this.JinengObj.hide(500, function () {
            roleModel.Shake();
        })
        Distory(this.JinengObj)
    }
    return this;
}
//角色一次性治疗技能动画模型
function RoleCureSkill(skillModel, roleModel) {
    this.Path = skillModel.SkillImg
    this.JinengObj = undefined;
    this.CreateSkill = function () {
        var guid = Guid();
        roleModel.JqueryObj.after('<div id="jineng_texiao_' + guid + '" style="z-index: 9999;margin-top: -' + roleModel.JqueryObj.height() + ';display:;position: absolute;left: 41%;top: 11%;"><img height="70";width="70"  src="' + this.Path + '" ><div>');
        this.JinengObj = $("#jineng_texiao_" + guid);

    }
    this.MoveTarget = function () {
        this.JinengObj.show(500)
        this.JinengObj.hide(500, function () {
        })
        Distory(this.JinengObj)
    }
    return this;
}


//技能模型
function Skill(skillModel, speed, enemyModel) {
    this.Path = skillModel.SkillImg
    this.Speed = speed;
    this.Id = skillModel.Id;
    this.X = $("#zhujiao").offset().left;
    this.Y = $("#zhujiao").offset().top,
    this.JinengObj = undefined;

    //有路径移动创建技能
    this.CreateModel = function () {
        var guid = Guid();
        $("#role_progess").before('<img id="jineng_texiao_' + skillModel.Id + '_' + guid + '" src="' + this.Path + '" style="z-index: 9999;position: absolute;top: -87px;left: 113px;">');
        this.JinengObj = $("#jineng_texiao_" + skillModel.Id + "_" + guid);

    }
    //脚下创建技能，无路径
    this.CreateModelTarget = function (i) {
        //计算一下一共几只怪，如果是1只，按1走中间。
        var mCount = 0;
        $(".monster").each(function () {
            mCount++;
        })
        var guid = Guid();
        var left = "14%";
        if (i == 1 || mCount == 1)
            left = "41%";
        else if (i == 2)
            left = "69%";
        enemyModel.JqueryObj.after('<div id="jineng_texiao_' + skillModel.Id + '_' + guid + '" style="z-index: 9999;margin-top: -' + enemyModel.JqueryObj.height() + ';display:;position: absolute;left: ' + left + ';top: 11%;"><img height="70";width="70"  src="' + this.Path + '" ><div>');
        this.JinengObj = $("#jineng_texiao_" + skillModel.Id + "_" + guid);

    }
    //移动特效1，移动过程不变大
    this.Move = function (i) {
        if (i != 1)
            this.JinengObj.animate({ top: enemyModel.Y - this.Y + 30, left: enemyModel.X - this.X + 50 }, 500, function () {
                enemyModel.Shake();
            });
        else
            this.JinengObj.animate({ top: enemyModel.Y - this.Y + 30 }, 500, function () {
                enemyModel.Shake();
            });
        Distory(this.JinengObj);
    }
    //移动特效2，直线过去，左右冲击
    this.MoveBig = function () {
        this.JinengObj.animate({ top: -this.Y + 60 }, 500);
        for (var i = 0; i < 3; i++) {
            this.JinengObj.animate({ left: -$("#monsterScreen").width() / 2 }, 100);
            this.JinengObj.animate({ left: $("#monsterScreen").width() }, 100);
        }
        this.JinengObj.animate({
            left: $("#monsterScreen").width()

        }, 500, function () {
            //群体技能，所有怪抖动，正式项目中，抖动后，会判读血条，并且动态处理后续逻辑
            $(".monster").each(function () {
                if ($(this).parent().css("visibility") != "hidden")
                    RaidModel.Model.CreateMonsterModel($(this).attr("id")).Shake();
            })
        })
        Distory(this.JinengObj)
    }
    //无路径移动特效
    this.MoveTarget = function () {
        this.JinengObj.show(500)
        this.JinengObj.hide(500, function () {
            enemyModel.Shake();
        })
        Distory(this.JinengObj)
    }
    return this;
}

function Distory(obj) {
    obj.hide(20, function () {
        obj.remove();
    })
}

function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}
function Guid() {
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}
