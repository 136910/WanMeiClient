﻿(function () {
    RaidModel.RaidList = {
        //副本列表持久化
    };
    //当前战斗副本的怪列表持久化。
    MonsterModel.CurrentFight = {
    };
    RaidModel.Model = {
        CreateHeroModel: function () {
            var obj = {};
            obj.X = $('#zhujiao').offset().left;
            obj.Y = $('#zhujiao').offset().top;
            obj.JqueryObj = $('#zhujiao');
            obj.Shake = function () {
                box_left = 0;
                for (var i = 1; 3 >= i; i++) {
                    obj.JqueryObj.animate({ left: box_left - (20 - 5 * i) }, 30);
                    obj.JqueryObj.animate({ left: box_left + (20 - 5 * i) }, 30);
                }
            }
            return obj;
        },
        CreateMonsterModel: function (id, soft) {
            var obj = {};
            obj.X = $("#" + id).offset().left;
            obj.Y = $("#" + id).offset().top;
            obj.Soft = soft;
            obj.JqueryObj = $("#" + id);
            obj.Distory = function () {
                obj.JqueryObj.hide(20, function () {
                    obj.JqueryObj.remove();
                })
            }
            obj.Shake = function () {
                box_left = 0;
                for (var i = 1; 3 >= i; i++) {
                    obj.JqueryObj.animate({ left: box_left - (20 - 5 * i) }, 30);
                    obj.JqueryObj.animate({ left: box_left + (20 - 5 * i) }, 30);
                }
            }
            return obj;
        }
    };

    //关于副本的业务
    RaidModel.Bussiness = {
        GetRaidById: function (raidId) {
            var res = null;
            for (var i = 0; i < RaidModel.RaidList.length; i++) {
                if (raidId == RaidModel.RaidList[i].Id) {
                    res = RaidModel.RaidList[i];
                    break;
                }
            }
            return res;
        },
        //创建绑定副本数据HTML
        CreateRaidHtml: function (data) {
            var htmlArr = [];
            htmlArr.push('<button style="background-color:#a53f3f" id="existRaid" class="layui-btn layui-btn-warm layui-btn-radius">返回</button>');
            htmlArr.push(' <input type="hidden" id="fightId" value="' + data.Data.FightId + '"/>');
            //----怪初始化开始
            htmlArr.push('<div style="height: 15%" id="monsterScreen">');
            for (var i = 0; i < data.Data.Monsters.length; i++) {
                if (data.Data.Monsters.length == 1)
                    htmlArr.push('<div style="width: 33%; height: 100px; margin:auto">');
                else
                    htmlArr.push('<div style="width: 33%; height: 100px; float: left">');
                //--血条开始
                htmlArr.push('<div class="layui-progress" style="margin: auto; width: 60px;">');
                htmlArr.push('<div class="layui-progress-bar layui-bg-red" hv="' + data.Data.Monsters[i].HeathValue + '" style="width: 100%;" id="monsterProgress_h_' + data.Data.Monsters[i].Id + '"></div>');
                htmlArr.push('</div>');
                ////--魔力，有技能显示
                //if (data.Data.Monsters[i].IsHasSkill == 1) {
                //    htmlArr.push('<div class="layui-progress" style="margin: auto; width: 60px;">');
                //    htmlArr.push('<div class="layui-progress-bar layui-bg-blue" mv="' + data.Data.Monsters[i].MagicValue + '" style="width: 100%;" id="monsterProgress_m_' + data.Data.Monsters[i].Id + '"></div>');
                //    htmlArr.push('</div>');
                //}
                //--血条结束
                htmlArr.push('<img skillCd="0" soft="' + i + '" class="monster" src="' + data.Data.Monsters[i].ImgPath + '1" id="monster_' + data.Data.Monsters[i].Id + '" />');
                //debuffer开始，这里显示角色给使用的持续技能
                htmlArr.push('<div id="monsterBuff_' + data.Data.Monsters[i].Id + '">');
                //htmlArr.push('<span><img style="height: 20px; width: 20px;" src="http://192.168.39.75:8028/GameProImages/jineng/dusu.png" /></span>');
                htmlArr.push('</div>');
                htmlArr.push(' </div>');
            }
            htmlArr.push(' </div>');
            //----怪初始化结束
            htmlArr.push('<div id="screen" style="width: 100%; background-color: #cccccc; position: fixed; bottom: 22%;">   </div>');
            //----角色初始化开始
            htmlArr.push('<div id="roleScceen">');
            //--角色相关信息开始
            htmlArr.push(' <div style="margin: auto;" id="role_progess">');
            htmlArr.push('<div class="layui-progress" style="margin: auto; width: 70px;">');
            htmlArr.push('<div class="layui-progress-bar layui-bg-red" style="width: 100%;" hv="' + data.Data.CurrentRoleInfo.HealthPoint + '" id="roleProgress_h"></div>');
            htmlArr.push(' </div>');
            htmlArr.push('<div class="layui-progress" style="margin: auto; width: 70px;">');
            htmlArr.push('<div class="layui-progress-bar layui-bg-blue" mv="' + data.Data.CurrentRoleInfo.MagicPower + '" style="width: 100%;" id="roleProgress_m"></div>');
            htmlArr.push(' </div>');
            htmlArr.push('<img src="http://192.168.39.75:8028/GameProImages/zhujiao/zhujiao1.png1" id="zhujiao" class="" />');//class是被动防御类技能的效果，回头要单独去做
            htmlArr.push(' </div>');
            //--角色相关信息结束
            //--角色BUFF开始
            htmlArr.push('<div id="buff">');
            for (var j = 0; j < data.Data.RoleSkills.length; j++) {
                if (data.Data.RoleSkills[j].SkillType == 3) {
                    htmlArr.push('<span><img style="height: 20px; width: 20px;" src="' + data.Data.RoleSkills[j].SkillIcon + '" /></span>');
                }
            }
            htmlArr.push(' </div>');
            htmlArr.push(' </div>');
            //--角色BUFF结束
            //--技能开始
            htmlArr.push(' <div id="skillScceen">');
            htmlArr.push('<ul id="roleSkills">');
            for (var z = 0; z < data.Data.RoleSkills.length ; z++) {
                if (data.Data.RoleSkills[z].SkillType != 3) {
                    htmlArr.push(' <li>');
                    htmlArr.push(' <span>');
                    htmlArr.push('  <img id="skill_' + data.Data.RoleSkills[z].Id + '" SkillId="' + data.Data.RoleSkills[z].Id + '" class="jineng" src="' + data.Data.RoleSkills[z].SkillIcon + '" />');
                    htmlArr.push('</span>');
                    htmlArr.push('</li>');
                }
            }
            htmlArr.push('</ul>');
            htmlArr.push(' </div>');
            //--技能结束
            htmlArr.push(' </div>');
            //----角色初始化结束
            return htmlArr.join('');
        },
        ShowRaidList: function () {
            var htmlArr = [];
            htmlArr.push('<button style="background-color:#a53f3f" id="closeRaidList" class="layui-btn layui-btn-warm layui-btn-radius">返回</button>');
            htmlArr.push('<div class="demo"><div class="container"><div class="row"><div class="col-md-12"><div class="main-timeline">');
            for (var i = 0; i < RaidModel.RaidList.length; i++) {

                htmlArr.push('<div class="timeline">');
                htmlArr.push('<div class="timeline-content">');
                htmlArr.push('<span class="date">');
                htmlArr.push('<span style="color:#FFD700" class="day">冒险等级</span>');
                htmlArr.push('<span style="color:#FFD700" class="month">' + RaidModel.RaidList[i].NeedLevel + '</span>');
                htmlArr.push('<span style="color:#FFD700" class="year">级</span>');
                htmlArr.push('</span>');
                htmlArr.push('<h2 class="title" style="background-color:' + ToolManager.Common.GetRandomColor() + '">' + RaidModel.RaidList[i].RaidName + '</h2>');
                htmlArr.push('<p class="description">' + RaidModel.RaidList[i].Introduce + '</p>');
                htmlArr.push('<p> <button name="fight" raidId="' + RaidModel.RaidList[i].Id + '" class="layui-btn layui-btn-normal layui-btn-radius">前往冒险</button></p>');
                htmlArr.push('</div></div>');
            }
            htmlArr.push(' </div></div></div> </div></div>');
            return htmlArr.join('');
        },
    };
})()