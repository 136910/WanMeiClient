﻿(function () {
    FloatTextClass = new FloatText();
    //战斗文字浮动类
    function FloatText() {
        //需要浮动战斗文字的对象注册
        var _self = this;
        function _init() {
            _self.InitMonsterObj = InitMonsterObj;
            _self.InitRoleObj = InitRoleObj;
            _self.FloatNum = FloatNum;
            _self.InitDotMonsterObj = InitDotMonsterObj;
        }
        //DOT技能怪注册
        function InitDotMonsterObj(id, isBj) {
            if (isBj == 1) {
                $("#monster_" + id).levelup({
                    'start': 0,
                    'incrementer': { 'class': 'DotfloatAddBaoJiFont' },
                    'decrementer': { 'class': 'DotfloatJianBaoJiFont' },

                });
            }
            else {
                $("#monster_" + id).levelup({
                    'start': 0,
                    'incrementer': { 'class': 'DotfloatAddFont' },
                    'decrementer': { 'class': 'DotfloatJianFont' },

                });
            }
        }

        //一次性技能怪注册
        function InitMonsterObj(isBj) {//目前是一个暴击，都暴击
            var args = GetALLObj();
            $.each(args, function (index, obj) {
                if (isBj == 1) {
                    obj.levelup({
                        'start': 0,
                        'incrementer': { 'class': 'floatAddBaoJiFont' },
                        'decrementer': { 'class': 'floatJianBaoJiFont' },

                    });
                } else {
                    obj.levelup({
                        'start': 0,
                        'incrementer': { 'class': 'floatAddFont' },
                        'decrementer': { 'class': 'floatJianFont' },

                    });
                }

            })
        };
        //一次性技能角色注册
        function InitRoleObj(isBj) {
            if (isBj == 1) {
                $("#zhujiao").levelup({
                    'start': 0,
                    'incrementer': { 'class': 'floatAddBaoJiFont' },
                    'decrementer': { 'class': 'floatJianBaoJiFont' },

                });
            } else {
                $("#zhujiao").levelup({
                    'start': 0,
                    'incrementer': { 'class': 'floatAddFont' },
                    'decrementer': { 'class': 'floatJianFont' },

                });
            }
        }
        //浮动文字
        function FloatNum(obj, num, type) {

            if (type == 1)//加
                obj.levelup('increment', num);
            else
                obj.levelup('decrement', num);
        };
        //获取当前所有怪和角色的对象
        function GetALLObj() {
            var objArr = [];
            $(".monster").each(function () {
                objArr.push($(this));
            })
            return objArr;
        }
        _init();
    };


})()