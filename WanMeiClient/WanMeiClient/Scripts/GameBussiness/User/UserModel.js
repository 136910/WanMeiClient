﻿//用户注册模型
//function RegisterModel(UserName, PassWord, RePassWord) {
//    this.UserName = UserName;
//    this.PassWord = PassWord;
//    this.RePassWord = RePassWord;
//    return this;
//}
//如非必要的需要protype原型链继承，属性继承，就不要用上述写法，最后new对象，性能和维护起来，都可能出现问题。
(function () {
    GameModel.User = {
        CreateRegisterModel: function (UserName, PassWord, RePassWord) {
            var o = {};
            o.UserName = UserName;
            o.PassWord = PassWord;
            o.RePassWord = RePassWord;
            return o;
        },
        CreateLoginModel: function (UserName, PassWord) {
            var o = {};
            o.UserName = UserName;
            o.PassWord = PassWord;
            return o;
        }
    };
    //角色持久化基本信息模型(不含装备)废弃
    UserBase.HeroStatus = {

    };
    //角色持久化基本信息模型(含装备)
    UserBase.Status = {

    };
    //角色持久链接模型
    UserBase.SignalrInfo = {
            
    }
    //角色持久化装备信息
    UserBase.Equipments = {

    };
    //角色背包信息持久化
    UserBase.BackPackInfo = {
    };
    //角色技能信息持久化
    UserBase.StatusSkills = {
    };
    //一些公用的数据加载方法
    UserBase.Common = {
        //SyncLoadUserEquipment: function () {
        //    var heroId = ToolManager.Common.UrlParms("heroId");
        //    if (heroId == "" || heroId == undefined) {
        //        ToolManager.Common.JumpChose("请不要篡改参数");
        //    }
        //    webExpress.utility.ajax.request("/User/LoadCurrentEquipments?token=" + ToolManager.Common.Token(), { "heroId": heroId }, function (data) {
        //        if (data.IsSuccess) {
        //            UserBase.Equipments = data.Data;
        //            //更新全局角色状态,传的角色基本模型是不含装备的，计算完赋值给韩装备的基本模型
        //            var _HealthPoint = UserBase.DynamicFunc.CallHealthPoint(UserBase.HeroStatus, UserBase.Equipments);
        //            var _MagicPower = UserBase.DynamicFunc.CallMagicPower(UserBase.HeroStatus, UserBase.Equipments);
        //            var _Armor = UserBase.DynamicFunc.CallArmor(UserBase.HeroStatus, UserBase.Equipments);
        //            var _CriticalValue = UserBase.DynamicFunc.CallCritical(UserBase.HeroStatus, UserBase.Equipments);
        //            var _ApValue = UserBase.DynamicFunc.CallAp(UserBase.HeroStatus, UserBase.Equipments);
        //            var _Power = UserBase.DynamicFunc.CallPower(UserBase.HeroStatus, UserBase.Equipments);
        //            UserBase.Status.HealthPoint = _HealthPoint;
        //            UserBase.Status.MagicPower = _MagicPower;
        //            UserBase.Status.Armor = _Armor;
        //            UserBase.Status.CriticalValue = _CriticalValue;
        //            UserBase.Status.Power = _Power;
        //            UserBase.Status.ApValue = _ApValue;
                   
        //        } else {
        //            ToolManager.Common.JumpChose("数据加载失败,请从新进入");

        //        }
        //    });
        //},
        SyncLoadUserInfoModelAndEqs: function () {
            var heroId = ToolManager.Common.UrlParms("heroId");
            if (heroId == "" || heroId == undefined) {
                ToolManager.Common.JumpChose("请不要篡改参数");
            }
            webExpress.utility.ajax.request("/User/SyncLoadUserInfoModelAndEqs?token=" + ToolManager.Common.Token(), { "heroId": heroId }, function (data) {
                if (data.IsSuccess) {
                    UserBase.Status = data.Data.UserRoleModel;
                    UserBase.Equipments = data.Data.Equipments;

                } else {
                    ToolManager.Common.JumpChose("数据加载失败,请从新进入");

                }
            });
        },
        SyncLoadUserBackPack: function () {
            var heroId = ToolManager.Common.UrlParms("heroId");
            if (heroId == "" || heroId == undefined) {
                ToolManager.Common.JumpChose("请不要篡改参数");
            }
            webExpress.utility.ajax.request("/User/SyncLoadUserBackPack?token=" + ToolManager.Common.Token(), { "heroId": heroId }, function (data) {
                if (data.IsSuccess) {
                    UserBase.BackPackInfo = data.Data;
                    
                } else {
                    ToolManager.Common.JumpChose("数据加载失败,请从新进入");

                }
            });
        },
        SyncLoadSkillList: function () {
            var heroId = ToolManager.Common.UrlParms("heroId");
            if (heroId == "" || heroId == undefined) {
                ToolManager.Common.JumpChose("请不要篡改参数");
            }
            webExpress.utility.ajax.request("/Skill/GetStatusSkillList?token=" + ToolManager.Common.Token(), { "heroId": heroId }, function (data) {
                if (data.IsSuccess) {
                    UserBase.StatusSkills = data.Data;

                } else {
                    ToolManager.Common.JumpChose("数据加载失败,请从新进入");

                }
            });
        }
    };
    
    //角色持久化当前技能

    //角色AP，血，蓝，暴击，护甲，战斗力，动态计算类(角色基本+装备)
    UserBase.DynamicFunc = {
        CallAp: function (us, ue) {
            return this.GetSumApFromUe(ue, "ap") + us.ApValue;
        },
        CallHealthPoint: function (us, ue) {
            return this.GetSumApFromUe(ue, "xue") + us.HealthPoint;
        },
        CallMagicPower: function (us, ue) {
            return this.GetSumApFromUe(ue, "lan") + us.MagicPower;
        },
        CallArmor: function (us, ue) {
            return this.GetSumApFromUe(ue, "hujia") + us.Armor;
        },
        CallCritical: function (us, ue) {
            return this.GetSumApFromUe(ue, "baoji") + us.CriticalValue;
        },
        CallPower: function (us, ue) {
            return this.GetSumApFromUe(ue, "pow") + us.Power;
        },

        GetSumApFromUe: function (ue, type) {
            var res = 0;
            switch (type) {
                case "ap":
                    for (var i = 0; i < ue.length; i++) {
                        res += ue[i].ApValue;
                    }
                    break;
                case "lan":
                    for (var i = 0; i < ue.length; i++) {
                        res += ue[i].InitMagicPower;
                    }
                    break;
                case "xue":

                    for (var i = 0; i < ue.length; i++) {
                        res += ue[i].InitHealthPoint;
                    }
                    break;
                case "hujia":
                    for (var i = 0; i < ue.length; i++) {
                        res += ue[i].InitArmor;
                    }
                    break;
                case "baoji":
                    for (var i = 0; i < ue.length; i++) {
                        res += ue[i].InitCriticalValue;
                    }
                    break;
                case "pow":
                    for (var i = 0; i < ue.length; i++) {
                        res += ue[i].Power;
                    }
                    break;
            }

            return res;
        }
    };
})();