﻿(function () {
    PropModel.Intance = {
        //道具的面板显示层封装
        //bpobj是背包里道具的模型
        Show: function (protype, proid) {
            var bpObj = this.GetPropObj(protype, proid);
            if (bpObj == null) {
                ToolManager.Layer.ErrorTimeMsg("数据错误!", 2);
                return false;
            }
            
            var objArr = [];
            objArr.push('<ul style="list-style: outside none none; color: white;">');
            switch (bpObj.PropType) {
                case 1://装备
                   
                    objArr.push(' <li><font color="' + this.GetColorForQuality(bpObj.Quality) + '">' + bpObj.EquipmentName + '(' + this.GetStrByPlace(bpObj.Place) + ')</font></li>');
                    objArr.push('<li><font color="' + this.GetColorForQuality(bpObj.Quality) + '">品质:' + this.GetQualityTitle(bpObj.Quality) + '</font></li>');
                    objArr.push(' <li><font color="#ffff33">强化+' + bpObj.EquipmentLevel + '</font></li>');
                    objArr.push(' <li><font color="' + (window.parent.UserBase.Status.GameLevel >= bpObj.NeedGameLevel ? "white" : "red") + '">需要等级:' + bpObj.NeedGameLevel + '</font></li>');
                    objArr.push('<li>职业要求:' + this.GetNeedRole(bpObj.NeedRole) + '</li>');
                    if (bpObj.ApValue > 0) {
                        objArr.push('<li><font color="green">攻击+' + bpObj.ApValue + '</font></li>');
                    }
                    if (bpObj.InitHealthPoint > 0) {
                        objArr.push('<li><font color="green">生命+' + bpObj.InitHealthPoint + '</font></li>');
                    }
                    if (bpObj.InitMagicPower > 0) {
                        objArr.push('<li><font color="green">魔力+' + bpObj.InitMagicPower + '</font></li>');
                    }
                    if (bpObj.InitArmor > 0) {
                        objArr.push('<li><font color="green">护甲+' + bpObj.InitArmor + '</font></li>');
                    }
                    if (bpObj.InitCriticalValue > 0) {
                        objArr.push('<li><font color="green">暴击+' + bpObj.InitCriticalValue + '</font></li>');
                    }
                    objArr.push('<li><font color="green">战斗力+' + bpObj.Power + '</font></li>');
                    objArr.push('<li>出售价格:<font color="#ffff00">' + bpObj.SalePrice + '</font></li>');
                   
                    objArr.push('<li><font color="gray">' + bpObj.Introduce + '</font></li>');
                    objArr.push('<li> <button class="layui-btn layui-btn-sm layui-btn-normal" name="TakeOn" BackId="' + bpObj.Id + '" Place="' + bpObj.Place + '">装备</button></li>');
                    break;
                case 2:
                    objArr.push('<li><font>' + bpObj.MaterialName + '</font></li>');
                    objArr.push('<li><font color="gray">' + bpObj.Introduce + '</font></li>');
                    break;
                case 3:
                    objArr.push(' <li><font>' + bpObj.GemstoneName + '</font></li>');
                    objArr.push('<li><font color="#ff6600">' + bpObj.Introduce + '</font></li>');
                    break;
            }
            objArr.push('</ul>');
            ToolManager.Layer.Message(objArr.join(''));
            $(".layui-m-layerchild").css("background-color", "#171717");
        },
        GetPropObj:function(protype, proid){
            var res = null;
            var list;
            switch (protype) {
                case "1":
                    list =  window.parent.UserBase.BackPackInfo.Equipments;                   
                    break;
                case "2":
                    list =  window.parent.UserBase.BackPackInfo.Materials;
                    break;
                case "3":
                    list =  window.parent.UserBase.BackPackInfo.Gemstones;   
                    break;
            }
            for (var i = 0; i < list.length; i++) {
                if(proid==list[i].PropId){
                    res = list[i];
                    break;
                }
            }
            return res;
        },
        //根据装备品质，给出颜色1白色，2绿色，3蓝色，4紫色，5橙色，6粉色
        GetColorForQuality: function (quality) {
            var res = ""
            switch (quality) {
                case 1:
                    res = "white";
                    break;
                case 2:
                    res = "green";
                    break;
                case 3:
                    res = "blue";
                    break;
                case 4:
                    res = "#6600cc";
                    break;
                case 5:
                    res = "#ff6600";
                    break;
                case 6:
                    res = "#ff00cc";
                    break;
            }
            return res;
        },
        //获取装备职业限制字符串
        GetNeedRole: function (n) {
            var res = "";
            switch (n) {
                case 0:
                    res = "不限";
                    break;
                case 1:
                    res = "剑士";
                    break;
                case 2:
                    res = "魔法师";
                    break;
            }
            return res;
        },
        GetQualityTitle: function (quality) {
            var res = ""
            switch (quality) {
                case 1:
                    res = "普通";
                    break;
                case 2:
                    res = "优质";
                    break;
                case 3:
                    res = "精良";
                    break;
                case 4:
                    res = "极品";
                    break;
                case 5:
                    res = "传说";
                    break;
                case 6:
                    res = "神器";
                    break;
            }
            return res;
        },
        GetStrByPlace: function (p) {////装备所属位置1头，2衣服，3武器，4护腕，5鞋，6戒指,7项链
            var res = "";
            switch (p) {
                case 1:
                    res = "头部";
                    break;
                case 2:
                    res = "衣服";
                    break;
                case 3:
                    res = "武器";
                    break;
                case 4:
                    res = "护腕";
                    break;
                case 5:
                    res = "鞋";
                    break;
                case 6:
                    res = "戒指";
                    break;
                case 7:
                    res = "项链";
                    break;
            }
            return res;
        }
    };
})();