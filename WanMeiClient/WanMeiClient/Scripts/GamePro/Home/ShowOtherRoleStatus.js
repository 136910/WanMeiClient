﻿(function () {
    ShowOtherRoleStatus = {};
    var enqArrs = [];
    ShowOtherRoleStatus.Instance = {
        Init: function () {
            this.GetData();
            //this.BindStatus();
            //this.BindEnquiments();           
            $(document).on('click', '.enquiment_ul li img', this.ShowEnq);
            $(document).on('click', 'button[name="back_last"]', this.BackLast);
        },
        GetData: function () {
            var heroId = ToolManager.Common.UrlParms("heroId");
            var un = ToolManager.Common.UrlParms("un");
            ToolManager.Layer.Loding("读取中.....");
            webExpress.utility.ajax.request("/User/GetTargetUserInfo?token=" + ToolManager.Common.Token(), { "heroId": heroId, "un": un }, function (data) {
                layer.closeAll('loading');
                if (data.IsSuccess) {
                    ShowOtherRoleStatus.Instance.BindStatus(data.Data.UserRoleModel);
                    enqArrs = data.Data.Equipments;
                    ShowOtherRoleStatus.Instance.BindEnquiments();
                } else {
                    ToolManager.Layer.ErrorTimeMsg("网络异常", 2);
                }
            });
        },
        BindStatus: function (data) {
            var htmlArr = [];
            htmlArr.push('<li>');
            htmlArr.push('<span>【角色名称】</span>');
            htmlArr.push('<span class="layui-badge">' + data.RoleNick + '</span>');

            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【角色等级】</span>');
            htmlArr.push('<span class="layui-badge">' + data.GameLevel + '级</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【职业等级】</span>');
            htmlArr.push('<span class="layui-badge">' + data.LevelTitle + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【完美会员】</span>');
            htmlArr.push('<span class="layui-badge">VIP' + data.VipLevel + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【战斗力】</span>');
            htmlArr.push('<span class="layui-badge">' + data.Power + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【生命值】</span>');
            htmlArr.push('<span class="layui-badge">' + data.HealthPoint + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【魔力值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-blue">' + data.MagicPower + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【攻击力】</span>');
            htmlArr.push('<span class="layui-badge">' + data.ApValue + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【护甲值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-cyan">' + data.Armor + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【暴击值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-orange">' + data.CriticalValue + '</span>');
            htmlArr.push('</li>');
           

            $(".status_ul").html(htmlArr.join(''));
        },
        BindEnquiments: function () {
            for (var i = 1; i < 8; i++) {
                var enq = this.GetPlaceEnq(i);
                if (enq == null) {
                    $("#enq_" + i).html('无');
                } else {
                    $("#enq_" + i).html('<img place="' + i + '" src="' + enq.ImgPath + '"/>');
                }
            }
        },

        BackLast: function () {
            layer.closeAll();
        },
       
       
        ShowEnq: function () {
            var place = $(this).attr("place");
            var enq = ShowOtherRoleStatus.Instance.GetPlaceEnq(place);
            var enqHtmlArr = [];
            enqHtmlArr.push('<ul style="list-style: outside none none; color: white;">');
            enqHtmlArr.push(' <li><font color="' + PropModel.Intance.GetColorForQuality(enq.Quality) + '">' + enq.EquipmentName + '(' + PropModel.Intance.GetStrByPlace(enq.Place) + ')</font></li>');
            enqHtmlArr.push('<li><font color="' + PropModel.Intance.GetColorForQuality(enq.Quality) + '">品质:' + PropModel.Intance.GetQualityTitle(enq.Quality) + '</font></li>');
            enqHtmlArr.push(' <li><font color="#ffff33">强化+' + enq.EquipmentLevel + '</font></li>');
            enqHtmlArr.push(' <li><font color="' + (window.parent.UserBase.Status.GameLevel >= enq.NeedGameLevel ? "white" : "red") + '">需要等级:' + enq.NeedGameLevel + '</font></li>');
            enqHtmlArr.push('<li>职业要求:' + PropModel.Intance.GetNeedRole(enq.NeedRole) + '</li>');
            if (enq.ApValue > 0) {
                enqHtmlArr.push('<li><font color="green">攻击+' + enq.ApValue + '</font></li>');
            }
            if (enq.InitHealthPoint > 0) {
                enqHtmlArr.push('<li><font color="green">生命+' + enq.InitHealthPoint + '</font></li>');
            }
            if (enq.InitMagicPower > 0) {
                enqHtmlArr.push('<li><font color="green">魔力+' + enq.InitMagicPower + '</font></li>');
            }
            if (enq.InitArmor > 0) {
                enqHtmlArr.push('<li><font color="green">护甲+' + enq.InitArmor + '</font></li>');
            }
            if (enq.InitCriticalValue > 0) {
                enqHtmlArr.push('<li><font color="green">暴击+' + enq.InitCriticalValue + '</font></li>');
            }
            enqHtmlArr.push('<li><font color="green">战斗力+' + enq.Power + '</font></li>');
          
            enqHtmlArr.push('<li><font color="gray">' + enq.Introduce + '</font></li>');
            enqHtmlArr.push('</ul>');
            ToolManager.Layer.Message(enqHtmlArr.join(''));
            $(".layui-m-layerchild").css("background-color", "#171717");
        },
        GetPlaceEnq: function (place) {//装备所属位置1头，2衣服，3武器，4护腕，5鞋，6戒指,7项链
            var res = null;
            for (var i = 0; i < enqArrs.length; i++) {
                if (enqArrs[i].Place == place) {
                    res = enqArrs[i];
                    break;
                }
            }
            return res;
        },
      
       



    };
})();