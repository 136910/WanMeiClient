﻿(function () {
    BackPack = {};
    BackPack.Instance = {
        Init: function () {
            this.CommonBind($("#back_enquiment"), window.parent.UserBase.BackPackInfo.Equipments);
            this.CommonBind($("#back_material"), window.parent.UserBase.BackPackInfo.Materials);
            this.CommonBind($("#back_gemstone"), window.parent.UserBase.BackPackInfo.Gemstones);
            $(document).on('click', '.showDetail', this.ShowDetail);
            $(document).on('click', 'button[name="TakeOn"]', this.TakeOn);
            this.Refresh();
        },
        CommonBind: function (obj, list) {
            if (list.length > 0) {
                var arr = [];
                for (var i = 0; i < list.length; i++) {
                    arr.push('<li>');
                    arr.push(' <span class="showDetail" propType="' + list[i].PropType + '" proId="' + list[i].PropId + '">');
                    if (list[i].PropType == 1) {
                        arr.push('<img src="' + list[i].ImgPath + '" />x1</span>');
                    } else {
                        arr.push('<img src="' + list[i].ImgPath + '" />x' + list[i].Num + '</span>');
                    }
                    arr.push('</li>');
                }
                obj.html(arr.join(''));
            }
        },
        TakeOn: function () {
            var backId = $(this).attr("BackId");
            webExpress.utility.ajax.request("/User/TakeOn?token=" + ToolManager.Common.Token(), { "backId": backId, "heroId": window.parent.UserBase.Status.HeroId }, function (data) {
                if (data.IsSuccess) {
                    //更新装备列表和状态信息
                    window.parent.UserBase.Common.SyncLoadUserInfoModelAndEqs();
                    //更新背包信息
                    window.parent.UserBase.Common.SyncLoadUserBackPack();
                    layer.closeAll();
                } else {
                    ToolManager.Layer.ErrorTimeMsg(data.Message, 2);
                }
            });
        },
        Refresh: function () {
            //客户端刷新状态信息和装备列表    
            setInterval(function () {
                BackPack.Instance.CommonBind($("#back_enquiment"), window.parent.UserBase.BackPackInfo.Equipments);
            }, 2000)

        },
        ShowDetail: function () {
            var protype = $(this).attr("propType");
            var proid = $(this).attr("proId");
            PropModel.Intance.Show(protype, proid);
        },

    };
})();