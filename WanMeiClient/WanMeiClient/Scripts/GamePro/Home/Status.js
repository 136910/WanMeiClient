﻿(function () {
    Status = {};
    Status.Instance = {
        Init: function () {
            this.BindStatus();
            this.BindEnquiments();
            this.BindSkills();
            this.Refresh();
            $(document).on('click', '.enquiment_ul li img', this.ShowEnq);
            $(document).on('click', 'button[name="takeoff"]', this.TakeOff);
            $(document).on('click', '.skillDetail', this.ShowSkill);
            $(document).on('click', 'button[name="back_last"]', this.BackLast);
            $(document).on('click', 'button[name="studySkill"]', this.StudySkill);
        },
        BindStatus: function () {
            var htmlArr = [];
            htmlArr.push('<li>');
            htmlArr.push('<span>【角色等级】</span>');
            htmlArr.push('<span class="layui-badge">' + window.parent.UserBase.Status.GameLevel + '级</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【职业等级】</span>');
            htmlArr.push('<span class="layui-badge">' + window.parent.UserBase.Status.LevelTitle + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【完美会员】</span>');
            htmlArr.push('<span class="layui-badge">VIP' + window.parent.UserBase.Status.VipLevel + '</span>');
            htmlArr.push('</li>');

            htmlArr.push('<li>');
            htmlArr.push('<span>【疲劳值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-green">' + window.parent.UserBase.Status.DepositValue + '</span>/<span class="layui-badge">200</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【金币数量】</span>');
            htmlArr.push('<span class="layui-badge">' + window.parent.UserBase.Status.Money + '</span>');
            htmlArr.push('</li>');

            htmlArr.push('<li>');
            htmlArr.push('<span>【战斗力】</span>');
            htmlArr.push('<span class="layui-badge">' + window.parent.UserBase.Status.Power + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【生命值】</span>');
            htmlArr.push('<span class="layui-badge">' + window.parent.UserBase.Status.HealthPoint + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【魔力值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-blue">' + window.parent.UserBase.Status.MagicPower + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【攻击力】</span>');
            htmlArr.push('<span class="layui-badge">' + window.parent.UserBase.Status.ApValue + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【护甲值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-cyan">' + window.parent.UserBase.Status.Armor + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【暴击值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-orange">' + window.parent.UserBase.Status.CriticalValue + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【历练值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-green">' + window.parent.UserBase.Status.PotentialValue + '</span>');
            htmlArr.push('</li>');
            htmlArr.push('<li>');
            htmlArr.push('<span>【经验值】</span>');
            htmlArr.push('<span class="layui-badge layui-bg-green">' + window.parent.UserBase.Status.ExValue + '</span>/<span class="layui-badge">' + ToolManager.Common.NextJy(window.parent.UserBase.Status.GameLevel) + '</span>');
            htmlArr.push('</li>');

            $(".status_ul").html(htmlArr.join(''));
        },
        BindEnquiments: function () {
            for (var i = 1; i < 8; i++) {
                var enq = this.GetPlaceEnq(i);
                if (enq == null) {
                    $("#enq_" + i).html('无');
                } else {

                    $("#enq_" + i).html('<img place="' + i + '" src="' + enq.ImgPath + '"/>');
                }
            }
        },
        BindSkills: function () {
            var skillList = window.parent.UserBase.StatusSkills;
            if (skillList.length > 0) {
                var skillHtmlArr = [];
                for (var i = 0; i < skillList.length; i++) {
                    skillHtmlArr.push('<li>');
                    skillHtmlArr.push('<span class="skillDetail" SkId="' + skillList[i].Id + '" ><img style="width:18%;" src="' + skillList[i].SkillIcon + '" /></span>');
                    skillHtmlArr.push('<span style="margin-left:3%;" class="layui-badge layui-bg-black">' + skillList[i].SkillName + '</span>');
                    if (skillList[i].IsStudy > 0)
                        skillHtmlArr.push('<span class="layui-badge layui-bg-green" style="margin-left:3%;">已学</span>');
                    else
                        skillHtmlArr.push('<span class="layui-badge" style="margin-left:3%;">未学</span>');

                    skillHtmlArr.push('</li>');
                }
                $(".skill_ul").html(skillHtmlArr.join(''));
            }
        },
        //学习或者升级技能
        StudySkill: function () {
            var skid = $(this).attr("SkillId");
            var skill = Status.Instance.GetSkill(skid);
            if (window.parent.UserBase.Status.GameLevel < skill.OptionNeedLevel) {
                ToolManager.Layer.ErrorTimeMsg("等级不足", 2);
                return false;
            }
            if (window.parent.UserBase.Status.PotentialValue < skill.NeedPotentialValue) {
                ToolManager.Layer.ErrorTimeMsg("历练值不足", 2);
                return false;
            }
            if (skill.SkillLevel == 20) {
                ToolManager.Layer.ErrorTimeMsg("已达到满级", 2);
                return false;
            }
            webExpress.utility.ajax.request("/Skill/Study?token=" + ToolManager.Common.Token(), { "skillId": skid, "heroId": window.parent.UserBase.Status.HeroId, "needPotential": skill.NeedPotentialValue }, function (data) {
                if (data.IsSuccess) {
                    //更新角色状态模型，服务端，用户信息缓存已经更新，为了性能客户端，直接修改JS模型
                    window.parent.UserBase.Status.PotentialValue -= skill.NeedPotentialValue;
                    //更新技能信息
                    window.parent.UserBase.Common.SyncLoadSkillList();
                    ToolManager.Layer.ErrorTimeMsg("成功", 2);
                    layer.closeAll();
                } else {
                    ToolManager.Layer.ErrorTimeMsg(data.Message, 2);
                }
            });
        },
        ShowSkill: function () {
            var id = $(this).attr("SkId");
            var skill = Status.Instance.GetSkill(id);
            var arr = [];
            arr.push('<ul style="list-style: outside none none; color: white;">');
            arr.push('<li><img style="width: 20%;" src="' + skill.SkillIcon + '"/></li>');
            arr.push('<li><font color="#f132a5">' + skill.SkillName + '</font></li>');
            arr.push('<li><font color="#f132a5">' + skill.SkillIntroduce + '</font></li>');
            if (skill.IsStudy <= 0) {
                arr.push('<li><font color="#ffff33">技能等级:1</font></li>');
            } else {
                arr.push('<li><font color="#ffff33">技能等级:' + skill.SkillLevel + '</font></li>');
            }
            switch (skill.SkillType) {
                case 1:
                    if (skill.SkillOption == 1) {
                        arr.push(' <li><font color="#ffff33">类型:立即伤害</font></li>');
                        arr.push(' <li><font color="#ffff33">提升伤害量:' + skill.SkillBussinessValue + '</font></li>');
                    } else {
                        arr.push(' <li><font color="#ffff33">类型:立即治疗</font></li>');
                        arr.push(' <li><font color="#ffff33">提升治疗量:' + skill.SkillBussinessValue + '</font></li>');
                    }
                    break;
                case 2:
                    if (skill.SkillOption == 1) {
                        arr.push(' <li><font color="#ffff33">类型:持续伤害</font></li>');
                        arr.push(' <li><font color="#ffff33">每隔' + skill.IntervalTime + '秒造成' + skill.SkillBussinessValue + '点提升伤害</font></li>');
                    } else {
                        arr.push(' <li><font color="#ffff33">类型:持续治疗</font></li>');
                        arr.push(' <li><font color="#ffff33">每隔' + skill.IntervalTime + '秒造成' + skill.SkillBussinessValue + '点提升治疗</font></li>');
                    }
                    arr.push(' <li><font color="#ffff33">持续时间:' + skill.SkillTime + '秒</font></li>');
                    break;
                case 3:
                    arr.push('<li><font color="#ffff33">类型:被动</font></li>');
                    if (skill.SkillOption == 1) {                    
                        arr.push(' <li><font color="#ffff33">增益提升:' + skill.SkillBussinessValue + '</font></li>');
                    } else {
                        arr.push(' <li><font color="#ffff33">减益提升:' + skill.SkillBussinessValue + '</font></li>');
                    }
                    break;
            }
            arr.push(' <li><font color="#ffff33">冷却时间:' + skill.CdTime + '</font></li>');
            arr.push(' <li><font color="blue">需要魔力:' + skill.NeedMagic + '</font></li>');
            if (window.parent.UserBase.Status.GameLevel < skill.OptionNeedLevel) {
                arr.push(' <li><font color="red">学习等级:' + skill.OptionNeedLevel + '</font></li>');
            } else {
                arr.push(' <li><font color="white">学习等级:' + skill.OptionNeedLevel + '</font></li>');
            }
            if (skill.IsStudy <= 0) {
                arr.push('<li><font color="#bd1d14">未学习</font></li>');
                arr.push(' <li><font color="white">需要历练值:' + skill.NeedPotentialValue + '</font></li>');
                arr.push('<li> <button class="layui-btn layui-btn-sm layui-btn-normal" name="studySkill" SkillId="' + skill.Id + '">学习</button> <button class="layui-btn layui-btn-sm layui-btn-normal" name="back_last" >返回</button></li>');
            } else {
                arr.push('<li><font color="green">已学习</font></li>');
                arr.push(' <li><font color="white">下一级收益提升:' + (skill.SkillBussinessValue + skill.GroupUpValue) + '</font></li>');
                arr.push(' <li><font color="white">下一级需要历练值:' + skill.NeedPotentialValue + '</font></li>');
                arr.push('<li> <button class="layui-btn layui-btn-sm layui-btn-normal" name="studySkill" SkillId="' + skill.Id + '">升级</button> <button class="layui-btn layui-btn-sm layui-btn-normal" name="back_last" >返回</button></li>');
            }
            arr.push('<li></li>');
            arr.push('</ul>');
            ToolManager.Layer.Message(arr.join(''));
            $(".layui-m-layerchild").css("background-color", "#171717");
            
            
        },
        BackLast: function () {
            layer.closeAll();
        },
        //脱下装备-》状态面板变化，装备面板变化，背包面板变化
        TakeOff: function () {
            var backId = $(this).attr("BackId");
            webExpress.utility.ajax.request("/User/TakeOff?token=" + ToolManager.Common.Token(), { "backId": backId, "heroId": window.parent.UserBase.Status.HeroId }, function (data) {
                if (data.IsSuccess) {
                    //更新装备列表和状态信息
                    window.parent.UserBase.Common.SyncLoadUserInfoModelAndEqs();
                    //更新背包信息
                    window.parent.UserBase.Common.SyncLoadUserBackPack();
                    layer.closeAll();
                } else {
                    ToolManager.Layer.ErrorTimeMsg(data.Message, 2);
                }
            });
        },
        Refresh: function () {
            //客户端刷新状态信息和装备列表    
            setInterval(function () {
                Status.Instance.BindEnquiments();
                Status.Instance.BindStatus();
                Status.Instance.BindSkills();
            }, 2000)

        },
        ShowEnq: function () {
            var place = $(this).attr("place");
            var enq = Status.Instance.GetPlaceEnq(place);
            var enqHtmlArr = [];
            enqHtmlArr.push('<ul style="list-style: outside none none; color: white;">');
            enqHtmlArr.push(' <li><font color="' + PropModel.Intance.GetColorForQuality(enq.Quality) + '">' + enq.EquipmentName + '(' + PropModel.Intance.GetStrByPlace(enq.Place) + ')</font></li>');
            enqHtmlArr.push('<li><font color="' + PropModel.Intance.GetColorForQuality(enq.Quality) + '">品质:' + PropModel.Intance.GetQualityTitle(enq.Quality) + '</font></li>');
            enqHtmlArr.push(' <li><font color="#ffff33">强化+' + enq.EquipmentLevel + '</font></li>');
            enqHtmlArr.push(' <li><font color="' + (window.parent.UserBase.Status.GameLevel >= enq.NeedGameLevel ? "white" : "red") + '">需要等级:' + enq.NeedGameLevel + '</font></li>');
            enqHtmlArr.push('<li>职业要求:' + PropModel.Intance.GetNeedRole(enq.NeedRole) + '</li>');
            if (enq.ApValue > 0) {
                enqHtmlArr.push('<li><font color="green">攻击+' + enq.ApValue + '</font></li>');
            }
            if (enq.InitHealthPoint > 0) {
                enqHtmlArr.push('<li><font color="green">生命+' + enq.InitHealthPoint + '</font></li>');
            }
            if (enq.InitMagicPower > 0) {
                enqHtmlArr.push('<li><font color="green">魔力+' + enq.InitMagicPower + '</font></li>');
            }
            if (enq.InitArmor > 0) {
                enqHtmlArr.push('<li><font color="green">护甲+' + enq.InitArmor + '</font></li>');
            }
            if (enq.InitCriticalValue > 0) {
                enqHtmlArr.push('<li><font color="green">暴击+' + enq.InitCriticalValue + '</font></li>');
            }
            enqHtmlArr.push('<li><font color="green">战斗力+' + enq.Power + '</font></li>');
            enqHtmlArr.push('<li>出售价格:<font color="#ffff00">' + enq.SalePrice + '</font></li>');
            enqHtmlArr.push('<li><font color="gray">' + enq.Introduce + '</font></li>');
            enqHtmlArr.push('<li> <button class="layui-btn layui-btn-sm layui-btn-normal" name="takeoff" BackId="' + enq.Id + '" Place="' + enq.place + '">脱下</button><button class="layui-btn layui-btn-sm layui-btn-normal" name="back_last" >返回</button></li>');
            enqHtmlArr.push('</ul>');
            ToolManager.Layer.Message(enqHtmlArr.join(''));
            $(".layui-m-layerchild").css("background-color", "#171717");
        },
        GetPlaceEnq: function (place) {//装备所属位置1头，2衣服，3武器，4护腕，5鞋，6戒指,7项链
            var res = null;
            for (var i = 0; i < window.parent.UserBase.Equipments.length; i++) {
                if (window.parent.UserBase.Equipments[i].Place == place) {
                    res = window.parent.UserBase.Equipments[i];
                    break;
                }
            }
            return res;
        },
        GetSkill: function (id) {
            var res = null;
            for (var i = 0; i < window.parent.UserBase.StatusSkills.length; i++) {
                if (window.parent.UserBase.StatusSkills[i].Id == id) {
                    res = window.parent.UserBase.StatusSkills[i];
                    break;
                }
            }
            return res;
        },
        GetSkillTypeName: function (skillType) {
            var res = "";
            switch (skillType)
            {
                case 1:
                    res = "立即生效";
                    break;
                case 2:
                    res = "持续性";
                    break;
                case 3:
                    res = "被动";
                    break;
            }
            return res;
        }



    };
})();