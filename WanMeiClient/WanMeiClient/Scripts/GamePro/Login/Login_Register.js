﻿(function () {
    //$.connection.hub.url = "http://localhost:8080/signalr";
    //var chat_user = $.connection.userHub;
    var _isLogin = true;
    Login_Register = {};
    Login_Register.Instance = {
        Init: function () {
            $(document).on('click', '#goto_register', this.GoToRegister);
            $(document).on('click', '#return_login', this.ReturnLogin);
            $(document).on('click', '#btn_register', this.BtnRegister);
            $(document).on('click', '#btn_login', this.BtnLogin);
            //this.NoticeClient();
            //如果用户记住账号密码，给赋值
            $("#username").val(jQuery.cookie('WANMEIUSERNAME'))
            if (jQuery.cookie('WANMEIPASSWORD') != undefined)
                $("#password").val(ToolManager.Base64.Decode(jQuery.cookie('WANMEIPASSWORD')));
        },
        GoToRegister: function () {
            $("#pannel_login").hide(200, function () {
                $("#pannel_register").show(200);
            });
        },
        ReturnLogin: function () {
            $("#pannel_register").hide(200, function () {
                $("#pannel_login").show(200);
            })
        },
        BtnLogin: function () {
            _isLogin = true;
            var username = $("#username").val().trim();
            var password = $("#password").val().trim();
            var rememberMe = $('#register_rememberMe').is(':checked');
            if (username == "" || password == "") {
                ToolManager.Layer.ErrorTimeMsg("账号和密码不能为空", 2);
                return false;
            }
            if (username.length < 6 || password.length < 6) {
                ToolManager.Layer.ErrorTimeMsg("账号和密码长度不能小于6", 2);
                return false;
            }
            if (rememberMe) { //如果记密码 
                jQuery.cookie('WANMEIUSERNAME', username, { expires: 30 });
                jQuery.cookie('WANMEIPASSWORD', ToolManager.Base64.Encode(password), { expires: 30 });
            } else {
                jQuery.cookie("WANMEIUSERNAME", '', { expires: -1 });
                jQuery.cookie("WANMEIPASSWORD", '', { expires: -1 });
            }
            var loginModel = GameModel.User.CreateRegisterModel(username, password);
            ToolManager.Layer.Loding("登录中.....");
            webExpress.utility.ajax.request("/Login/Login", loginModel, function (data) {
                layer.closeAll('loading');
                if (data.IsSuccess) {
                    window.location.href = "/Login/ChoseRole";
                } else {
                    ToolManager.Layer.ErrorTimeMsg(data.Message, 2);
                }
            });
            //$.connection.hub.start().done(function () {
            //    var loginModel = GameModel.User.CreateRegisterModel(username, password);
            //    chat_user.server.login(loginModel);
            //    ToolManager.Layer.Loding("登录中.....");
            //});
        },
        BtnRegister: function () {
            _isLogin = false;
            var register_username = $("#register_username").val().trim();
            var register_password = $("#register_password").val().trim();
            var register_repassword = $("#register_repassword").val().trim();
            var introducer = $("#introducer").val().trim();
            var isRember = $('#register_rememberMe').is(':checked');
            if (register_username == "" || register_password == "" || register_repassword == "") {
                ToolManager.Layer.ErrorTimeMsg("前三行数据不能为空", 2);
                return false;
            }
            if (register_username.length < 6 || register_password.length < 6) {
                ToolManager.Layer.ErrorTimeMsg("账号和密码长度不能小于6", 2);
                return false;
            }
            if (register_password != register_repassword) {
                ToolManager.Layer.ErrorTimeMsg("两次密码输入不一致", 2);
                return false;
            }
            var registerModel = GameModel.User.CreateRegisterModel(register_username, register_password, register_repassword);
            ToolManager.Layer.Loding("注册中.....");
            webExpress.utility.ajax.request("/Login/Register", registerModel, function (data) {
                layer.closeAll('loading');
                if (data.IsSuccess) {
                    ToolManager.Layer.Message("注册成功,即将为您进入游戏", function () {
                        setTimeout(function () {
                            window.location.href = "/Login/ChoseRole";
                        }, 1)
                    });
                } else {
                    ToolManager.Layer.ErrorTimeMsg(data.Message, 2);
                }
            });
            //$.connection.hub.start().done(function () {
            //    var registerModel = GameModel.User.CreateRegisterModel(register_username, register_password, register_repassword);
            //    chat_user.server.register(registerModel);
            //    ToolManager.Layer.Loding("注册中.....");
            //});
        }
        //服务器通信给客户端
        //NoticeClient: function () {
        //    //注册通知
        //    chat_user.client.receiveRegisterAndLogin = function (rtn) {
        //        layer.closeAll('loading');
        //        if (rtn.IsSuccess) {
        //            ToolManager.Common.AddCache(rtn.Message);
        //            ToolManager.Layer.ErrorTimeMsg("登录已保存", 2);
        //            if (!_isLogin) {
        //                ToolManager.Layer.Message("注册成功,即将为您进入游戏", function () {
        //                    setTimeout(function () {
        //                        window.location.href = "/Login/ChoseRole?un=" + rtn.Message;
        //                    }, 1)
        //                });
        //            } else {
        //                window.location.href = "/Login/ChoseRole?un=" + rtn.Message;
        //            }
        //        } else {
        //            ToolManager.Layer.ErrorTimeMsg(rtn.Message, 2);
        //        }
        //    }
        //}
    };
})();