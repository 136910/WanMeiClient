﻿(function () {

    ChoseHero = {};
    ChoseHero.Instance = {
        Init: function () {
            $(document).on('click', 'h3[name="setNick"]', this.SetNick);
            $(document).on('click', '.gotogame', this.GotoIndex);
        },
        //进入游戏
        GotoIndex: function () {
            var isHasNick = $(this).attr("isHasNick");
            if (isHasNick == "0") {
                //设置角色名称
                ToolManager.Layer.ErrorTimeMsg("请先设置角色昵称", 4);
                return false;
            }
            var heroId = $(this).attr("heroId");
            window.location.href = "/home/index?heroId=" + heroId;
        },
        //设置昵称
        SetNick: function () {
            var heroId = $(this).attr("heroId");
            var obj = $(this);
            ToolManager.Layer.SelfSet('<p>请输入6个字以内的昵称:</p><input id="tempNick" type="text" /><p><span id="errmsg" style="color:red"></span></p>', '确定', '取消', function (index) {
                var _nickName = $("#tempNick").val().trim();
                if (_nickName == "") {
                    $("#errmsg").text("空昵称");
                    return false;
                }
                if (_nickName.length > 6) {
                    $("#errmsg").text("长度不要超过6");
                    return false;
                }
                webExpress.utility.ajax.request("/User/SetNick?token=" + ToolManager.Common.Token(), { "nick": _nickName, "heroId": heroId }, function (data) {
                    if (data.IsSuccess) {
                        obj.html(_nickName).attr("name","");
                        $("#go_" + heroId).attr("isHasNick", "1");
                        layer.close(index);
                    } else {
                        $("#errmsg").text(data.Message);
                    }
                });
                //layer.close(index);
            });
           
        }

    };
})()