﻿window.art = webExpress;
//$.messager.defaults = { ok: "确定", cancel: "取消" };
yg = {};
yg.data = {};
yg.data.usa = {};


//  上传文件部分Start
yg.ajaxFileUpload = function ajaxFileUpload(divBox, fileControl, photoTypeName, needTitle) {
    if ($(".commodelPanel").find('img').length > 0) {
        alert("只能上传1张，请先删除当前图片，然后在上传");
        return false;
    }
    var uploadCommodel = $(divBox);
    uploadCommodel.find(".msgPanel").ajaxStart(function () {
        $(this).html("正在上传<img src='/Content/Images/loading.gif'/>");
    }).ajaxComplete(function () {
        $(this).html("");
    });

    $.ajaxFileUpload
    ({
        url: '/handels/FileUploadHandler.ashx?object=' + uploadCommodel.attr("object"),
        secureuri: false,
        fileElementId: fileControl,
        dataType: 'json',
        success: function (data, status) {
            if (typeof (data.states) != 'undefined') {
                if (data.states == "false") {
                    yg.showMsg(uploadCommodel, data.msg);
                }
                else {
                    //alert(data.msg);
                    yg.showMsg(uploadCommodel, data.msg);
                    yg.showPicDiv(uploadCommodel, data, photoTypeName, needTitle);
                }
            }
        },
        error: function (data, status, e) {
            alert(e);
        }
    })
    return false;
}
yg.showMsg = function showMsg(uploadCommodel, msg) {
    uploadCommodel.find(".msgPanel").html(msg);
}
yg.showPicDiv = function showPicDiv(uploadCommodel, data, photoTypeName, needTitle) {
    var picDivID = photoTypeName + "_" + Math.ceil(Math.random() * 1000);
    var picDiv = '<div id="' + picDivID + '" class="housepicone">'
                + '<div class="housepic">'
                + '<img height="99" width="132" src="' + data.minPhotoUrl + '" alt="" />'
                + '<div class="deletepic1">'
                + '<a onclick="yg.deletePicDiv(\'' + picDivID + '\');" href="javascript:;">删除</a>'
                + '</div>'
                + '<input type="hidden" value="' + data.minPhotoUrl + '" name="' + photoTypeName + '_Min" />'
                + '<input type="hidden" value="' + data.maxPhotoUrl + '" name="' + photoTypeName + '_Max" />'
                + '</div>';
    if (needTitle) {
        picDiv = picDiv + '<div class="housepicw1" style="display:none;"><textarea name="' + photoTypeName + '_Title">照片描述最多16个中文字</textarea><span style="color: rgb(102, 102, 102);">描述：(选填)</span><br /></div></div>';
    }
    else {
        picDiv = picDiv + '<div style="display:none;" class="housepicw1"><textarea name="' + photoTypeName + '_Title">描述：(选填)</textarea><span style="color: rgb(102, 102, 102);">描述：(选填)</span><br /></div></div>';
    }

    //uploadCommodel.find(".defaultPicPanel").hide();
    uploadCommodel.find(".defaultPicPanel").remove();
    uploadCommodel.find(".commodelPanel").append(picDiv);
}

yg.deletePicDiv = function deletePicDiv(picDivID) {
    $("#" + picDivID).parent().parent().find(".msgPanel").html("");
    $("#" + picDivID).remove();
}