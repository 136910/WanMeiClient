﻿/////公共分页扩展类@@feiyang
(function () {
    PageCommonExpress = new PageCommonExpressClass();
    function PageCommonExpressClass() {
        var _self = this;
        function _init() {
            _self.init = init;
            _self.pageBind = pageBind;
         
        }
        function init() {
            //$(document).on('click', '#pageData a', pageOnClick);
        }

        //加载绑定分页
        function pageBind(pagingResult) {
            if (pagingResult.TotalCount == 0) {
                $("#pageData").html("共0条数据");
                return false;
            } else {
                var dataHtmlArr = [];
                dataHtmlArr.push('<div class="pages">');
                dataHtmlArr.push('<span>共<font>' + pagingResult.TotalCount + '</font>条数据</span>');

                if (pagingResult.HasPreviousPage) {
                    dataHtmlArr.push('<a pageindex="' + (pagingResult.PageIndex - 1) + '" href="javascript:void(0)">前一页>></a>');
                } else {
                    dataHtmlArr.push('<b><font color="#cccccc">前一页</font></b>');
                }
                //页码过多时，动态取当前5页
                if (pagingResult.TotalPages <= 5) {
                    for (var i = 0; i < pagingResult.TotalPages; i++) {
                        if (i == pagingResult.PageIndex) {
                            dataHtmlArr.push('<b>' + (i + 1) + '</b>');
                        } else {
                            dataHtmlArr.push('<a PageIndex="' + i + '" href="javascript:void(0)">' + (i + 1) + '</a>');
                        }
                    }
                } else {
                    if (pagingResult.TotalPages - (pagingResult.PageIndex + 1) >= 5) {
                        for (var i = pagingResult.PageIndex; i < pagingResult.PageIndex + 5; i++) {
                            if (i == pagingResult.PageIndex) {
                                dataHtmlArr.push('<b>' + (i + 1) + '</b>');
                            } else {
                                dataHtmlArr.push('<a PageIndex="' + i + '" href="javascript:void(0)">' + (i + 1) + '</a>');
                            }
                        }
                        dataHtmlArr.push('...');
                        dataHtmlArr.push('<a pageindex="' + (pagingResult.TotalPages - 1) + '" href="javascript:void(0)">' + pagingResult.TotalPages + '</a>');
                    } else {
                        for (var i = pagingResult.TotalPages - 5 ; i < pagingResult.TotalPages; i++) {
                            if (i == pagingResult.PageIndex) {
                                dataHtmlArr.push('<b>' + (i + 1) + '</b>');
                            } else {
                                dataHtmlArr.push('<a PageIndex="' + i + '" href="javascript:void(0)">' + (i + 1) + '</a>');
                            }
                        }
                    }
                }
                if (pagingResult.HasNextPage) {
                    dataHtmlArr.push('<a pageindex="' + (pagingResult.PageIndex + 1) + '" href="javascript:void(0)">下一页>></a>');
                } else {
                    dataHtmlArr.push('<b><font color="#cccccc">下一页</font></b>');
                }
                dataHtmlArr.push('</div>');
                $("#pageData").html(dataHtmlArr.join(""));

            }
        }

        _init();

    }
})();