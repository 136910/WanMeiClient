﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WanMeiClient.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //TempData["LayoutUserName"] = UserName;
            string RequestInfo = string.Format("控制器名称:{0}----方法名:{1}----访问IP:{2}", filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName, HttpContext.Request.UserHostAddress);
            //LogManger.Instance.WriteLog(RequestInfo);
            base.OnActionExecuting(filterContext);
        }
    }
}