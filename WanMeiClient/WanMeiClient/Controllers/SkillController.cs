﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WanMeiBussinessService;
using WanMeiCommon;
using WanMeiModel;

namespace WanMeiClient.Controllers
{
    public class SkillController : BaseController
    {
        //
        // GET: /Skill/
        private ISkillService _ISkillService;
        private IHeroService _IHeroService;
        public SkillController(ISkillService ISkillService, IHeroService IHeroService)
        {
            _ISkillService = ISkillService;
            _IHeroService = IHeroService;
        }

        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 获取角色状态中全部技能列表
        /// </summary>
        /// <param name="heroId"></param>
        /// <returns></returns>
        [JsonException]
        [CheckLoginFitler]
        public JsonResult GetStatusSkillList(int heroId)
        {
            string un = Session["un"].ToString();
            List<StatusSkillItem> result = AssemblyData.Instance.GetStatusSkillList(heroId, un);
            return Json(ResultManager.CreateResult<List<StatusSkillItem>>(true, result), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 学习或升级技能
        /// </summary>
        /// <param name="skillId"></param>
        /// <param name="heroId"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [CheckLoginFitler]
        [JsonException]
        public JsonResult Study(int skillId, int heroId, long needPotential)
        {
            string un = SecretClass.DecryptQueryString(Session["un"].ToString());
            int res = _ISkillService.Study(skillId, heroId, un, needPotential);
            bool result = false;
            string err_msg = string.Empty;
            switch (res)
            {
                case 2:
                    err_msg = "角色不存在";
                    break;
                case 3:
                    err_msg = "技能不存在";
                    break;
                case 4:
                    err_msg = "等级不足";
                    break;
                case 5:
                    err_msg = "历练值不足";
                    break;
                case 7:
                    err_msg = "已经满级";
                    break;
                case 6:
                case -1:
                    err_msg = "网络异常";
                    break;
                case 1:
                    result = true;
                    //更新状态列表技能缓存，角色当前状态缓存减去需要的历练值
                    string key = "StatusSkill_" + heroId + "_" + Session["un"].ToString();
                    RedisManager.Remove(key);
                    UserInfoModelResult userInfo = AssemblyData.Instance.GetUserInfoModelResult(heroId, Session["un"].ToString());
                    string userKey = "UserInfoModel_" + heroId + "_" + Session["un"].ToString();
                    userInfo.UserRoleModel.PotentialValue -= needPotential;
                    RedisManager.Set<UserInfoModelResult>(userKey, userInfo, DateTime.Now.AddDays(1));
                    break;
            }
            return Json(ResultManager.CreateResult(result, err_msg));
        }
    }
}
