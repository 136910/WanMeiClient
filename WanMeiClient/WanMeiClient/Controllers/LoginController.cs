﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WanMeiBussinessService;
using WanMeiCommon;
using WanMeiModel;

namespace WanMeiClient.Controllers
{
    public class LoginController : Controller
    {
        private IHeroService _IHeroService;
        private IUserService _IUserService;
        public LoginController(IHeroService IHeroService, IUserService IUserService)
        {
            _IHeroService = IHeroService;
            _IUserService = IUserService;
        }
   
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Register(UserRegisterModel model)
        {
            var msg = "";
            if (string.IsNullOrEmpty(model.UserName) || string.IsNullOrEmpty(model.PassWord) || string.IsNullOrEmpty(model.RePassWord))
            {
                msg = "前三行数据不能为空";
                return Json(ResultManager.CreateReuslt(false, msg));
            }
            if (model.UserName.Length < 6 || model.PassWord.Length < 6)
            {
                msg = "账号和密码长度不能小于6";
                return Json(ResultManager.CreateReuslt(false, msg));
            }
            if (model.PassWord != model.RePassWord)
            {
                msg = "两次密码不一致";
                return Json(ResultManager.CreateReuslt(false, msg));
            }
            int returnValue = _IUserService.Register(model);
            bool res = true;
            switch (returnValue)
            {
                case 1:
                    Session["un"] = SecretClass.EncryptQueryString(model.UserName);
                    break;
                case 2:
                    res = false;
                    msg = "已存在用户";
                    break;
                default:
                    res = false;
                    msg = "注册出错!";
                    break;
            }
            return Json(ResultManager.CreateReuslt(res, msg));
        }

        public JsonResult Login(UserLoginModel model)
        {
            var msg = "";
            if (string.IsNullOrEmpty(model.UserName) || string.IsNullOrEmpty(model.PassWord))
            {
                msg = "账号和密码不能为空";
                return Json(ResultManager.CreateReuslt(false, msg));
            }
            if (model.UserName.Length < 6 || model.PassWord.Length < 6)
            {
                msg = "账号和密码长度不能小于6";
                return Json(ResultManager.CreateReuslt(false, msg));
            }
            int returnValue = _IUserService.Login(model);
            bool res = true;
            switch (returnValue)
            {
                case 1:
                    Session["un"] = SecretClass.EncryptQueryString(model.UserName);
                    break;
                case 2:
                    res = false;
                    msg = "用户不存在";
                    break;
                default:
                    res = false;
                    msg = "账号或密码错误!";
                    break;
            }
            return Json(ResultManager.CreateReuslt(res, msg));
        }

        [CheckLoginFitler]
        public ActionResult ChoseRole()
        {
            string un = Session["un"].ToString();
            string trueUserName = SecretClass.DecryptQueryString(un);
            List<ChoseHeroItem> result = _IHeroService.GetHerosByUserName(trueUserName);
            return View(result);
        }

        public JsonResult AddCache(string un)
        {
            CacheHelper.SetCache(un,"1",60*12);
            return Json(new ResultModel(true, ""), JsonRequestBehavior.AllowGet);
        }
    }
}
