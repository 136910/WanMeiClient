﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WanMeiBussinessService;
using WanMeiCommon;
using WanMeiModel;


namespace WanMeiClient.Controllers
{
    public class UserController : BaseController
    {
        //
        // GET: /User/
        private IHeroService _IHeroService;
        private IUserService _IUserService;
        public UserController(IHeroService IHeroService, IUserService IUserService)
        {
            _IHeroService = IHeroService;
            _IUserService = IUserService;
        }

        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 获取装备+基本属性的角色信息和当前装备列表
        /// </summary>
        /// <param name="heroId"></param>
        /// <returns></returns>
        [CheckLoginFitler]
        [JsonException]
        public JsonResult SyncLoadUserInfoModelAndEqs(int heroId)
        {
            string un = Session["un"].ToString();
            UserInfoModelResult result = AssemblyData.Instance.GetUserInfoModelResult(heroId, un);
            return Json(ResultManager.CreateResult<UserInfoModelResult>(true, result), JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [CheckLoginFitler]
        [JsonException]
        public JsonResult GetTargetUserInfo(int heroId, string un)
        {
            UserInfoModelResult result = AssemblyData.Instance.GetUserInfoModelResult(heroId, un);
            return Json(ResultManager.CreateResult<UserInfoModelResult>(true, result), JsonRequestBehavior.AllowGet);
        }

        [CheckLoginFitler]
        public JsonResult SetNick(string nick, int heroId)
        {
            bool result = false;
            string errmsg = string.Empty;
            try
            {
                string un = SecretClass.DecryptQueryString(Session["un"].ToString());
                int ret = _IUserService.SetRole(nick, un, heroId);
                switch (ret)
                {
                    case 1:
                        result = true;
                        break;
                    case 2:
                        errmsg = "用户不存在";
                        break;
                    case 3:
                        errmsg = "昵称已存在";
                        break;
                    case 5:
                        errmsg = "当前账户已存在该角色,不要随意篡改参数";
                        break;
                    case 4:
                    case -1:
                        errmsg = "设置出错500";
                        break;

                }
            }
            catch (Exception ex)
            {
                errmsg = "设置异常500";
                LogManger.Instance.WriteLog(ex.ToString());
            }
            return Json(ResultManager.CreateReuslt(result, errmsg), JsonRequestBehavior.AllowGet);
        }

        [JsonException]
        [CheckLoginFitler]
        public JsonResult LoadCurrentEquipments(int heroId)
        {
            string un = Session["un"].ToString();
            List<HeroEquipmentItem> result = null;

            string key = "Equipments_" + heroId + "_" + un;
            Func<InputArgs, List<HeroEquipmentItem>> func = delegate(InputArgs ia)
            {
                return _IHeroService.GetCurrentEquipments(ia.Un, ia.HeroId);
            };
            result = DataIntegration.GetData<List<HeroEquipmentItem>>(key, func, new InputArgs(heroId, un));
            return Json(ResultManager.CreateResult<List<HeroEquipmentItem>>(true, result), JsonRequestBehavior.AllowGet);
        }

        [JsonException]
        [CheckLoginFitler]
        public JsonResult SyncLoadUserBackPack(int heroId)
        {
            string un = SecretClass.DecryptQueryString(Session["un"].ToString());
            HeroBackPackPropManager result = null;

            string key = "BackPack_" + heroId + "_" + Session["un"].ToString();
            Func<InputArgs, HeroBackPackPropManager> func = delegate(InputArgs ia)
            {
                return _IHeroService.GetCurrentBackPackInfos(ia.Un, ia.HeroId);
            };
            result = DataIntegration.GetData<HeroBackPackPropManager>(key, func, new InputArgs(heroId, un));
            return Json(ResultManager.CreateResult<HeroBackPackPropManager>(true, result), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 脱下装被
        /// </summary>
        /// <param name="backId"></param>
        /// <returns></returns>
        /// 
        [JsonException]
        [CheckLoginFitler]
        public JsonResult TakeOff(int backId, int heroId)
        {
            string un = SecretClass.DecryptQueryString(Session["un"].ToString());
            bool result = _IHeroService.TakeOff(un, backId);
            if (result)
            {
                //删除装备列表+角色模型信息缓存，客户端刷新获取
                //更新背包列表缓存,由于需要往背包装备列表添加模型，比较麻烦，就直接移除这个缓存，客户端直接刷新从新获取
                List<string> keys = new List<string>();
                keys.Add("UserInfoModel_" + heroId + "_" + Session["un"].ToString());
                keys.Add("BackPack_" + heroId + "_" + Session["un"].ToString());
                RedisManager.RemoveList(keys);
                return Json(ResultManager.CreateReuslt(true, string.Empty), JsonRequestBehavior.AllowGet);
            }
            return Json(ResultManager.CreateReuslt(false, string.Empty), JsonRequestBehavior.AllowGet);
        }

        [CheckLoginFitler]
        public JsonResult TakeOn(int backId, int heroId)
        {
            string un = SecretClass.DecryptQueryString(Session["un"].ToString());
            int res = _IHeroService.TakeOn(un, backId, heroId);
            bool resut = false;
            string err = string.Empty;
            switch (res)
            {
                case -1:
                case 4:
                    err = "数据异常";
                    break;
                case 2:
                    err = "当前道具不存在";
                    break;
                case 3:
                    err = "等级不足";
                    break;
                case 1:
                    resut = true;
                    // //删除装备列表+角色模型信息缓存，客户端刷新获取，删除当前背包缓存
                    var keys = new List<string>();
                    keys.Add("UserInfoModel_" + heroId + "_" + Session["un"].ToString());
                    keys.Add("BackPack_" + heroId + "_" + Session["un"].ToString());
                    RedisManager.RemoveList(keys);
                    break;
            }
            return Json(ResultManager.CreateReuslt(resut, err), JsonRequestBehavior.AllowGet);
        }
    }
}
