﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WanMeiClient.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/
        [CheckLoginFitler]
        public ActionResult Index()
        {
            ViewBag.un = Session["un"].ToString();
            return View();
        }
        [CheckLoginFitler]
        public ActionResult Status()
        {
            return View();
        }
        [CheckLoginFitler]
        public ActionResult BackPack()
        {
            return View();
        }
        [CheckLoginFitler]
        public ActionResult ShowOtherRoleStatus()
        {
            return View();
        }
    }
}
