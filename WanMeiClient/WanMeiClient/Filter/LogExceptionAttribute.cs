﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WanMeiCommon;

namespace WanMeiClient
{ 
    /// <summary>
    /// 全局针对页面跳转错误页级别的过滤器，本项目由于是异步加载，暂时不用
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
    public class LogExceptionAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                string controllerName = (string)filterContext.RouteData.Values["controller"];
                string actionName = (string)filterContext.RouteData.Values["action"];
                string msg = string.Format("Conrtoll:{0}------Action:{1}-----{2}", controllerName, actionName, filterContext.Exception.ToString());
                LogManger.Instance.WriteLog(msg);
            }

            if (filterContext.Result is JsonResult)
            {
                //当结果为json时，设置异常已处理
                filterContext.ExceptionHandled = true;
            }
            else
            {
                //否则调用原始设置
                base.OnException(filterContext);
            }
        }
    }
}