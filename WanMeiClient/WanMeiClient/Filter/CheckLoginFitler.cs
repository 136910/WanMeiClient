﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WanMeiCommon;

namespace WanMeiClient
{
    /// <summary>
    /// 是否进行跳转登录过滤器
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class CheckLoginFitler : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(HttpContext.Current.Session["un"]==null)
            {
                filterContext.Result = new RedirectResult("/Login");
                return;
            }
           
        }
    }
}